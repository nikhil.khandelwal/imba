'''
Calculate moving average, pnl and postion.
'''
import time
import pandas as pd
import matplotlib.pyplot as plt
#from plt_basics import read_data
from datetime import datetime
import nsepy

def calc_mavg(stck,mavg,thresh=20):
    mavg['rolling_avg']=float('nan')
    mavg['action']=''

    for i in range(1,len(mavg)): 
        if(i>=20):
            mavg.loc[i,'rolling_avg']=mavg['Close'][i-thresh:i].mean()
    for i in range(0,len(mavg)):
        if mavg.loc[i,'Close']>mavg.loc[i,'rolling_avg'] :
            mavg.loc[i,'action']='Buy'
        elif mavg.loc[i,'Close']<mavg.loc[i,'rolling_avg']: 
            mavg.loc[i,'action']='Sell'
        else:
            mavg.loc[i,'action']='Do nothing'            
    return mavg

def calc_pnl(mavg):
    buy=0
    sell=0
    mavg['pnl']=0
    for p in range(1,len(mavg)): 
        if mavg.loc[p,'action']=='Buy' and (mavg.loc[p-1,'action']=='Sell' or buy==0):
            buy=mavg.loc[p,'Close']
            if(sell!=0):
                mavg.loc[p,'pnl']=sell-buy
        elif mavg.loc[p,'action']=='Sell' and (mavg.loc[p-1,'action']=='Buy' or sell==0):  
            sell=mavg.loc[p,'Close']
            if(buy!=0):
                mavg.loc[p,'pnl']=sell-buy
                
    return mavg
def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'Buy'),['buy','cover']] = 1
    df.loc[(df['action'] == 'Sell'),['sell','short']] = 1
    flag=0
    df['pos']=0
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df
stck1 = 'HDFCBANK'
ntry=4
df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
while(df_stck1.empty and ntry>0):
    try:
        df_stck1=nsepy.get_history(symbol=stck1,start=datetime(2017,1,1,0,0),end=datetime(2018,3,28,0,0), index=False,futures=False)
    except Exception:
        time.sleep(100)
        pass
    ntry-=1
df_stck1=df_stck1.reset_index()

m_avg = calc_mavg(stck1,df_stck1)
m_avg = calc_pnl(m_avg)
m_avg=cal_pos(m_avg)
plt.plot(m_avg['rolling_avg'])
plt.plot(m_avg['Close'])
