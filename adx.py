import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import nsepy

def calc_adx(stck,adx_calc,r=14):
    adx_calc['p_dm1'] = 0.0
    adx_calc['m_dm1'] = 0.0
    adx_calc['trr'] = 0.0
    adx_calc['p_dmr'] = 0.0
    adx_calc['m_dmr'] = 0.0
    adx_calc['p_dir'] = 0.0
    adx_calc['m_dir'] = 0.0
    adx_calc['p_di_dif'] = 0.0
    adx_calc['p_di_sum'] = 0.0
    adx_calc['dx'] = 0.0
    adx_calc['adx'] = 0.0   
    adx_calc['tr'] =0.0
    for i in range(1,len(adx_calc)): 
        adx_calc.loc[i,'tr'] = max(adx_calc.loc[i,'High'] - adx_calc.loc[i,'Low'],np.absolute(adx_calc.loc[i,'High']-adx_calc.loc[i-1,'Close']),np.absolute(adx_calc.loc[i,'Low']-adx_calc.loc[i-1,'Close']))
        adx_calc.loc[i,'p_dm1'] = adx_calc.loc[i,'High'] - adx_calc.loc[i-1,'High'] if (adx_calc.loc[i,'High'] - adx_calc.loc[i-1,'High']>0 and (adx_calc.loc[i,'High'] - adx_calc.loc[i-1,'High']>adx_calc.loc[i-1,'Low'] - adx_calc.loc[i,'Low'])) else 0
        adx_calc.loc[i,'m_dm1'] = adx_calc.loc[i-1,'Low'] - adx_calc.loc[i,'Low'] if (adx_calc.loc[i,'Low'] - adx_calc.loc[i-1,'Low']<0 and (adx_calc.loc[i,'High'] - adx_calc.loc[i-1,'High']<adx_calc.loc[i-1,'Low'] - adx_calc.loc[i,'Low'])) else 0    
        if i==r:        
            adx_calc.loc[i,'trr'] = adx_calc['tr'][1:r+1].sum()
            adx_calc.loc[i,'p_dmr'] = adx_calc['p_dm1'][1:r+1].sum()
            adx_calc.loc[i,'m_dmr'] = adx_calc['m_dm1'][1:r+1].sum()
        elif i>r:
            adx_calc.loc[i,'trr'] = adx_calc.loc[i-1,'trr'] + adx_calc.loc[i,'tr'] - adx_calc.loc[i-r,'tr']
            adx_calc.loc[i,'p_dmr'] = adx_calc.loc[i-1,'p_dmr'] + adx_calc.loc[i,'p_dm1'] - adx_calc.loc[i-r,'p_dm1']
            adx_calc.loc[i,'m_dmr'] = adx_calc.loc[i-1,'m_dmr'] + adx_calc.loc[i,'m_dm1'] - adx_calc.loc[i-r,'m_dm1']
        adx_calc.loc[i,'p_dir'] = 100.0*adx_calc.loc[i,'p_dmr']/float(adx_calc.loc[i,'trr']) if (adx_calc.loc[i,'trr']>0) else 0
        adx_calc.loc[i,'m_dir'] = 100.0*adx_calc.loc[i,'m_dmr']/float(adx_calc.loc[i,'trr']) if (adx_calc.loc[i,'trr']>0) else 0
    adx_calc['p_di_dif'] = np.absolute(adx_calc['p_dir'] - adx_calc['m_dir'])
    adx_calc['p_di_sum'] = adx_calc['p_dir'] + adx_calc['m_dir']
    adx_calc['dx'] = 100*adx_calc['p_di_dif']/adx_calc['p_di_sum']
    for i in range(r,len(adx_calc)):
        if i==2*r-2:
           adx_calc.loc[i,'adx'] = adx_calc['dx'][r-1:2*r-1].mean()
        elif i>2*r-2:
            adx_calc.loc[i,'adx']= (adx_calc.loc[i-1,'adx']*(r-1)+ adx_calc.loc[i,'dx'])/float(r)
    return adx_calc

def calc_action(adx_calc,thresh=20.0):
    adx_calc['action']=''
    for p in range(0,len(adx_calc)): 
        if adx_calc.loc[p,'adx']>thresh:
            if(adx_calc.loc[p,'m_dir']>adx_calc.loc[p,'p_dir']):
                adx_calc.loc[p,'action']='Short'
            elif(adx_calc.loc[p,'m_dir']<= adx_calc.loc[p,'p_dir']):
                adx_calc.loc[p,'action']='Buy'        
        else:
            adx_calc.loc[p,'action']='Do Nothing'
    return adx_calc

def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'Buy'),'buy'] = 1
    df.loc[(df['action'] == 'Short'),'short'] = 1
    df.loc[(df['action'] == 'Do Nothing'),['cover','sell']] = 1
    flag=0
    df['pos']=0
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df
        
def calc_pnl(adx_calc):
    buy=0
    sell=0
    adx_calc['pnl']=0
    for p in range(1,len(adx_calc)): 
        if adx_calc.loc[p,'action']=='Buy' and (adx_calc.loc[p-1,'action']=='Sell' or buy==0):
            buy=adx_calc.loc[p,'Close']
            if(sell!=0):
                adx_calc.loc[p,'pnl']=sell-buy
        elif adx_calc.loc[p,'action']=='Sell' and (adx_calc.loc[p-1,'action']=='Buy' or sell==0):  
            sell=adx_calc.loc[p,'Close']
            if(buy!=0):
                adx_calc.loc[p,'pnl']=sell-buy
    return adx_calc

stck1 = 'HDFCBANK'
ntry=4
df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
while(df_stck1.empty and ntry>0):
    try:
        df_stck1=nsepy.get_history(symbol=stck1,start=datetime(2017,1,1,0,0),end=datetime(2018,3,28,0,0), index=False,futures=False)
    except Exception:
        time.sleep(100)
        pass
    ntry-=1
df_stck1=df_stck1.reset_index()
#df_stck1 = read_data('1.csv',stck1)
adx = calc_adx(stck1,df_stck1)
adx = calc_action(adx)
adx=calc_pnl(adx)
adx=cal_pos(adx)
#Plot
#%matplotlib qt
plt.figure(1)
plt.subplot(211)
a,=plt.plot(adx['adx'],'b',label = 'adx')
b, =plt.plot(adx['p_dir'],'g',label = 'pDI')
c, =plt.plot(adx['m_dir'],'r', label = 'mDI')
plt.hlines(20,0,len(adx))
#plt.legend([a, b,c], ['adx', 'pDI','mDI'])
plt.subplot(212)
plt.plot(adx['Open'])
plt.show()