'''
Pair Trading Stratergy(Earlier one) : Input: last day top stocks
                Calculate Current day top stocks in the interval of 15 minutes and 
                print alert message if the position of thse top stocks changes  

'''
from kiteconnect import KiteConnect
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from utilities import get_option_code
from scipy.stats.stats import pearsonr 
import statsmodels.tsa.stattools as ts
import urllib2
import time
#############################################################################
access_token = "MP77UFiS7SLktlJ3copRBRHqWWczjoaS"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)


#############################################################################
def slope(df,value):
    x = np.arange(len(df)) 
    y = list(df[value])
    m, b = np.polyfit(x, y, 1)
    return m
def cor(df1,df2,value):
    x = list(df1[value])
    y = list(df2[value])
    c,d = pearsonr(x,y)
    return c,d
def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""

###############################################################################    
def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'buy'),'buy'] = 1
    df.loc[(df['action'] == 'short'),'short'] = 1
    df.loc[(df['action'] == 'cover'),'cover'] = 1
    df.loc[(df['action'] == 'sell'),'sell'] = 1

    flag=0
    df['pos']=''
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df
###############################################################################


###############################################################################    
def calc_pnl(df1,stock1,stock2):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['pnl']=0
    curr_pos=0
    prev_pos=0
    for p in range(1,len(df1)): 
        if (df1.loc[p,'pos'] != df1.loc[p-1,'pos']):
            curr_pos=p
            if prev_pos=='':
                prev_pos=curr_pos
            else:    
                if df1.loc[curr_pos,'pos']==1:
                    buy1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    buy2=df1.loc[curr_pos,'close_'+str(stock2)]
                    if df1.loc[prev_pos,'pos']==-1:
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))      # + ((sell1-buy1)+(buy2-sell2))

                elif df1.loc[curr_pos,'pos']==0:   
                    if df1.loc[prev_pos,'pos']==1:
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                    elif df1.loc[prev_pos,'pos']==-1:
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                
                elif df1.loc[curr_pos,'pos']==-1:
                    short1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    short2=df1.loc[curr_pos,'close_'+str(stock2)]
                    
                    if df1.loc[prev_pos,'pos']==1:
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                        
                prev_pos=curr_pos
    return df1
###############################################################################


###############################################################################    
def calc_mavg(mavg,thresh=100):
    mavg['mavg']=float('nan')
    mavg['stdev']=float('nan')
    for i in range(1,len(mavg)): 
        if(i>=thresh):
            mavg.loc[i,'mavg']=mavg['close'][i-thresh:i].mean()
            mavg.loc[i,'stdev']=np.std(mavg['close'][i-thresh:i])            
    return mavg
###############################################################################


###############################################################################
def top_stock_pnl(stock1,stock2,date,a=1,b=4):
    
    df_stck1=data_stcks[stck1.index(stock1)][(pd.to_datetime((data_stcks[stck1.index(stock1)])['date'].dt.tz_localize(None)))<=date].reset_index()
    df_stck2=data_stcks[stck1.index(stock2)][(pd.to_datetime((data_stcks[stck1.index(stock2)])['date'].dt.tz_localize(None)))<=date].reset_index()

#    df_stck1=data_stcks[stck1.index(stock1)]
#    df_stck2=data_stcks[stck1.index(stock2)]

    close_ratio=pd.DataFrame(df_stck1['close']/df_stck2['close'])
    close_ratio['date']= [d.date() for d in pd.to_datetime(df_stck1['date'])]
    

    close_ratio['close_'+str(stock1)]=df_stck1['close']
    close_ratio['close_'+str(stock2)]=df_stck2['close']

    close_ratio=calc_mavg(close_ratio)
    close_ratio['action']=''
    close_ratio['last_action']=''
    lastaction=''
    prev_close=0

    for i in range(1,len(close_ratio)):
        mavg=close_ratio.loc[i,'mavg']
        stdev=close_ratio.loc[i,'stdev']
        close=close_ratio.loc[i,'close']
        prev_close=close_ratio.loc[i-1,'close']
        if close_ratio.loc[i-1,'action']!='do nothing':
            lastaction=close_ratio.loc[i-1,'action']       
        close_ratio.loc[i,'last_action']=lastaction

        
        if ((mavg+a*stdev)>close and (prev_close > (mavg+a*stdev)) and lastaction!='buy' and lastaction!='short'):
            close_ratio.loc[i,'action']='short'
            
        elif((mavg-a*stdev)<close and (prev_close < (mavg-a*stdev))and lastaction!='short' and lastaction!='buy'):
            close_ratio.loc[i,'action']='buy'
        
        elif(lastaction=='short'):
            if(close<mavg):
                close_ratio.loc[i,'action']='cover'
            elif close > (mavg+(b*stdev)):    
                close_ratio.loc[i,'action']='cover'
        elif(lastaction=='buy' ):
            if(close>mavg):
                close_ratio.loc[i,'action']='sell'
            elif close<(mavg-(b*stdev)):    
                close_ratio.loc[i,'action']='sell'
                
        if(close_ratio.loc[i,'action']==''):
            close_ratio.loc[i,'action']='do nothing'

    close_ratio=cal_pos(close_ratio)        
    close_ratio=calc_pnl(close_ratio,stock1,stock2)
    return close_ratio
###############################################################################


###############################################################################
def calc_top_stocks(date,temp=[]):
    top_stocks=pd.DataFrame()
    if type(temp)==list and not temp:
        temp=pd.DataFrame()
        temp[['stock1','stock2']]=(score.sort_values('Std/mean')[0:10].reset_index())[['Stock1','Stock2']]
        temp.loc[len(temp)]=['CAPF','IDFCBANK']
    for k in range(len(temp)):
        stock1=temp['stock1'][k]
        stock2=temp['stock2'][k]
        a=top_stock_pnl(stock1,stock2,date)
        buyP=0
        shortP=0
        l=len(a)-1
        while(buyP==0 or shortP==0):
            if(a['action'][l]=='do nothing'):
                l-=1
            else:
                if(a['action'][l]=='buy'):
                    buyP=a['close'][l]
                    shortP=float('nan')
                elif(a['action'][l]=='short'):
                    shortP=a['close'][l]    
                    buyP=float('nan')
                else:
                    buyP=float('nan')
                    shortP=float('nan')
        top_stocks.loc[k,'stock1']=stock1
        top_stocks.loc[k,'ClosePrice_stock1']=a['close_'+str(stock1)][-1:].iloc[0]
        top_stocks.loc[k,'stock2']=stock2
        top_stocks.loc[k,'ClosePrice_stock2']=a['close_'+str(stock2)][-1:].iloc[0]
        top_stocks.loc[k,'CloseRatio']=a['close'][-1:].iloc[0]
        top_stocks.loc[k,'BuyPrice']=buyP
        top_stocks.loc[k,'ShortPrice']=shortP
        mavg=a['mavg'][-1:].iloc[0]
        stdev=a['stdev'][-1:].iloc[0]
        top_stocks.loc[k,'Moving_Average']=mavg     
        top_stocks.loc[k,'Standard_Deviation']=stdev
        top_stocks.loc[k,'Mavg+Stdev']=mavg+stdev
        top_stocks.loc[k,'Mavg-Stdev']=mavg-stdev
        top_stocks.loc[k,'Mavg+4*Stdev']=mavg+4*stdev
        top_stocks.loc[k,'Mavg-4*Stdev']=mavg-4*stdev
        top_stocks.loc[k,'Current_Position']=a['pos'][-1:].iloc[0]    
        top_stocks.loc[k,'z-value']=(a['close'][-1:].iloc[0]-mavg)/stdev
        pos_count=len(a[(a['pos']!=0) & (a['pos']!='')]['pos'])
        if(pos_count==0):
            top_stocks.loc[k,'pnl']=0
        else:    
            top_stocks.loc[k,'pnl'] = (sum(a['pnl'])/np.mean(a['close_'+stock1]))*(len(a)/pos_count)
        top_stocks.loc[k,'count_pos']= pos_count                          
        top_stocks.loc[k,'count_pnl']=len(a[a['pnl']!=0]['pos'])
    return top_stocks
###############################################################################

################################## Read last 1 year Data 
data = pd.read_csv('nifty200.csv')
stck1 = list(data['Symbol'])
stck1.append('CAPF')
stck1.append('NESTLEIND')

stck2 = stck1

data_stcks = {}
curr_date=datetime.now().replace(hour=9,minute=15,second=0,microsecond=0)
from_date=curr_date.replace(year=curr_date.year-1)
print("Calculating Last 1 year data....")
for i in range(len(stck1)):
    try:
        indx=False
        if stck1[i] in ['NIFTY', 'BANKNIFTY']:
            indx=True
        if stck1[i] =='NIFTY':
            tradingsym= 'NIFTY 50'
        elif stck1[i] =='BANKNIFTY':
            tradingsym= 'NIFTY BANK'
        else:
            tradingsym = stck1[i]
        print tradingsym    
        ntry=6
        df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
        while(df_stck1.empty and ntry>0):
           try:
               df_stck1=pd.DataFrame(kite.historical_data(get_option_code(tradingsym, "NSE"), from_date, curr_date, 'day', continuous=False))
           except AttributeError:
               time.sleep(5)
        ntry-=1
        data_stcks[i] = df_stck1.copy()
        data_stcks[i]['Sector']=get_sector(stck1[i].upper())
        data_stcks[i]=data_stcks[i].reset_index()
    except urllib2.HTTPError:
        print stck1[i]
        pass
print("Data calculation finish.")

################################## Read last 1 year Data

################################## Calculate their different score
k=0
columns = ['Stock1','Stock2','Sector','S_CloseRatio','S_CloseDif','S_Cor','S_pVal','Std/mean','mean','Cointegration']
data_score = np.zeros(shape=(len(stck1)*len(stck2),len(columns)))
score = pd.DataFrame(data_score,columns = columns)

for i in range(len(data_stcks)):
    try:
        df_stck1 = pd.DataFrame(data_stcks[i])

    except:
        continue
    for j in range(i,len(stck2)):
        if(i!=j and data_stcks[i]['Sector'][1]==data_stcks[j]['Sector'][1]):
            try:
                df_stck2 = pd.DataFrame(data_stcks[j])

            except:
                continue
            if(len(df_stck1)!= len(df_stck2)):
                print 'Abort',i,j
                continue
            result = df_stck1.merge(df_stck2, on='date')                

            result['Close_ratio']= result['close_x']/result['close_y']
            result['Close_dif'] = result['close_x']-result['close_y']
            sc = [stck1[i],stck2[j],data_stcks[i]['Sector'][1]]
            
            a = slope(result,'Close_ratio')
            b = slope(result,'Close_dif')
            c= np.std(result['Close_ratio'])/np.mean(result['Close_ratio'])
            d= np.mean(result['Close_ratio'])
            coin_result= ts.adfuller(result['Close_ratio'])
            score_cor,score_p = cor(df_stck1,df_stck2,'close')
            score.ix[k] = [stck1[i],stck2[j],data_stcks[i]['Sector'][1],a,b,score_cor,score_p,c,d,coin_result[1]]
            k+=1

score = score[score['Stock1']!=0]
score=score[score.Sector!='']
################################## Calculate their different score

################################## Live Data Calculation (n, z-score)
#curr_date=datetime.now().replace(hour=9,minute=15,second=0,microsecond=0)
result=pd.DataFrame(columns=['Date','Time','stock1','stock2','n','previous n','close1','close2','ClosePrice','Z-score','Target','StopLoss','BuyPrice','ShortPrice'])

fdate=datetime(2018,5,1,0,0)
tdate=datetime(2018,5,4,0,0)

for i in range(fdate,tdate):
    print i
while(data_stcks[0][(pd.to_datetime(data_stcks[0]['date'].dt.tz_localize(None)))==curr_date].empty):
    curr_date-=timedelta(days=1)
top_stocks=calc_top_stocks(curr_date)

prev_ydate = curr_date - timedelta(days = 1)    
while(data_stcks[0][(pd.to_datetime(data_stcks[0]['date'].dt.tz_localize(None)))==prev_ydate].empty):
    prev_ydate-=timedelta(days=1)

prev_data1=calc_top_stocks(prev_ydate)

prev_data1=pd.read_csv("last_day_top_stocks.csv")
prev_data=prev_data1[['stock1','ClosePrice_stock1','stock2','ClosePrice_stock2','CloseRatio',
                      'BuyPrice','ShortPrice','Moving_Average','Standard_Deviation','Mavg+Stdev',
                      'Mavg-Stdev','Mavg+4*Stdev','Mavg-4*Stdev','Current_Position','z-value',
                      'pnl','count_pos','count_pnl']]

    
if not top_stocks[['stock1','stock2']].equals(prev_data[['stock1','stock2']]):
    top_stocks=top_stocks.append(prev_data[(~((prev_data.stock1.isin(top_stocks.stock1))&(prev_data.stock2.isin(top_stocks.stock2))))&(prev_data.Current_Position!=0)], ignore_index=True)
    top_stocks=calc_top_stocks(curr_date,top_stocks[['stock1','stock2']])

z_score=top_stocks['z-value']
prev_z=z_score[:]

curr_pos=top_stocks['Current_Position']
prev_pos=calc_top_stocks(prev_ydate,top_stocks[['stock1','stock2']])['Current_Position']
print("Last Position..")
print(top_stocks[['stock1','stock2','Current_Position']])

n=[0 for j in range(len(top_stocks))]
curr_time=datetime.strftime(datetime.now(), "%H:%M")
first=1
while curr_time<="15:29":
    a=pd.DataFrame(columns=['Stocks1', 'Stock2', 'CURRENT_N', 'PREVIOUS_N', 'Close', 'Z_score', 'Target', 'Stop_Loss'])    
    prev_z[:]=z_score[:]
    print "Fetching Data for session : "+str(curr_time)
    for k in range(len(top_stocks)):
        stock1=top_stocks['stock1'][k]
        stock2=top_stocks['stock2'][k]
        buyP=top_stocks['BuyPrice'][k]
        shortP=top_stocks['ShortPrice'][k]
        if(first==1):
            try:
                n[k]=prev_data1[(prev_data1['stock1']==stock1) & (prev_data1['stock2']==stock2)]['n'].iloc[0]
            except Exception:
                if top_stocks[(top_stocks.stock1==stock1)&(top_stocks.stock2==stock2)]['Current_Position'][-1:].iloc[0]!=0:
                    n[k]=2                            
                else:
                    n[k]=0
                
        n_prev=n[k]
        if (n_prev!=0):
            if stock1 =='NIFTY':
                tradingsym1= 'NIFTY 50'
            elif stock1 =='BANKNIFTY':
                tradingsym1= 'NIFTY BANK'
            else:
                tradingsym1 = stock1
            if stock2 =='NIFTY':
                tradingsym2= 'NIFTY 50'
            elif stock2 =='BANKNIFTY':
                tradingsym2= 'NIFTY BANK'
            else:
                tradingsym2 = stock2

            close1=0
            close2=0
            ntry=0
            while ((close1==0 or close2==0) and ntry<5):
                ntry+=1
                try:
                    data=kite.quote('NSE:'+str(tradingsym1),'NSE:'+str(tradingsym2))
                    data1=data['NSE:'+str(tradingsym1)]
                    data2=data['NSE:'+str(tradingsym2)]
                    try:
                        close1 = (data1['last_price'] + data1['depth']['buy'][0]['price'] + data1['depth']['sell'][0]['price'])/3.0
                    except:
                        close1 = (data1['last_price'])
 
                    try:
                        close2 = (data2['last_price'] + data2['depth']['buy'][0]['price'] + data2['depth']['sell'][0]['price'])/3.0
                    except:
                        close2 = (data2['last_price'])                    
                except:
                    print 'Trying to fetch Data'
                    time.sleep(1)

            mavg,stdev=top_stocks[(top_stocks.stock1==stock1)&(top_stocks.stock2==stock2)][['Moving_Average','Standard_Deviation']].iloc[0]

            if(close1==0 or close2==0):
                n[k]=n_prev
                close=0
                lose=0
                z_score[k]=prev_z[k]

            else:
                close=close1/close2
                z_score[k]= (close-mavg)/stdev    

                if curr_pos[k]==1:
                    if -0.5<z_score[k]<0:
                        n[k]=1
                    elif z_score[k]>0 or z_score[k]<-4 :
                        n[k]=0
                    elif -0.5<prev_z[k]<0 and -1<z_score[k]<-0.5:
                        n[k]=1
                    elif -1<prev_z[k]<0 and -3<z_score[k]<-1:
                        n[k]=2
                    elif -3<prev_z[k]<-1 and -4<z_score[k]<-3:
                        n[k]=3
                    elif -4<prev_z[k]<-3 and -3<z_score[k]<-1:
                        n[k]=3
                    elif -4<prev_z[k]<-3 and -1<z_score[k]<-0.5:
                        n[k]=2
                    loss=mavg-4*stdev    
    
                    
                elif curr_pos[k]==-1:
                    if 0<z_score[k]<0.5:
                        n[k]=1
                    elif z_score[k]<0 or z_score[k]>4 :
                        n[k]=0
                    elif 0<prev_z[k]<0.5 and 0.5<z_score[k]<1:
                        n[k]=1
                    elif 0<prev_z[k]<1 and 1<z_score[k]<3:
                        n[k]=2
                    elif 1<prev_z[k]<3 and 3<z_score[k]<4:
                        n[k]=3
                    elif 3<prev_z[k]<4 and 1<z_score[k]<3:
                        n[k]=3
                    elif 3<prev_z[k]<4 and 0.5<z_score[k]<1:
                        n[k]=2
                    loss=mavg+4*stdev    
                if(n[k]!=n_prev):
                    print("ALERT: Stocks: "+stock1+" and "+stock2+"\n CURRENT N: "+str(n[k])+" PREVIOUS N: "+str(n_prev)+" Close:"+str(round(close,2))+" Z-score: "+str(z_score[k])+" Target: "+str(mavg)+" and Stop Loss: "+str(loss))
                    result.loc[len(result)]=[str(curr_date.date()),curr_time,stock1,stock2, n[k],n_prev,close1,close2,close,z_score,mavg,loss,buyP,shortP]
        elif(n_prev==0):
            if prev_pos[k]==curr_pos[k]:
                n[k]=0
            elif prev_pos[k]==0 and (curr_pos[k]==1 or curr_pos[k]==-1):
                n[k]=2

        if(n_prev!=0):
            a.loc[len(a)]=[stock1, stock2, n[k], n_prev, round(close,4), round(z_score[k],4), round(mavg,4), round(loss,4)]
        else:
            a.loc[len(a)]=[stock1, stock2, n[k], n_prev, 0, 0, 0, 0]
    pd.set_option('display.width', 70)
    print a
    print "Last session completed. Please wait for 15 minutes for the next session to start..."
    first=0     
    time.sleep(900)
    curr_time=datetime.strftime(datetime.now(), "%H:%M")
top_stocks['n']=n
top_stocks.to_csv('C:/Users/DELL/Commodity_OptionData/last_day_top_stocks.csv', index = False)
##########################################################################################

###### Plot moving average+standard deviation graph ###########################

'''
def plot1(stock1,stock2):
    close_ratio=top_stock_pnl(stock1,stock2)
    close_ratio['mavg']=close_ratio['mavg'].fillna(method='bfill')
    close_ratio['stdev']=close_ratio['stdev'].fillna(method='bfill')
    plt.plot(close_ratio['close'])
    plt.plot(close_ratio['mavg']) 
    plt.plot(close_ratio['mavg']+2*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']-2*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']+3*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']-3*close_ratio['stdev'])    
    plt.show()

    
stock1='IPCALAB'
stock2='LUPIN'
plot1(stock1,stock2)

plt.subplot(3, 1, 1).set_title(stock1 +" &" +stock2+" "+str(year))

stock1='OFSS'
stock2='TCS'
year=2015
plt.subplot(3, 1, 2).set_title(stock1 +" &" +stock2+" "+str(year))
plot1(stock1,stock2,year)

stock1='IGL'
stock2='PETRONET'
year=2017
plt.subplot(3, 1, 3).set_title(stock1 +" &" +stock2+" "+str(year))
plot1(stock1,stock2,year)
'''
###############################################################################
