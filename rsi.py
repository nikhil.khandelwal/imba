import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#from plt_basics import read_data
from datetime import datetime
import nsepy

def calc_rsi(stck,rsi_d,win=7):
    rsi_d['gain'] = 0.0
    rsi_d['loss'] = 0.0
    rsi_d['avg_g'] = 0.0
    rsi_d['avg_l'] =0.0
    rsi_d['rs']=0.0
    rsi_d['rsi'] =0.0
    rsi_d['rolling_avg_20']=0.0

    for i in range(1,len(rsi_d)): 
        if (rsi_d.loc[i,'Close']-rsi_d.loc[i-1,'Close']>0):
            rsi_d.loc[i,'gain'] = rsi_d.loc[i,'Close']-rsi_d.loc[i-1,'Close']
        else:
           rsi_d.loc[i,'loss'] = rsi_d.loc[i-1,'Close']-rsi_d.loc[i,'Close']
        if i==win:
           rsi_d.loc[i,'avg_g'] = rsi_d['gain'][0:win].mean()
           rsi_d.loc[i,'avg_l'] = rsi_d['loss'][0:win].mean()
        elif i>win:
            rsi_d.loc[i,'avg_g'] = (rsi_d.loc[i-1,'avg_g']*(win-1)+ rsi_d.loc[i,'gain'])/float(win)
            rsi_d.loc[i,'avg_l'] = (rsi_d.loc[i-1,'avg_l']*(win-1)+ rsi_d.loc[i,'loss'])/float(win)
        if(i>=20):
            rsi_d.loc[i,'rolling_avg_20']=rsi_d['Close'][i-20:i].mean()
    
    rsi_d['rs'] = rsi_d['avg_g']/rsi_d['avg_l']
    rsi_d['rsi'] = 100.0 - 100.0/(1.0+rsi_d['rs'])
    return rsi_d
def cal_pos(df1):
    df1['buy']=0
    df1['sell']=0
    df1['cover']=0
    df1['short']=0

    df1.loc[(df1['action'] == 'buy'),'buy'] = 1
    df1.loc[(df1['action'] == 'short'),'short'] = 1
    df1.loc[(df1['action'] == 'cover'),'cover'] = 1
    df1.loc[(df1['action'] == 'sell'),'sell'] = 1

    flag=0
    df1['pos']=0
    for p in range(0,len(df1)):
        if(flag==0):
            if(df1.loc[p,'buy']==0 and df1.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df1.loc[p,'pos'] = 1 if(df1.loc[p,'buy']==1) else -1
        else:
            if(df1.loc[p,'buy']==1):
                df1.loc[p,'pos']=1
            elif(df1.loc[p,'short']==1):
                df1.loc[p,'pos']=-1
            elif(df1.loc[p,'sell']==1 and df1.loc[p-1,'pos']==1):    
                df1.loc[p,'pos']=0
            elif(df1.loc[p,'cover']==1 and df1.loc[p-1,'pos']==-1):
                df1.loc[p,'pos']=0
            else:
                df1.loc[p,'pos']=df1.loc[p-1,'pos']
    
    return df1

def calc_action(rsi_d):
    rsi_d['action']=''
    for p in range(0,len(rsi_d)): 
        if rsi_d.loc[p,'rsi']>70:
            rsi_d.loc[p,'action']='buy'
        elif rsi_d.loc[p,'rsi']<30:
            rsi_d.loc[p,'action']='short'      
        elif rsi_d.loc[p,'rsi']>50:    
            rsi_d.loc[p,'action']='cover'
        elif rsi_d.loc[p,'rsi']<50:    
            rsi_d.loc[p,'action']='sell'
        
        else:
            rsi_d.loc[p,'action']='Do Nothing'
    return rsi_d

def calc_pnl(df1):
    buy=0  #1
    sell=0 #0
    short=0 #-1
    df1['pnl']=0
    for p in range(1,len(df1)): 
        if df1.loc[p,'pos'] != df1.loc[p-1,'pos']:
            if df1.loc[p,'pos']==1:
                buy=df1.loc[p,'close']
                if df1.loc[p-1,'pos']==-1:
                    df1.loc[p,'pnl']=(short-sell)-(sell-buy)
    
            elif df1.loc[p,'pos']==0:  
                sell=df1.loc[p,'close']
                if df1.loc[p-1,'pos']==1:
                    df1.loc[p,'pnl']=sell-buy
                elif df1.loc[p-1,'pos']==-1:
                    df1.loc[p,'pnl']=short-sell
                    
            elif df1.loc[p,'pos']==-1:  
                short=df1.loc[p,'close']
                if df1.loc[p-1,'pos']==1:
                    df1.loc[p,'pnl']=(short-sell)-(sell-buy)                
                
    return df1


stck1 = 'HDFCBANK'
#df_stck1 = read_data('1.csv',stck1)
ntry=4
df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
while(df_stck1.empty and ntry>0):
    try:
        df_stck1=nsepy.get_history(symbol=stck1,start=datetime(2017,1,1,0,0),end=datetime(2018,3,28,0,0), index=False,futures=False)
    except Exception:
        time.sleep(100)
        pass
    ntry-=1
df_stck1=df_stck1.reset_index()

rsi = calc_rsi(stck1,df_stck1)
rsi=calc_action(rsi)
rsi=cal_pos(rsi)
#Plot
#%matplotlib qt
plt.figure(1)
plt.subplot(211)
plt.plot(rsi['Close'])
plt.subplot(212)
line1,=plt.plot(rsi['rsi'],label = 'rsi')
plt.hlines(30,0,len(rsi))
plt.hlines(70,0,len(rsi))
plt.legend([line1], ['rsi'])
plt.show()


'''
df=pd.DataFrame({'close':adx['Close'],'adx':adx['adx'],'adx_pdir':adx['p_dir'],
'adx_mdir':adx['m_dir'],'mavg_rollavg':m_avg['rolling_avg'],'rsi':rsi['rsi']})
df.fillna(0,inplace=True)    
df['action']=''
for p in range(0,len(df)): 
    if(df.loc[p,'adx']>20 and (df.loc[p,'adx_pdir']>df.loc[p,'adx_mdir']) and df.loc[p,'rsi']>50 and (df.loc[p,'mavg_rollavg']*1.01<df.loc[p,'close'])):
        df.loc[p,'action']='buy'
    elif(df.loc[p,'adx']>20 and (df.loc[p,'adx_pdir']<df.loc[p,'adx_mdir']) and df.loc[p,'rsi']<50 and (df.loc[p,'mavg_rollavg']*0.99>df.loc[p,'close'])):
        df.loc[p,'action']='short'
    elif(df.loc[p,'mavg_rollavg']>df.loc[p,'close']):
        df.loc[p,'action']='sell'
    elif(df.loc[p,'mavg_rollavg']<df.loc[p,'close']):
        df.loc[p,'action']='cover'
    else:
        df.loc[p,'action']='Do nothing'
df['buy']=0
df['sell']=0
df['cover']=0
df['short']=0

df.loc[(df['action'] == 'buy'),'buy'] = 1
df.loc[(df['action'] == 'short'),'short'] = 1
df.loc[(df['action'] == 'cover'),'cover'] = 1
df.loc[(df['action'] == 'sell'),'sell'] = 1

df=cal_pos(df)
df=calc_pnl(df)

'''