'''
Calculate the returns(day week,month,6month and year high and returns) 
for the nifty100 list from year 2013 to 2015

'''
from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime, timedelta
import time
from utilities import get_option_code
import nsepy
from collections import defaultdict
access_token = "brgetBKoEWihxAsi4hNdqeQ1QVtMknCx"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)

###############################################################################
def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
###############################################################################

#list of Nifty100 indexes included and excluded from the year 2013 to 2015
df1=pd.read_excel("IndexInclExcl.xls",sheetname=2)
for p in range(len(df1)):
    if(type(df1['Event Date'][p])== unicode):
        df1['Event Date'][p]=datetime.strptime(df1['Event Date'][p],"%d-%m-%Y")


df3=pd.read_csv("ind_nifty100list.csv")
nifty_c = list(df3.Symbol)
df1 = df1[["Event Date","Description","Ticker"]]
df4 = df1[::-1].reset_index(drop=True)
nifty = list(nifty_c)
result = {}
for i in range(len(df4)):
    if df4.ix[i]['Description'] =='Exclusion from Index ' or df4.ix[i]['Description'] =='Exclusion from Index':
        try:    
            nifty.append(df4.ix[i]['Ticker'])
        except:
            continue
    elif df4.ix[i]['Description'] =='Inclusion into Index' or df4.ix[i]['Description'] =='Inclusion into Index ':
        try:    
            nifty.remove(df4.ix[i]['Ticker'])
        except:
            continue
    else:
        print "Error"
        break
    result[df4["Event Date"][i]] = nifty[:]

dates=pd.DataFrame(result.keys(),columns=['Date'])
dates['list']=0
for dt in range(len(dates['Date'])):
    dates['list'][dt]=result[dates['Date'][dt]]
    if type(dates['Date'][dt])==unicode:
        dates.loc[dt,'Date']=datetime.strptime(dates['Date'][dt], '%d-%m-%Y')

dates=dates[dates.Date>datetime(2012,9,20)].reset_index(drop=True)

list1=[]
for j in range(len(dates)):
    list1+=dates['list'][j]

list1=list(set(list1))
list1.append('NIFTY 50')
######################################################################

df = defaultdict(list)
d=[]

''' pull data form 2008 to 2018'''
print "Fetching Data using Kite..."
for i in range(len(list1)):
    tradingsym=list1[i]    
    ntry=4
    a=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
    a2=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
    while(a.empty and ntry>0):
       try:
           a=pd.DataFrame(kite.historical_data(get_option_code(tradingsym, "NSE"), datetime(2008, 1, 1 ,9, 15, 0), datetime(2013, 1, 1, 9, 15, 0), 'day', continuous=False))
           a2=pd.DataFrame(kite.historical_data(get_option_code(tradingsym, "NSE"), datetime(2013, 1, 1 ,9, 15, 0), datetime(2018, 6, 5, 9, 15, 0), 'day', continuous=False))
           a=a.append(a2).reset_index(drop=True)
       except Exception:
           time.sleep(1)
       ntry-=1
    if a.empty:
        d.append(list1[i])
    else:
        df[tradingsym]=a 
######################################################################

print "Fetching Data of the remaining stocks using Nsepy..."

'''pull data(2008-2018) of stocks who's data is not fetched using kite'''
d1=d[:]
for i in range(len(d)):
    tradingsym=d[i]
    ntry=3
    while(ntry>0):
        try:
            nsepyData=nsepy.get_history(symbol=tradingsym,start=datetime(2008, 1, 1 ,9, 15, 0),end=datetime(2018, 6, 4, 9, 15, 0), index=False,futures=False)
            nsepyData=nsepyData.reset_index()
            nsepyData.columns=map(str.lower, nsepyData.columns)
            if max(nsepyData['high'][0:-1].reset_index(drop=True)/nsepyData['low'][1:].reset_index(drop=True))>2:
                print tradingsym, 'Splited'
                ntry=0
            else:    
                df[tradingsym]=nsepyData  
                d1.remove(tradingsym)
                ntry=0    
        except AttributeError:
            print tradingsym, 'Error Fetching'
            ntry-=1
            pass    

d2=list(set(d).difference(set(d1)))
list1=list(set(list1).difference(set(d1)))

'''
##################################### Calculate Nifty100 list(2013-2017) high ##################
no_of_days: no of days high to be calculated (273 for 9 months and so on)
'''
def calc_data(no_of_days):
    curr_date = datetime(2017, 1, 1 ,9, 15, 0)
    to_date = datetime(2017, 5, 31, 9, 15, 0)
    data = pd.DataFrame(columns=['Scriptname','Date','Type','Price'])
    l1=dates['list'][0]
    k=1
    while(curr_date<to_date):
        while(df['ADANIPORTS'][(pd.to_datetime((df['ADANIPORTS'])['date'].dt.tz_localize(None)))==curr_date].empty and curr_date<=to_date):
            curr_date+=timedelta(days=1)
        
        if curr_date.replace(hour=0,minute=0,second=0)==dates['Date'][k]:
            print(curr_date.date(),"change NIFTY100list")
            l1=dates['list'][k+1]
            k+=1
                
        for i in range(len(l1)):
            tradingsym=l1[i]    
            try:
                if tradingsym in d2:
                    df1=df[tradingsym][(pd.to_datetime(df[tradingsym]['date']+timedelta(minutes=555))<=curr_date) & (pd.to_datetime(df[tradingsym]['date']+timedelta(minutes=555))>=curr_date-timedelta(days=no_of_days))].reset_index()
                else:                
                    df1=df[tradingsym][(pd.to_datetime(df[tradingsym]['date'].dt.tz_localize(None))<=curr_date) & (pd.to_datetime(df[tradingsym]['date'].dt.tz_localize(None))>=curr_date-timedelta(days=no_of_days))].reset_index()
    
                if(max(df1['close'])==df1['close'].iloc[-1]):
                    data.loc[len(data)]=[tradingsym,str(curr_date.date()),'High',df1['close'].iloc[-1]]
                elif(min(df1['close'])==df1['close'].iloc[-1]):
                    data.loc[len(data)]=[tradingsym,str(curr_date.date()),'Low',df1['close'].iloc[-1]]
    
            except Exception:
                continue
    
        if (curr_date.weekday()>=4):
            curr_date=curr_date+timedelta(days=(7-curr_date.weekday()))
        else:
            curr_date=curr_date+timedelta(days=1)
    
    ###############################################################################
    stck_list=list(data['Scriptname'].unique())
    indx=[]
    for r in range(len(stck_list)):
        sub_data=data[data['Scriptname']==stck_list[r]].reset_index()
        initial=0
        for s in range(1,len(sub_data)):
            day=(datetime.strptime(sub_data['Date'][s],'%Y-%m-%d')-datetime.strptime(sub_data['Date'][initial],'%Y-%m-%d')).days
            if(day<180):
                indx.append(sub_data['index'][s])
            else:
                initial=s
    
    data=(data.drop(data.index[indx])).reset_index(drop=True)
    ###############################################################################
    
    
    for j in range(len(data)):
    
        tradingsym=data['Scriptname'][j]
        date=datetime.strptime(data['Date'][j], '%Y-%m-%d').replace(hour=9,minute=15)
        if tradingsym in d2:
            idx=df[tradingsym].index[pd.to_datetime(df[tradingsym]['date'])+timedelta(minutes=555)==date]
        else:
            idx=df[tradingsym].index[(pd.to_datetime(df[tradingsym]['date'].dt.tz_localize(None)))==date]
    
        idx2=df['NIFTY 50'].index[(pd.to_datetime((df['NIFTY 50'])['date'].dt.tz_localize(None)))==date]
        data.loc[j,'Sector']=get_sector(tradingsym.upper())
    
    
        try:
            curr_close=df[tradingsym].loc[idx,'close'].iloc[0]
            month6=df[tradingsym].loc[idx+126,'close'].iloc[0]
            month12=df[tradingsym].loc[idx+252,'close'].iloc[0]
            curr_close2=df['NIFTY 50'].loc[idx2,'close'].iloc[0]
            month6_n=df['NIFTY 50'].loc[idx2+126,'close'].iloc[0]
            month12_n=df['NIFTY 50'].loc[idx2+252,'close'].iloc[0]

        except Exception:
            continue
    
        data.loc[j,'nextyearPrice']=month12
        data.loc[j,'Average_6months_return']=((month6/curr_close)-1)/182
        data.loc[j,'Average_12months_return']=((month12/curr_close)-1)/365
    
    
        data.loc[j,'NIFTYAverage_6months_return']=((month6_n/curr_close2)-1)/182
        data.loc[j,'NIFTYAverage_12months_return']=((month12_n/curr_close2)-1)/365
    
    data['DIFFAverage_6months']=data['Average_6months_return']-data['NIFTYAverage_6months_return']
    data['DIFFAverage_12months']=data['Average_12months_return']-data['NIFTYAverage_12months_return']

    data['year'] = pd.DatetimeIndex(data['Date']).year
#    data=data[data.Type=='High'].reset_index(drop=True)
    
    x= data[['Average_6months_return','DIFFAverage_6months',
    'Average_12months_return','DIFFAverage_12months',
    ]].groupby([data['Type'],data['year']]).mean()
    
    return data,x

print"calculating 12 months high data" 
data_12months,x_12months=calc_data(365)
print"calculating 9 months high data" 
data_9months,x_9months=calc_data(273)
print"calculating 15 months high data" 
data_15months,x_15months=calc_data(457)
print"calculating 18 months high data" 
data_18months,x_18months=calc_data(549)
print"calculating 2 years high data" 
data_24months,x_24months=calc_data(730)
print"calculating 5 years high data" 
data_60months,x_60months=calc_data(1826)

final = pd.concat([x_9months,x_12months,x_15months,x_18months,x_24months,x_60months], axis=1, keys=('9months','1year','15months','18months','2years','5years',))
final['Count_12months']=data_12months[['Average_6months_return','DIFFAverage_6months',
'Average_12months_return','DIFFAverage_12months',
]].groupby([data_12months['Type'],data_12months['year']]).size()

final['Count_15months']=data_15months[['Average_6months_return','DIFFAverage_6months',
'Average_12months_return','DIFFAverage_12months',
]].groupby([data_15months['Type'],data_15months['year']]).size()

final['Recalculate_6monthsreturn_12High']=(final['1year', 'Average_6months_return']*250)-(final['Count_12months']*0.001)
final['Recalculate_12monthsreturn_12High']=(final['1year', 'Average_12months_return']*250)-(final['Count_12months']*0.001)
final['Recalculate_6monthsreturn_15High']=(final['15months', 'Average_6months_return']*250)-(final['Count_15months']*0.001)
final['Recalculate_12monthsreturn_15High']=(final['15months', 'Average_12months_return']*250)-(final['Count_15months']*0.001)

#final=final.query('2013 <= year <= 2016')
#final=final.query("Type == u'High'")
High=final.query("Type==u'High'").mean()
Low=final.query("Type==u'Low'").mean()
final.loc['Average_High'] =High 
final.loc['Average_Low'] = Low


writer = pd.ExcelWriter('Nifty(2013-2017).xlsx', engine='xlsxwriter')


# Write each dataframe to a different worksheet.
final.to_excel(writer, sheet_name='Complete Result')
data_9months.to_excel(writer, sheet_name='9 Months High ')
data_12months.to_excel(writer, sheet_name='12 Months High')
data_15months.to_excel(writer, sheet_name='15 Months High')
data_18months.to_excel(writer, sheet_name='18 Months High')
data_24months.to_excel(writer, sheet_name='2 Years High')
data_60months.to_excel(writer, sheet_name='5 Years High')

# Close the Pandas Excel writer and output the Excel file.
writer.save()


