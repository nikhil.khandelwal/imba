from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime, timedelta
import time
from utilities import get_option_code, get_pain, import_nsepydata, get_vol
import math
access_token = "dgfbzmx2oz5f20bh1001qfzvrn176xxz"
stocks=pd.read_csv("dashboard_stocks.csv")
stocks_list = list(stocks.ix[0])
stocks_list.extend(['NIFTY', 'BANKNIFTY'])

opt_maturity = datetime(2018, 3, 28, 15, 30, 00)
prev_expiry = datetime(2018, 2, 22,9, 15, 00)
root = ''

def get_price(exch, ticker):
    price=0
    ntry=0
    while price==0  and ntry<5:
        ntry+=1
        try:
            data = kite.quote(exch, ticker)
            price = (data['last_price'] + data['depth']['buy'][0]['price'] + data['depth']['sell'][0]['price'])/3.0
        except:
            print ('Trying to fetch Data')
            time.sleep(1)
    price = round(0.05*round(price/0.05), 2)
    return price
def get_strike_diff(underlying):
    if underlying =='NIFTY' or underlying =='BANKNIFTY':
        return 100.0
    data = pd.read_excel('strike_price.xlsx')
    return float(data[data['Ticker ']==underlying]['Strike  Diff.'])
def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
def get_atmStrike(price, strike_dif):
    try:
        temp = int(int(price / strike_dif)*strike_dif)
    except:
        return 0
    if underlying =='NIFTY' or underlying =='BANKNIFTY':
        return temp
    data = pd.read_excel('strike_price.xlsx')
    baseP = data[data['Ticker ']==underlying]['Strike Price'].iloc[0]
    if int((temp - baseP)/strike_dif) == (temp - baseP)/strike_dif:
        return temp
    else:
        print underlying
        return int(baseP + int((temp - baseP)/strike_dif)*baseP)
def getprevdayData(underlying, result_yesterday):
    try:
        data  = result_yesterday[result_yesterday['underlying']==underlying].iloc[0]
        return data['OI'], data['vol_ce'], data['vol_pe'], data['close']
    except:
        return 0,0,0,0
def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r
    
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
exch="NFO"
exch2 ="NSE"
cur_month = datetime.strftime(opt_maturity, '%b').upper()
cur_year = datetime.strftime(opt_maturity, "%y")
maturity = str(cur_year)+cur_month
prev_exp_str = prev_expiry.strftime('%Y-%m-%dT%H:%M:%S+0530')
to_date_now = datetime.strftime(datetime.now(), "%Y-%m-%d")
if datetime.now().weekday() ==0:
    yesterdayDelta = 3
else:
    yesterdayDelta = 1
yesterday_date = datetime.strftime(datetime.now() - timedelta(days = yesterdayDelta), "%Y-%m-%d")
from_date_now = datetime.strftime(datetime.now().replace(year = datetime.now().year-5), "%Y-%m-%d")
deliveryDataDate  = datetime.now().replace(year = datetime.now().year - 1)
curr_time = datetime.strftime(datetime.now(), "%H:%M:%S")
filePath = root+ 'dashboard/'+to_date_now+'.csv'
filePath_yesterday = root+'dashboard/'+yesterday_date+'.csv'
columns = ['Date', 'underlying', 'open', 'high', 'low','close','lastDayClose','today_return'
           , 'lastday_return', '5day_return', 'niftyReturn', 'beta', 'sector', 'premium', '5d avg_premium'
           ,'deliveryAvg_5', 'deliveryAvg_10', 'deliveryAvg_30'
           , 'volume_inc_perc', 'OI','change oi', 'atm_strike', 'vol_ce', 'vol_pe', 'avg_vol','change ce_vol','change pe_vol', 'pain', 'pain_price_ratio', '5yr high'
           , '5yr_low', '50d high', '50d low', '100d high', '100d low', 'max_ce_oi', 'max_pe_oi', 'prev_expiry','fo_view']

try:
    result_yesterday = pd.read_csv(filePath_yesterday)
except:
    result_yesterday = pd.DataFrame(columns = columns)

errorL = []
error_run = 0
if error_run == 1 and len(errorL)>0:
    run_l = errorL[:]
    result_today = pd.read_csv(filePath)
else:
    run_l = stocks_list[:]
    result_today = pd.DataFrame(columns = columns)

indexData  = pd.DataFrame(kite.historical(get_option_code('NIFTY 50', exch2),from_date_now, to_date_now, 'day'))
for i in range(len(run_l)):
    if len(errorL)==0:
        underlying  = stocks_list[i]
    else:
        underlying = errorL[0]
    if underlying =='NIFTY':
        tradingsym= 'NIFTY 50'
    elif underlying =='BANKNIFTY':
        tradingsym= 'NIFTY BANK'
    else:
        tradingsym = underlying
    fo_ticker = underlying+maturity+'FUT'
    strike_dif = get_strike_diff(underlying)
    ############ PULL DATA ####################
    try:
        fo_curr_data = kite.quote(exch, fo_ticker)
        master_fo = pd.DataFrame(kite.historical(get_option_code(fo_ticker, exch),from_date_now, to_date_now, 'day'))
        if underlying in ['NIFTY', 'BANKNIFTY']:
            master = master_fo[:][:]
        else:
            master = pd.DataFrame(kite.historical(get_option_code(underlying, exch2),from_date_now, to_date_now, 'day'))
        nsepyData  = import_nsepydata(underlying, deliveryDataDate)
        print (underlying , 'Data Successfully pulled. Doing Calculations...')        
        time.sleep(3)
    except Exception as e:
        print (underlying , 'Data pull failed. Moving to next... Error was:', e.__class__.__name__)
        errorL.append(underlying)
        time.sleep(5)
        continue
    ############## CALCULATIONS################
    openP = master.ix[len(master)-1]['open']
    highP = master.ix[len(master)-1]['high']
    lowP = master.ix[len(master)-1]['low']
    closeP = master.ix[len(master)-1]['close']
    lastDayClose = master.ix[len(master)-2]['close']
    today_return = (closeP/openP - 1)*100
    lastDay_return = (closeP/lastDayClose - 1)*100
    nday_return_5 = (closeP/master.ix[len(master)-6]['close'] - 1)*100
    niftyReturn =  (indexData.ix[len(indexData)-1]['close']/indexData.ix[len(indexData)-2]['close'] - 1)*100
    indexReturn =  lastDay_return - niftyReturn
    avg_change = master.ix[len(master)-200:len(master)]['close'].std()/master.ix[len(master)-200:len(master)]['close'].mean()
    index_avg_change = indexData.ix[len(indexData)-200:len(indexData)]['close'].std()/indexData.ix[len(indexData)-200:len(indexData)]['close'].mean()
    beta = avg_change/index_avg_change
    sector = get_sector(underlying)
    premium  = (master_fo.ix[len(master_fo)-1]['close']/master.ix[len(master)-1]['close'] -1)*100
    avg_premium = (master_fo.ix[len(master_fo)-5:len(master_fo)]['close'].mean()/master.ix[len(master)-5:len(master)]['close'].mean() - 1)*100
    if len(nsepyData)>0:
        deliveryAvg_5 = nsepyData.ix[len(nsepyData)-5:len(nsepyData)]['Deliverable Volume'].mean()
        deliveryAvg_10 = nsepyData.ix[len(nsepyData)-10:len(nsepyData)]['Deliverable Volume'].mean()
        deliveryAvg_30 = nsepyData.ix[len(nsepyData)-30:len(nsepyData)]['Deliverable Volume'].mean()
        volume_inc_percentage = float(nsepyData.ix[len(nsepyData)-1]['Volume'])/nsepyData.ix[len(nsepyData)-6:len(nsepyData)]['Volume'].mean()
    else:
        deliveryAvg_5 = 0
        deliveryAvg_10 = 0
        deliveryAvg_30 = 0
        volume_inc_percentage = 0
    OI = fo_curr_data['open_interest']
    high_5yr =max(master['high'])
    low_5yr = min(master['low'])
    high_50d =max(master.ix[len(master)-50:len(master)]['high'])
    high_100d =max(master.ix[len(master)-100:len(master)]['high'])
    low_50d =min(master.ix[len(master)-50:len(master)]['low'])
    low_100d =min(master.ix[len(master)-100:len(master)]['low'])
    prevexpiry_close = float(master[master['date']==prev_exp_str]['close'])
    oi_last, ce_vol_last, pe_vol_last, close_last = getprevdayData(underlying, result_yesterday)
    if math.isnan(strike_dif) or int(strike_dif*10)/10 != strike_dif:
        print ('No option volume')
        atm_strike = 0
        vol_ce = 0
        vol_pe = 0
        avg_vol = 0
        pain = 0
        pain_price_dif = 0
        max_call_OI = 0
        max_put_OI = 0
    else:
        atm_strike = get_atmStrike(fo_curr_data['last_price'],strike_dif)
        print (underlying , 'Calculating Option Volitility')
        vol_ce = get_vol(access_token, underlying, str(atm_strike), 'CE', fo_curr_data['last_price'], opt_maturity)
        vol_pe = get_vol(access_token, underlying, str(atm_strike), 'PE', fo_curr_data['last_price'], opt_maturity)
        avg_vol = 0.5*(vol_pe+ vol_ce)
        print (underlying , 'Calculating Option Pain')
        pain, OI_data = get_pain(access_token, tradingsym, underlying, int(strike_dif))
        pain_price_dif = (pain/closeP - 1)*100
        max_call_OI = OI_data['Strike'][OI_data['OI_CE'].idxmax()]
        max_put_OI = OI_data['Strike'][OI_data['OI_PE'].idxmax()]
    change_oi = calc_change(OI,oi_last)
    change_ce_vol = calc_change(vol_ce, ce_vol_last)
    change_pe_vol = calc_change(vol_pe, pe_vol_last)
    if OI>oi_last:
        if closeP>close_last:
            fo_view= 'Long Buildup'
        elif closeP<close_last:
            fo_view = 'Short Buildup'
        else:
            fo_view = 'Unknown'    
    elif OI<oi_last:
        if closeP>close_last:
            fo_view= 'Short Covering'
        elif closeP<close_last:
            fo_view = 'Long unwinding'
        else:
            fo_view = 'Unknown'
    else:
        fo_view = 'Unknown'      
    result_today.loc[len(result_today)] = [to_date_now, underlying, openP, highP, lowP,closeP,lastDayClose,today_return
           , lastDay_return, nday_return_5, indexReturn, beta, sector, premium, avg_premium
           ,deliveryAvg_5, deliveryAvg_10, deliveryAvg_30
           , volume_inc_percentage, OI, change_oi, atm_strike, vol_ce, vol_pe, avg_vol,change_ce_vol,change_pe_vol, pain, pain_price_dif, high_5yr
           , low_5yr, high_50d, low_50d, high_100d, low_100d, max_call_OI,max_put_OI, prevexpiry_close, fo_view]
    print(underlying, 'Completed')
    try:
        errorL.remove(underlying)
    except:
        pass

print ('Error encountered in:', errorL)
result_today.to_csv(filePath, index = False)
