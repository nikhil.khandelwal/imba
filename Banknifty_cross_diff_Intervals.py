'''
Banknifty (Thursday) price rise at different intervals (12 ,1,3) using balck scholes
'''

import pandas as pd
import nsepy
import time
from datetime import datetime, timedelta
from utilities import calc_expiry, get_option_code
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import stats

from kiteconnect import KiteConnect
access_token = "xTa5lTQJ9F5YmVueI6z3nZPJ64Njl96q"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)


#############################################################################################################
def get_atmStrike(price, strike_dif=100.0):
    temp = int(int(price / strike_dif)*strike_dif)
    return temp
def black_scholes (cp, s, k, t, v=0.25, rf=0.0, div=0):
    """ Price an option using the Black-Scholes model.
    Input parameters
    s: initial stock price
    k: strike price
    t: expiration time
    v: volatility
    rf: risk-free rate
    div: dividend
    cp: +1/-1 for call/put
    
    :Return optprice: A float, option price
    """
    t = float(t)/365.0
    d1 = (math.log(s/k)+(rf-div+0.5*math.pow(v,2))*t)/(v*math.sqrt(t))
    d2 = d1 - v*math.sqrt(t)
    optprice = (cp*s*math.exp(-div*t)*stats.norm.cdf(cp*d1)) - (cp*k*math.exp(-rf*t)*stats.norm.cdf(cp*d2))
    return optprice

##############################################################################################################
f_date=datetime(2016,6,1,0,0)
t_date=datetime(2018,7,6,0,0)


data=nsepy.get_history(symbol='BANKNIFTY',start=f_date,end=t_date, index=True,futures=False).reset_index()

m=pd.DataFrame(columns=['Date', 'Open', 'Open@12','Strike@12','CrossTime_12','Price@12','Close_Price@12','Price@12-Close_Price@12', 'Open@1','Strike@1','CrossTime_1','Price@1','Close_Price@1','Price@1-Close_Price@1', 'Open@2','Strike@2','CrossTime_2','Price@2','Close_Price@2','Price@2-Close_Price@2', 'Open@3','Strike@3','Price@3','Close_Price@3','Price@3-Close_Price@3', 'Close'])

while(f_date<=t_date):
    if(f_date.weekday()==3):
        try:
            close=data[data['Date']==f_date.date()]['Close'].iloc[0]
            exp=f_date
        except:
            exp=f_date-timedelta(days=1)    
            close=data[data['Date']==exp.date()]['Close'].iloc[0]
        if(f_date==datetime(2017,10,19,0,0)):
            exp=f_date-timedelta(days=1)
        
        master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)

        ntry=4
        while(master_fo.empty and ntry>0):
            try:
                master_fo=pd.DataFrame(kite.historical_data(get_option_code('NIFTY BANK','NSE'), exp, exp+timedelta(days=1), 'minute', continuous=False))
                master_fo['date']=pd.to_datetime(master_fo['date'].dt.tz_localize(None))
                Open = master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,9,15,0)]['open'].iloc[0]
                open12 = master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,12,0,0)]['open'].iloc[0]
                open1 = master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,13,0,0)]['open'].iloc[0]
                open2 = master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,14,0,0)]['open'].iloc[0]
                open3 = master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,15,0,0)]['open'].iloc[0]
                close=master_fo[master_fo['date']==datetime(exp.year,exp.month,exp.day,15,29,0)]['open'].iloc[0]
                
                d1=open12-Open
                d2=open1-Open
                d3=open2-Open
                d4=open3-Open

                if(d1>0):
                    s1=math.ceil(open12/100.0)*100
                    try:
                        crossTime_12=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,12,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']>s1))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_12).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_12]['open'].iloc[0]
                        cp1=black_scholes(1,p,s1,t/24.0)
                        close_cp1=black_scholes(1,close,s1,0.1/(24.0*60.0))
                    except:
                        crossTime_12=float('nan')
                        cp1=float('nan')
                        close_cp1=float('nan')
                        
                else:
                    s1=math.floor(open12/100.0)*100
                    try:
                        crossTime_12=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,12,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']<s1))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_12).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_12]['open'].iloc[0]
                        cp1=black_scholes(-1,p,s1,t/24.0)
                        close_cp1=black_scholes(-1,close,s1,0.1/(24.0*60.0))
                    except:
                        crossTime_12=float('nan')
                        cp1=float('nan')
                        close_cp1=float('nan')
                   
                
                if(d2>0):
                    s2=math.ceil(open1/100.0)*100
                    try:
                        crossTime_1=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,13,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']>s2))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_1).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_1]['open'].iloc[0]
                        cp2=black_scholes(1,p,s2,t/24.0)
                        close_cp2=black_scholes(1,close,s2,0.1/(24.0*60.0))

                    except:
                        crossTime_1=float('nan')
                        cp2=float('nan')
                        close_cp2=float('nan')
                else:
                    s2=math.floor(open1/100.0)*100
                    try:
                        crossTime_1=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,13,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']<s2))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_1).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_1]['open'].iloc[0]
                        cp2=black_scholes(-1,p,s2,t/24.0)
                        close_cp2=black_scholes(-1,close,s2,0.1/(24.0*60.0))
                    except:
                        crossTime_1=float('nan')
                        cp2=float('nan')
                        close_cp2=float('nan')


                if(d3>0):
                    s3=math.ceil(open2/100.0)*100
                    try:
                        crossTime_2=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,14,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']>s3))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_2).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_2]['open'].iloc[0]
                        cp3=black_scholes(1,p,s3,t/24.0)
                        close_cp3=black_scholes(1,close,s3,0.1/(24.0*60.0))
                    except:
                        crossTime_2=float('nan')
                        cp3=float('nan')
                        close_cp3=float('nan')
                else:
                    s3=math.floor(open2/100.0)*100
                    try:
                        crossTime_2=master_fo[((master_fo['date']>datetime(exp.year,exp.month,exp.day,14,0,0))& (master_fo['date']<datetime(exp.year,exp.month,exp.day,15,30,0))&(master_fo['open']<s3))]['date'].head(1).iloc[0]
                        t=((datetime(exp.year,exp.month,exp.day,15,30,0)-crossTime_2).seconds)/3600.0
                        p=master_fo[master_fo['date']==crossTime_2]['open'].iloc[0]
                        cp3=black_scholes(-1,open2,s3,t/24.0)
                        close_cp3=black_scholes(-1,close,s3,0.1/(24.0*60.0))
                    except:
                        crossTime_2=float('nan')
                        cp3=float('nan')
                        close_cp3=float('nan')

                if(d4>0):
                    s4=math.ceil(open3/100.0)*100
                    cp4=black_scholes(1,open3,s4,0.5/24.0)
                    close_cp4=black_scholes(1,close,s4,0.1/(24.0*60.0))
                else:
                    s4=math.floor(open3/100.0)*100
                    cp4=black_scholes(-1,open3,s4,0.5/24.0)
                    close_cp4=black_scholes(-1,close,s4,0.1/(24.0*60.0))

                m.loc[len(m)]=[exp.date(),Open,open12,s1,crossTime_12,cp1,close_cp1,close_cp1-cp1,open1,s2,crossTime_2,cp2,close_cp2,close_cp2-cp2,open2,s3,crossTime_2,cp3,close_cp3,close_cp3-cp3,open3,s4,cp4,close_cp4,close_cp4-cp4,close]
#                m.loc[len(m)]=[exp.date(),Open,open12,s1,crossTime_12,open1,s2,crossTime_1,open2,s3,crossTime_2,close]
             
            except KeyError:
                print "Please wait. Trying to fetch data from nsepy...",exp
                time.sleep(3)
                pass
            ntry-=1
    f_date=f_date+timedelta(days=1)


################################################################################
m1=m.copy().fillna(0)
m1[m1['Price@12-Close_Price@12']>0]['Price@12-Close_Price@12'].count()
m1[m1['Price@12-Close_Price@12']==0]['Price@12-Close_Price@12'].count()
m1[m1['Price@12-Close_Price@12']<0]['Price@12-Close_Price@12'].count()

plt.subplot(2, 1, 1).set_title("With Cross Time Calculation")
plt.plot(np.cumsum(m1['Price@12-Close_Price@12']))
plt.plot(np.cumsum(m1['Price@1-Close_Price@1']))
plt.plot(np.cumsum(m1['Price@2-Close_Price@2']))
#plt.plot(np.cumsum(m1['Price@3-Close_Price@3']))


m2=pd.DataFrame(m1[m1['CrossTime_2']!=0][['CrossTime_2','Date']].reset_index(drop=True))
m2['time']=''
for i in range(len(m2)):
    m2['time'][i]=(m2['CrossTime_2'][i]-datetime(m2['Date'][i].year,m2['Date'][i].month,m2['Date'][i].day,14,0,0)).seconds

(m2['time'].mean())/60
pd.to_timedelta(m2['CrossTime_2']).values.astype(np.int64).mean()

m1['new12']=''
m1['new1']=''
m1['new2']=''
for i in range(len(m1)):
    a12=pd.DataFrame(m1['Price@12-Close_Price@12'][0:i])
    a1=pd.DataFrame(m1['Price@1-Close_Price@1'][0:i])
    a2=pd.DataFrame(m1['Price@2-Close_Price@2'][0:i])

    pos12=a12[a12['Price@12-Close_Price@12']>0]['Price@12-Close_Price@12'].count()
    zero12=a12[a12['Price@12-Close_Price@12']==0]['Price@12-Close_Price@12'].count()
    neg12=a12[a12['Price@12-Close_Price@12']<0]['Price@12-Close_Price@12'].count()

    pos1=a1[a1['Price@1-Close_Price@1']>0]['Price@1-Close_Price@1'].count()
    zero1=a1[a1['Price@1-Close_Price@1']==0]['Price@1-Close_Price@1'].count()
    neg1=a1[a1['Price@1-Close_Price@1']<0]['Price@1-Close_Price@1'].count()

    pos2=a2[a2['Price@2-Close_Price@2']>0]['Price@2-Close_Price@2'].count()
    zero2=a2[a2['Price@2-Close_Price@2']==0]['Price@2-Close_Price@2'].count()
    neg2=a2[a2['Price@2-Close_Price@2']<0]['Price@2-Close_Price@2'].count()

    try:
        m1['new12'][i]=(float(pos12+zero12+neg12)/float(pos12+neg12))*m1['Price@12-Close_Price@12'][i]
        m1['new1'][i]=(float(pos1+zero1+neg1)/float(pos1+neg1))*m1['Price@1-Close_Price@1'][i]
        m1['new2'][i]=(float(pos2+zero2+neg2)/float(pos2+neg2))*m1['Price@2-Close_Price@2'][i]
    except:
        m1['new12'][i]=0.0
        m1['new1'][i]=0.0
        m1['new2'][i]=0.0

plt.plot(np.cumsum(m1['new12']))
plt.plot(np.cumsum(m1['new1']))
plt.plot(np.cumsum(m1['new2']))
plt.plot((m1['new']))

#for i in range(96,len(m)):
#    print "CE",m['Date'][i],m['Strike@12'][i],m['Price@12'][i],(nsepy.get_history(symbol="BANKNIFTY",start=m['Date'][i],end=m['Date'][i], index=True,option_type='CE',
#                                  strike_price=m['Strike@12'][i], expiry_date=m['Date'][i]))['Open'].iloc[0]
#    print "PE",m['Date'][i],m['Strike@12'][i],m['Price@12'][i],(nsepy.get_history(symbol="BANKNIFTY",start=m['Date'][i],end=m['Date'][i], index=True,option_type='PE',
#                                  strike_price=m['Strike@12'][i], expiry_date=m['Date'][i]))['Open'].iloc[0]

################################################################################
