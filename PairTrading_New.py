'''
Pair Trading New Strategy 
single pair calculations
'''
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from utilities1 import plot_data, plot_daily, VARanalysis, hurst, LR
from statsmodels.tsa.stattools import adfuller
from sklearn import linear_model
from statsmodels.tsa.api import VAR, DynamicVAR
from statsmodels.tsa.stattools import coint
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import time
import nsepy
import matplotlib.mlab as mlab
from scipy.stats import norm
from tabulate import tabulate

def get_sector(underlying):
    data = pd.read_excel('C:/Users/Nikhil/files/sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
def pull_data(stck1,curr_date,to_date):
    data_stcks = {}
    curr_date=curr_date.replace(hour=9,minute=15,second=0,microsecond=0)
    to_date=to_date.replace(hour=9,minute=15,second=0,microsecond=0)
    print("Calculating data....")
    for i in range(len(stck1)):
        try:
            indx=False
            if stck1[i] in ['NIFTY', 'BANKNIFTY']:
                indx=True
            if stck1[i] =='NIFTY':
                tradingsym= 'NIFTY 50'
            elif stck1[i] =='BANKNIFTY':
                tradingsym= 'NIFTY BANK'
            else:
                tradingsym = stck1[i]
            print tradingsym    
            ntry=6
            df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(df_stck1.empty and ntry>0):
               try:
                   df_stck1= nsepy.get_history(symbol=stck1[i],start=curr_date, end=to_date,index=indx)
               except AttributeError:
                   time.sleep(5)
            if max(df_stck1['High'][0:-1].reset_index(drop=True)/df_stck1['Low'][1:].reset_index(drop=True))>2:
                print stck1[i], 'Splited'
                
            ntry-=1
            data_stcks[stck1[i]] = df_stck1.copy().reset_index()
            data_stcks[stck1[i]]['Sector']=get_sector(stck1[i].upper())
        except Exception:
            print stck1[i]
            pass
    print("Data calculation finish.")
    return data_stcks

#############################################################################
root=''
#############################################################################

def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'buy'),'buy'] = 1
    df.loc[(df['action'] == 'short'),'short'] = 1
    df.loc[(df['action'] == 'cover'),'cover'] = 1
    df.loc[(df['action'] == 'sell'),'sell'] = 1

    flag=0
    df['pos']=''
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df

###############################################################################
def calc_pnl(df1):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['pnl']=0
    curr_pos=0
    prev_pos=0
    for p in range(1,len(df1)): 
        if (df1.loc[p,'pos'] != df1.loc[p-1,'pos']):
            curr_pos=p
            if prev_pos=='':
                prev_pos=curr_pos
            else:    
                if df1.loc[curr_pos,'pos']==1:
                    buy1=df1.loc[curr_pos,'close1'] 
                    buy2=df1.loc[curr_pos,'close2']
                    if df1.loc[prev_pos,'pos']==-1:
                        cover1=df1.loc[curr_pos,'close1'] 
                        cover2=df1.loc[curr_pos,'close2']
                        short1=df1.loc[prev_pos,'close1'] 
                        short2=df1.loc[prev_pos,'close2']
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))

                elif df1.loc[curr_pos,'pos']==0:   
                    if df1.loc[prev_pos,'pos']==1:
                        buy1=df1.loc[prev_pos,'close1'] 
                        buy2=df1.loc[prev_pos,'close2']
                        sell1=df1.loc[curr_pos,'close1'] 
                        sell2=df1.loc[curr_pos,'close2']
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                    elif df1.loc[prev_pos,'pos']==-1:
                        short1=df1.loc[prev_pos,'close1'] 
                        short2=df1.loc[prev_pos,'close2']
                        cover1=df1.loc[curr_pos,'close1'] 
                        cover2=df1.loc[curr_pos,'close2']
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                
                elif df1.loc[curr_pos,'pos']==-1:
                    short1=df1.loc[curr_pos,'close1'] 
                    short2=df1.loc[curr_pos,'close2']
                    
                    if df1.loc[prev_pos,'pos']==1:
                        sell1=df1.loc[curr_pos,'close1'] 
                        sell2=df1.loc[curr_pos,'close2']
                        buy1=df1.loc[prev_pos,'close1'] 
                        buy2=df1.loc[prev_pos,'close2']
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                        
                prev_pos=curr_pos
    return df1
    
####################################################################################################    
def top_stock_pnl(temp,entryZscore,exitZscore,stoploss):
    
    temp['action']=''
    temp['last_action']=''
    lastaction=''
    prev_zscore=0

    for i in range(1,len(temp)):
        close=temp.loc[i,'zScore']
        prev_zscore=temp.loc[i-1,'zScore']
        if temp.loc[i-1,'action']!='do nothing':
            lastaction=temp.loc[i-1,'action']       
        temp.loc[i,'last_action']=lastaction

        
        if (entryZscore>close and prev_zscore >entryZscore and lastaction!='buy' and lastaction!='short'):
            temp.loc[i,'action']='short'
            
        elif(-entryZscore<close and prev_zscore < -entryZscore and lastaction!='short' and lastaction!='buy'):
            temp.loc[i,'action']='buy'
        
        elif(lastaction=='short'):
            if(close<0):
                temp.loc[i,'action']='cover'
            elif close > stoploss:    
                temp.loc[i,'action']='cover'
        elif(lastaction=='buy' ):
            if(close>0):
                temp.loc[i,'action']='sell'
            elif close<-stoploss:    
                temp.loc[i,'action']='sell'
                
        if(temp.loc[i,'action']==''):
            temp.loc[i,'action']='do nothing'

    temp=cal_pos(temp)        
    temp=calc_pnl(temp)
    return temp
###############################################################################
def calc_unpnl(df1):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['unr_pnl']=0
    p=0
    while(p<len(df1)):
        if df1.loc[p,'pos']==1:
            buy1=df1.loc[p,'close1'] 
            buy2=df1.loc[p,'close2']
            p=p+1
            while p<len(df1) and df1.loc[p,'pos']==1:
                sell1=df1.loc[p,'close1'] 
                sell2=df1.loc[p,'close2']
                df1.loc[p,'unr_pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2))
                p+=1
        
        elif df1.loc[p,'pos']==-1:
            short1=df1.loc[p,'close1'] 
            short2=df1.loc[p,'close2']
            p+=1
            while p<len(df1) and df1.loc[p,'pos']==-1:
                cover1=df1.loc[p,'close1'] 
                cover2=df1.loc[p,'close2']
                df1.loc[p,'unr_pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                p+=1                   
        p+=1        
    return df1
###################################################################################
    
def calc_strategy_params(pnl_list,base_price=1,time_period = 'daily'):
    
    avg_profit = sum(pnl_list)/float(len(pnl_list))
    max_drawdown = 0
    temp_sum= 0
    for i in range(len(pnl_list)):
        if temp_sum+pnl_list[i]<0:
            temp_sum=temp_sum + pnl_list[i]
        else:
            temp_sum = 0
        if temp_sum<max_drawdown:
            max_drawdown =temp_sum
            

    risk_free_ratio = 7
    if time_period == 'monthly':
        scaling_factor = 12.0
    elif time_period == 'daily':
        scaling_factor =255.0
    else:
        scaling_factor = 1.0
        
    sharpe_ratio = np.sqrt(scaling_factor)*((avg_profit/base_price)*100 - risk_free_ratio/scaling_factor)/np.std([x*100 / base_price for x in pnl_list])
    return sharpe_ratio,max_drawdown

###############################################################################
def calc_n(data):
    data['n']=0
    for i in range(1,len(data)):
        
        if data.loc[i,'pos']!=0 and data.loc[i,'pos']!='':
            data.loc[i,'n']=2                            
            if data.loc[i-1,'n']!=0:
                data.loc[i,'n']=data.loc[i-1,'n']    
        else:
            data.loc[i,'n']=0
        
        start_n=data.loc[i,'n']
        if (start_n!=0):
            if data.loc[i,'pos']==1:
                if -0.5<data.loc[i,'zScore']<0:
                    data.loc[i,'n']=1
                elif data.loc[i,'zScore']>0 or data.loc[i,'zScore']<-4 :
                    data.loc[i,'n']=0
                elif -0.5<data.loc[i-1,'zScore']<0 and -1<data.loc[i,'zScore']<-0.5:
                    data.loc[i,'n']=1
                elif -1<data.loc[i-1,'zScore']<0 and -3<data.loc[i,'zScore']<-1:
                    data.loc[i,'n']=2
                elif -3<data.loc[i-1,'zScore']<-1 and -4<data.loc[i,'zScore']<-3:
                    data.loc[i,'n']=3
                elif -4<data.loc[i-1,'zScore']<-3 and -3<data.loc[i,'zScore']<-1:
                    data.loc[i,'n']=3
                elif -4<data.loc[i-1,'zScore']<-3 and -1<data.loc[i,'zScore']<-0.5:
                    data.loc[i,'n']=2
                
            elif data.loc[i,'pos']==-1:
                if 0<data.loc[i,'zScore']<0.5:
                    data.loc[i,'n']=1
                elif data.loc[i,'zScore']<0 or data.loc[i,'zScore']>4 :
                    data.loc[i,'n']=0
                elif 0<data.loc[i-1,'zScore']<0.5 and 0.5<data.loc[i,'zScore']<1:
                    data.loc[i,'n']=1
                elif 0<data.loc[i-1,'zScore']<1 and 1<data.loc[i,'zScore']<3:
                    data.loc[i,'n']=2
                elif 1<data.loc[i-1,'zScore']<3 and 3<data.loc[i,'zScore']<4:
                    data.loc[i,'n']=3
                elif 3<data.loc[i-1,'zScore']<4 and 1<data.loc[i,'zScore']<3:
                    data.loc[i,'n']=3
                elif 3<data.loc[i-1,'zScore']<4 and 0.5<data.loc[i,'zScore']<1:
                    data.loc[i,'n']=2
            elif data.loc[i,'pos']=='':
                data.loc[i,'n']=0

        elif(start_n==0):
            if data.loc[i-1,'pos']==data.loc[i,'pos']:
                data.loc[i,'n']=0
            elif data.loc[i-1,'pos']==0 and (data.loc[i,'pos']==1 or data.loc[i,'pos']==-1):
                data.loc[i,'n']=2
            elif data.loc[i,'pos']=='':
                data.loc[i,'n']=0

    return data 
#######################################################################################################
    
s1='JUSTDIAL'
s2='TATAGLOBAL'
stocks_list = [s1, s2,'NIFTY']
data_stcks = pull_data(stocks_list,datetime(2014,1,1,0,0),datetime(2017,1,1,0,0))
stocks = [s1, s2,'NIFTY']

for i in stocks:
    if i=='NIFTY' or i=='NIFTYBANK':
        continue
    f = [0]
    for j in range(1, len(data_stcks[i]['Close'])):
        f.append((data_stcks[i]['Close'][j]- data_stcks[i]['Close'][j-1])/data_stcks[i]['Close'][j])
    data_stcks[i]['Change'] = f
    
#############PLOT################
#plt.subplot(3, 1, 1)
#plot_data(data_stcks,stocks)
#
#plt.subplot(3, 1, 2)
#plot_daily(data_stcks,stocks)
############Stability Check###################
#adfuller(data_stcks[s1]['Close'])
#adfuller(data_stcks[s1]['Change'])
#################################################
f = pd.DataFrame()
for j in stocks:
    if j=='NIFTY' or j=='NIFTYBANK':
        continue
    f[j] = data_stcks[j]['Change']
    print j
    
f.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])

model = VAR(f)
results = model.fit(1)
results.summary()
#results.plot()
model.select_order(15)

aic, bic, A, residue_cov_matrix = VARanalysis(f,1)
e,v = np.linalg.eig(A[1:])

######Stability###########
for eig in e:
    if np.vdot(eig,eig)>1:
        print 'Not Stable'
        
############# EG Procedure##############
stock1 = data_stcks[s1]['Close']/data_stcks[s1]['Close'][0]
stock2 = data_stcks[s2]['Close']/data_stcks[s2]['Close'][0]
stock1.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
stock2.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
#stock1=stock1[stock1.index<datetime(2016,1,1,0,0)]
#stock2=stock2[stock2.index<datetime(2016,1,1,0,0)]

x = stock1.values
y = stock2.values
length = len(x)
x = x.reshape(length, 1)
y = y.reshape(length, 1)
regr = linear_model.LinearRegression()
regr.fit(x, y)
regr.coef_
regr.intercept_
error = (regr.predict(x) - y)
#plt.scatter(x, y,  color='black')
#plt.plot(x, regr.predict(x), color='blue', linewidth=3)
#plt.plot(error, color='blue', linewidth=3)
errorl = pd.DataFrame()
error = list(error.T[0])
errorl['e'] = error
errorl.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
#errorl.index = pd.DatetimeIndex(data_stcks['NIFTY'][data_stcks['NIFTY']['Date']<datetime(2016,1,1,0,0).date()]['Date'])

adfuller(errorl['e'])
#coint(stock1,stock2)
#print "Hurst Exponent =",round(hurst(errorl),2)

############EG Procedure###############3
deltay = stock2.diff().dropna()
deltax = stock1.diff().dropna()
X = errorl.shift(1).dropna()
X['dx'] = deltax
lr=LR(X, deltay)

####################
errorl['e1'] = errorl.shift(1)
errorl = errorl.dropna()
#lm = LinearRegression()
#lm.fit(errorl[1:],errorl.shift(1).dropna(0))
m = VAR(errorl)
results = m.fit(1)
results.summary()

B=results.params['e']['L1.e']

#B = 0.932303 ####manul####
tau = 1/252.0
theta = -np.log(abs(B))/tau
H = np.log(2)/theta

#ue = lm.intercept_[0]/(1-B)
roll = H/tau
beta = [1, -regr.coef_[0][0]]
spread = beta[0]*stock1 + beta[1]*stock2
#plt.plot(spread)
ub = spread.mean()
ustd  = spread.std()

data=pd.DataFrame()
data['Date']=spread.index
data['close1']=data_stcks[s1]['Close']
data['close2']=data_stcks[s2]['Close']
spread=spread.reset_index(drop=True)
meanSpread = spread.rolling(window=int(roll)).mean()
stdSpread = spread.rolling(window=int(roll)).std()
    

plt.plot(meanSpread+stdSpread)
plt.plot(meanSpread-stdSpread)
plt.plot(meanSpread)

data['zScore'] = (spread-meanSpread)/stdSpread
data['NiftyClose']=data_stcks['NIFTY']['Close']
data['Nifty_AdjClose']=data['NiftyClose']-data['NiftyClose'][0]

def calc_data(df,entryZscore,exitZscore=0,stoploss=4):    
    df=top_stock_pnl(df,entryZscore,exitZscore,stoploss)
    df=calc_unpnl(df)
    df['rpnl']=0
    for e in range(len(data)):
        if df.loc[e,'pnl']!=0:
            df.loc[e,'rpnl']=df.loc[e,'pnl']-0.001*(df.loc[e,'close1']+abs(beta[1])*df.loc[e,'close2'])
        else:
            df.loc[e,'rpnl']=0
    df['PNL']=np.cumsum(df['pnl'])+df['unr_pnl']
    return df


'''
############### PLOT for different entryZscore############################################################ 
data5=calc_data(data.copy(),0.5)
data1=calc_data(data.copy(),1)
data15=calc_data(data.copy(),1.5)
data2=calc_data(data.copy(),2)
data25=calc_data(data.copy(),2.5)

plt.subplot(3, 2, 1).set_title('entryZscore->0.5, transactions= '+str(np.count_nonzero(data5['pnl'])))
plt.plot((np.cumsum(data['pnl'])+data['unr_pnl'])/np.mean(data['close1']),label='without transaction')
plt.plot((np.cumsum(data['rpnl'])+data['unr_pnl'])/np.mean(data['close1']),label='with transaction')
plt.legend()
plt.xlabel("days")
plt.ylabel("Pnl")
plt.grid(True)
plt.xticks(rotation=45)

for labels in ax1.xaxis.get_ticklabels():
    labels.set_rotation(45)
    
plt.subplot(3, 2, 2).set_title('entryZscore->1, transactions= '+str(np.count_nonzero(data1['pnl'])))
plt.plot((np.cumsum(data1['pnl'])+data1['unr_pnl'])/np.mean(data1['close1']))
plt.plot((np.cumsum(data1['rpnl'])+data1['unr_pnl'])/np.mean(data1['close1']))
plt.legend(['without transaction','with transaction'])

plt.subplot(3, 2, 3).set_title('entryZscore->1.5, transactions= '+str(np.count_nonzero(data15['pnl'])))
plt.plot((np.cumsum(data15['pnl'])+data15['unr_pnl'])/np.mean(data15['close1']))
plt.plot((np.cumsum(data15['rpnl'])+data15['unr_pnl'])/np.mean(data15['close1']))
plt.legend(['without transaction','with transaction'])

plt.subplot(3, 2, 4).set_title('entryZscore->2, transactions= '+str(np.count_nonzero(data2['pnl'])))
plt.plot((np.cumsum(data2['pnl'])+data2['unr_pnl'])/np.mean(data2['close1']))
plt.plot((np.cumsum(data2['rpnl'])+data2['unr_pnl'])/np.mean(data2['close1']))
plt.legend(['without transaction','with transaction'])

plt.subplot(3, 2, 5).set_title('entryZscore->2.5, transactions= '+str(np.count_nonzero(data25['pnl'])))
plt.plot((np.cumsum(data25['pnl'])+data25['unr_pnl'])/np.mean(data25['close1']))
plt.plot((np.cumsum(data25['rpnl'])+data25['unr_pnl'])/np.mean(data25['close1']))
plt.legend(['without transaction','with transaction'])

############### PLOT for different entryZscore############################################################ 
'''
data=calc_data(data.copy(),1.8)
for k in range(1,len(data)):
    data.loc[k,'daily_pnl']=data.loc[k,'PNL']-data.loc[k-1,'PNL']
    


'''
############################ CALCULATE %Change ################################################
data['%change']=0
data['%Niftychange']=0
for l in range(1,len(data)):
    data.loc[l,'%change']=((data.loc[l,'PNL']-data.loc[l-1,'PNL'])/data.loc[l-1,'PNL'])*100
    data.loc[l,'%Niftychange']=((data.loc[l,'Nifty_AdjClose']-data.loc[l-1,'Nifty_AdjClose'])/data.loc[l-1,'Nifty_AdjClose'])*100
    
############################ CALCULATE %Change ###########################################################
'''

'''

############################ CALCULATE BETA using risk free rate ##########################
#risk free rate=2% 

data['beta']=0
data['b1']=0
data['b2']=0
for m in range(100,len(data)):
    b1=(data.loc[m,'PNL']-data.loc[m-100,'PNL'])/1200 -0.02
    b2=(data.loc[m,'Nifty_AdjClose']-data.loc[m-100,'Nifty_AdjClose'])/600 -0.02
    data.loc[m,'b1']=b1
    data.loc[m,'b2']=b2
    data.loc[m,'beta']=data.loc[m,'b1']/data.loc[m,'b2']
############################ CALCULATE BETA using %Change #################################

############### Smoothing Beta values    
for n in range(100,len(data)):
    if abs(data.loc[n,'beta']-data.loc[n-1,'beta'])>1:
        data.loc[n,'beta']=np.mean([data.loc[n-1,'beta'],data.loc[n+1,'beta']])

############### Smoothing Beta values  ########################################  

'''


############### Sharpe Ration AND Max Drawdown    

data['sharpe_ratio']=float('nan')
data['max_drawdown']= float('nan')
for p in range(100,len(data)):
    a,b=calc_strategy_params(data['daily_pnl'][p-100:p].reset_index(drop=True))
    data.loc[p,'sharpe_ratio']=a
    data.loc[p,'max_drawdown']= b
#plt.plot(data['max_drawdown'])
#plt.plot(data['sharpe_ratio'])
############### Sharpe Ration AND Max Drawdown  ###############################
data=calc_n(data.copy())    

#n=0
#
#for q in range(len(data)):
#    n=data.loc[q,'n']
#    pos=data.loc[q,'pos']
#    action=data.loc[q,'action']
#    while(pos=='' or action==''):
#        continue
#    
#    data['new_pnl']
#    data['new_upnl']

data['n']=data['pos']*data['n']    
data.loc[data['n']=='','n']=0

data['n1']=0
data['n2']=0
data['n3']=0

data.loc[data['n']>=1,'n1']=1
data.loc[data['n']>=2,'n2']=1
data.loc[data['n']==3,'n3']=1

data.loc[data['n']<=-1,'n1']=-1
data.loc[data['n']<=-2,'n2']=-1
data.loc[data['n']==-3,'n3']=-1

sample_data=pd.DataFrame()
sample_data[['Date','close1','close2','pos']]=data[['Date','close1','close2','n1']]
data['pnl1']=calc_pnl(sample_data)['pnl']
data['unr_pnl1']=calc_unpnl(sample_data)['unr_pnl']

sample_data=pd.DataFrame()
sample_data[['Date','close1','close2','pos']]=data[['Date','close1','close2','n2']]
data['pnl2']=calc_pnl(sample_data)['pnl']
data['unr_pnl2']=calc_unpnl(sample_data)['unr_pnl']
    
sample_data=pd.DataFrame()
sample_data[['Date','close1','close2','pos']]=data[['Date','close1','close2','n3']]
data['pnl3']=calc_pnl(sample_data)['pnl']
data['unr_pnl3']=calc_unpnl(sample_data)['unr_pnl']

data['final_pnl']=data['pnl1']+data['pnl2']+data['pnl3']
data['final_unr_pnl']=data['unr_pnl1']+data['unr_pnl2']+data['unr_pnl3']

plt.plot(np.cumsum(data['pnl'])+data['unr_pnl'])
plt.plot((np.cumsum(data['final_pnl'])+data['final_unr_pnl'])/1.825)

data['final_PNL']=np.cumsum(data['final_pnl'])+data['final_unr_pnl']
for k in range(1,len(data)):
    data.loc[k,'final_daily_pnl']=data.loc[k,'final_PNL']-data.loc[k-1,'final_PNL']

data['final_sharpe_ratio']=float('nan')
for p in range(100,len(data)):
    a,b=calc_strategy_params(data['final_daily_pnl'][p-100:p].reset_index(drop=True))
    data.loc[p,'final_sharpe_ratio']=a

#plt.plot(data['sharpe_ratio'])
#plt.plot(data['final_sharpe_ratio'])
#abs(data[data['n']!=0]['n']).mean()

'''
import scipy.optimize as spo
def f(w):    
    a=calc_data(data.copy(),w)
    return -a['PNL'][-1:].iloc[0]

Cguess=np.poly1d([1.5])
cons = ({'type': 'ineq', 'fun': lambda x: -x[0]+0 },
        {'type': 'ineq', 'fun': lambda x: -4+x[0] })
spo.minimize(f,Cguess,method='SLSQP',options={'disp':True},constraints=cons)


###############################return value 1.8
spo.minimize_scalar(f, bounds=(0, 4), method='bounded')
'''

###############################################################################
'''                    Calculating Value at Risk                           '''


lst=data[['close1','close2','daily_pnl']].fillna(0)
beta = [1, -regr.coef_[0][0]]
lst['beta']=(beta[0]*lst['close1'] + abs(beta[1])*lst['close2'])
lst['pnl']= lst['daily_pnl']/lst['beta']
lst1=lst[lst['pnl']!=0.0]['pnl'].reset_index(drop=True)

#plt.hist(lst1,bins = np.linspace(min(lst1), max(lst1),100))

lst1.hist(bins=50,density=True,histtype='stepfilled',alpha=0.5)


mean=np.mean(lst1)
stdev=np.std(lst1)
x=np.linspace(mean-3*stdev,mean+3*stdev,100)
plt.plot(x,mlab.normpdf(x,mean,stdev),"r")
plt.show()

VaR_90=norm.ppf(1-0.9,mean,stdev)
VaR_95=norm.ppf(1-0.95,mean,stdev)
VaR_99=norm.ppf(1-0.99,mean,stdev)

print tabulate([['90%',VaR_90],['95%',VaR_95],['99%',VaR_99]],headers=['Confidence lever','value at risk'])

###############################################################################
