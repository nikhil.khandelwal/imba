'''
Daily Dashboard 

'''
from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime, timedelta
import time
from utilities import get_option_code, get_vol_nsepy,get_strike_diff,get_pain_anyDate,calc_expiry
import math
import statistics
import nsepy
from calendar import monthrange
import scipy

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import Encoders
##################################################################
access_token = "lNKnwmGm5JSLJdsRAIfHoKCbLbM3lIhv"
from_date = datetime(2018, 5, 18 ,9, 15, 0)
to_date = datetime(2018, 5, 21, 9, 15, 0)
#from_date=datetime.now().replace(hour=9,minute=15,second=0,microsecond=0)-timedelta(days=1)
#to_date=from_date
##################################################################
#stocks=pd.read_csv("dashboard_stocks.csv")
#stocks_list = list(stocks.ix[0])
stocks=pd.read_csv("nifty200.csv",usecols=[2])
stocks_list = list(stocks['Symbol'])

#stocks_list.extend(['NIFTY', 'BANKNIFTY'])

def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
def get_atmStrike(price, strike_dif):
    try:
        temp = int(int(price / strike_dif)*strike_dif)
    except:
        return 0
    if underlying =='NIFTY' or underlying =='BANKNIFTY':
        return temp
    data = pd.read_excel('strike_price.xlsx')
    baseP = data[data['Ticker ']==underlying]['Strike Price'].iloc[0]
    if int((temp - baseP)/strike_dif) == (temp - baseP)/strike_dif:
        return temp
    else:
        print (underlying)
        return int(baseP + int((temp - baseP)/strike_dif)*baseP)
def getprevdayData(underlying, result_yesterday):
    try:
        data  = result_yesterday[result_yesterday['Sciptname']==underlying].iloc[0]
        return data['pain'], data['change oi'], data['pain_price_ratio'], data['vol_ce'], data['vol_pe'], data['close'],data['P-C ratio']
    except:
        return 0,0,0,0,0,0,0
def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r
def calc_last_thursday(curr_date):
    a,max_days = monthrange(curr_date.year, curr_date.month) 
    while(max_days>20):
        curr_date=curr_date.replace(day=max_days)
        if(curr_date.weekday()==3):
            return curr_date
        else:
            max_days-=1    

api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
exch="NFO"
exch2 ="NSE"
if(from_date>to_date):
    raise ValueError('from_date cannot be greater than to_date')


ntry=4
indexData1=pd.DataFrame(data=None,index=None,columns=None)
while(indexData1.empty and ntry>0):
    try:
        indexData1=nsepy.get_history(symbol='NIFTY 50',start=from_date.replace(year=from_date.year-5),end=to_date, index=True,futures=False)
#        indexData1  = pd.DataFrame(kite.historical(get_option_code('NIFTY 50', exch2),fdate, tdate, 'day'))
    except Exception:
        time.sleep(100)
        pass
    ntry-=1
indexData1.columns=map(str.lower, indexData1.columns)
if (from_date.weekday()>4):
    curr_date=from_date+timedelta(days=(7-from_date.weekday()))
else:
    curr_date=from_date

    
while(curr_date<=to_date):
    while(indexData1[indexData1.index==curr_date.date()].empty and curr_date<=to_date):
        curr_date+=timedelta(days=1)

    opt_maturity=calc_expiry(curr_date).replace(hour=15,minute=30)     
    prev_expiry=calc_expiry(curr_date,'prev').replace(hour=9,minute=15)     
    
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    maturity = str(cur_year)+cur_month
    prev_exp_str = prev_expiry.strftime('%Y-%m-%dT%H:%M:%S+0530')
    to_date_now = datetime.strftime(curr_date, "%Y-%m-%d")
    
    yesterday_date = curr_date - timedelta(days = 1)
    
    while(indexData1[indexData1.index==yesterday_date.date()].empty and yesterday_date>=from_date-timedelta(days=10)):
        yesterday_date-=timedelta(days=1)

    yesterday_date = datetime.strftime(yesterday_date, "%Y-%m-%d")

    from_date_now_min = datetime.strftime(curr_date-timedelta(days=29), "%Y-%m-%d")
    from_date_now = datetime.strftime(curr_date.replace(year = datetime.now().year-5), "%Y-%m-%d")
    root='C:/Users/Administrator/Desktop/zerodha/dashboard/'
#    root='C:/Users/DELL/Commodity_OptionData/'

    curr_time = datetime.strftime(datetime.now(), "%H:%M:%S")
    filePath = root+ to_date_now+'.csv'
#    filePath = to_date_now+'.csv'
    filePath_yesterday = root+yesterday_date+'.csv'
#    filePath_yesterday = yesterday_date+'.csv'
    
    columns = ['Sciptname','Date','Sector','Standard deviation(last 5 Days)',
               'Gap Return(last day 3:20 to today 9:20)','lastHour/TotalDay Return',
               'Return(1-3:30)-Return(9:20-1)','R-square(5 min candle)',
               'today(9:20-10)','today(9:20-11)','today(9:20-1)',
               'today(9:20-2)','today(9:20-3)',
               'open', 'high', 'low','close','lastDayClose','IntraDay_return'
               , 'today_return', '5day_return', 'niftyReturn', 'beta', 'premium', 
               '5d avg_premium', 'Last Day Premium'
               ,'%delivery','deliveryAvg_5', 'deliveryAvg_10', 'deliveryAvg_30'
               , 'volume_inc_perc', 'OI','change oi','prev_change OI', 'atm_strike', 'vol_ce', 'vol_pe', 'avg_vol','change ce_vol','change pe_vol','Last Day CallIV-PutIV', 'pain', 'Last Pain','pain_price_ratio', 'last pain_price ratio'
               ,'5yr high', '5yr_low', '50d high', '50d low', '100d high', '100d low', 'max_ce_oi', 'max_pe_oi', 'prev_expiry','P-C ratio','change P-C ratio','fo_view']
        
    result_today = pd.DataFrame(columns = columns)
    try:
        result_yesterday = pd.read_csv(filePath_yesterday)
    except:
        result_yesterday = pd.DataFrame(columns = columns)
        
    errorL = []
    error_run = 0
    if error_run == 1 and len(errorL)>0:
        run_l = errorL[:]
        result_today = pd.read_csv(filePath)
    else:
        run_l = stocks_list[:]
        result_today = pd.DataFrame(columns = columns)
        
    indexData  = indexData1[indexData1.index>=datetime.strptime(from_date_now, "%Y-%m-%d").date()]
    indexData  = indexData[indexData.index<=datetime.strptime(to_date_now, "%Y-%m-%d").date()]

    indexData_min=pd.DataFrame(kite.historical_data(get_option_code('NIFTY 50', exch2), from_date_now_min, to_date_now, 'minute', continuous=False))
#    indexData_min  = pd.DataFrame(kite.historical(get_option_code('NIFTY 50', exch2),from_date_now_min, to_date_now, 'minute'))    
    for i in range(len(run_l)):
        if len(errorL)==0:
            underlying  = stocks_list[i]
        else:
            underlying = errorL[0]
        if underlying =='NIFTY':
            tradingsym= 'NIFTY 50'
        elif underlying =='BANKNIFTY':
            tradingsym= 'NIFTY BANK'
        else:
            tradingsym = underlying
        fo_ticker = underlying+maturity+'FUT'
        fo_ticker_curr=underlying+maturity[0:2]+datetime.now().strftime("%b").upper()+'FUT'
        strike_dif = get_strike_diff(underlying)
        
        print underlying
        if underlying =='PCJEWELLER':
            continue

        ############ PULL DATA ####################
        try:
            ##########START:::CALCULATING MASTER DATA #####################
            d1=datetime.strptime(to_date_now, "%Y-%m-%d")
            f1=d1.replace(year=d1.year-1)
            master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            f_date=f1
            t_date=calc_expiry(f_date)
            if(underlying=='NIFTY'or underlying=='BANKNIFTY'):
                indx=True
            else:
                indx=False

            while(t_date<d1):                    
                t_date=calc_expiry(f_date)
                if(t_date>d1):
                    t_date=d1
                if(f_date>t_date):
                    f_date=t_date
                    
                if (master_fo.empty):
                    ntry=4
                    while(master_fo.empty and ntry>0):
                        try:
                            master_fo=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=indx,futures=True,expiry_date=calc_expiry(f_date.date()))
                        except AttributeError:
                            time.sleep(100)
                            pass
                        ntry-=1
                else:
                    mData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
                    ntry=4
                    while(mData.empty and ntry>0):
                        try:
                            mData=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=indx,futures=True,expiry_date=calc_expiry(f_date.date()))
                            master_fo=pd.concat([master_fo,mData])
                        except AttributeError as e:
                            time.sleep(100)
                            pass
                        ntry-=1
                f_date=t_date+timedelta(days=1)    
                              
            if(master_fo.empty):
                continue
            if underlying in ['NIFTY', 'BANKNIFTY']:
                master = master_fo[:][:]
                master.columns=map(str.lower, master.columns)
            else:
                ntry=4
                master=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
                while(master.empty and ntry>0):
                    try:
                        master=nsepy.get_history(symbol=underlying,start=datetime.strptime(from_date_now, "%Y-%m-%d"),end=datetime.strptime(to_date_now, "%Y-%m-%d"), index=False,futures=False)
                        master.columns=map(str.lower, master.columns)
                    except Exception:
                        time.sleep(100)
                        pass
                    ntry-=1

            ntry=6
            master_min=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(master_min.empty and ntry>0):
                try:
                    master_min=pd.DataFrame(kite.historical_data(get_option_code(tradingsym, exch2), from_date_now_min, to_date_now, 'minute', continuous=False))
#                    master_min = pd.DataFrame(kite.historical(get_option_code(tradingsym, exch2),from_date_now_min, to_date_now, 'minute'))
                except Exception:
                    time.sleep(5)
                    pass
                ntry-=1
            ########END:::CALCULATING MASTER DATA #########################          
            ###START:::NSEPY DATA #############################################
            ntry=1
            nsepyData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(nsepyData.empty and ntry>0):
                try:
                    if(underlying=='NIFTY'or underlying=='BANKNIFTY'):
                        nsepyData=nsepy.get_history(symbol=underlying,start=from_date-timedelta(days=45),end=to_date, index=True,futures=False)
                    else:    
                        nsepyData=nsepy.get_history(symbol=underlying,start=from_date-timedelta(days=45),end=to_date, index=False,futures=False)
                except Exception:
                    print "Please wait for 5 minutes. Trying to fetch data from nsepy..."
                    time.sleep(100)
                    pass
                ntry-=1
            master_min['close'][0]
            ##END::NSEPY DATA #################################################
            print underlying , 'Data Successfully pulled. Doing Calculations...'      
        except Exception as e:
            print underlying , 'Data pull failed. Moving to next... Error was:', e.__class__.__name__
            if not underlying in errorL:            
                errorL.append(underlying)
            time.sleep(5)
            continue
        ############## CALCULATIONS################
    
        sector = get_sector(underlying)
        cdate=to_date_now
        if not master_min.empty:
            try:
                tdr9to10=((master_min[master_min['date']==cdate+"T10:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
                tdr9to11=((master_min[master_min['date']==cdate+"T11:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
                tdr9to1=((master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
                tdr9to2=((master_min[master_min['date']==cdate+"T14:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
                tdr9to3=((master_min[master_min['date']==cdate+"T15:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
                nextDayGapReturn3to9=((master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T15:20:00+0530"]['close'].iloc[0]) -1)*100
                returnbtw=(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0]) -1)*100)-(((master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100)    
                lastHr_totalDay=(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T14:30:00+0530"]['close'].iloc[0]) -1)*100)/(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:15:00+0530"]['close'].iloc[0]) -1)*100)
            
            except:
                tdr9to10=0
                tdr9to11=0
                tdr9to1=0
                tdr9to2=0
                tdr9to3=0
                nextDayGapReturn3to9=0
                returnbtw=0
                lastHr_totalDay=0
        ##############R-square(5 min candle)###################################            
        q=datetime.now().replace(hour=9,minute=15,second=0,microsecond=0)
        list1=[]
        while (q.strftime("%H:%M:%S")<'15:30:00'):
            try:
                list1.append(master_min[master_min['date']==cdate+"T"+q.strftime("%H:%M:%S")+"+0530"]['close'].iloc[0])
            except:
                pass
            q+=timedelta(minutes=5)  
        slope, intercept, r_value, p_value, std_err=scipy.stats.linregress(range(len(list1)),list1)
        rsquare = (r_value) * (r_value)
        ##########Standard deviation(last 5 Days)##############################  
        lastday_return=[]
        for r in range(2,7):
            lastday_return.append((master.ix[len(master)-r]['close']/master.ix[len(master)-(r+1)]['close'] - 1)*100)
        std_dev=statistics.stdev(lastday_return)
    #################################Day Data Calculation########################
        openP = master.ix[len(master)-1]['open']
        highP = master.ix[len(master)-1]['high']
        lowP = master.ix[len(master)-1]['low']
        closeP = master.ix[len(master)-1]['close']
        lastDayClose = master.ix[len(master)-2]['close']
        IntraDay_return = (closeP/openP - 1)*100
        today_return = (closeP/lastDayClose - 1)*100
        nday_return_5 = (closeP/master.ix[len(master)-6]['close'] - 1)*100
        niftyReturn =  (indexData.ix[len(indexData)-1]['close']/indexData.ix[len(indexData)-2]['close'] - 1)*100
        indexReturn =  today_return - niftyReturn
        avg_change = master.ix[len(master)-200:len(master)]['close'].std()/master.ix[len(master)-200:len(master)]['close'].mean()
        index_avg_change = indexData.ix[len(indexData)-200:len(indexData)]['close'].std()/indexData.ix[len(indexData)-200:len(indexData)]['close'].mean()
        beta = avg_change/index_avg_change
        sector = get_sector(underlying)
        premium  = (master_fo.ix[len(master_fo)-1]['Close']/master.ix[len(master)-1]['close'] -1)*100
        avg_premium=[]
        for q in range(2,7):
            avg_premium.append((master_fo.ix[len(master_fo)-q]['Close']/master.ix[len(master)-q]['close'] -1)*100)
        avg_premium=statistics.mean(avg_premium)
        last_day_premium=(master_fo.ix[len(master_fo)-2]['Close']/master.ix[len(master)-2]['close'] -1)*100
        if (len(nsepyData)>0 and underlying not in ['NIFTY', 'BANKNIFTY']):
            deliveryperc = nsepyData.ix[len(nsepyData)-1]['%Deliverble']*100
            deliveryAvg_5 = nsepyData.ix[len(nsepyData)-5:len(nsepyData)]['%Deliverble'].mean()
            deliveryAvg_10 = nsepyData.ix[len(nsepyData)-10:len(nsepyData)]['%Deliverble'].mean()
            deliveryAvg_30 = nsepyData.ix[len(nsepyData)-30:len(nsepyData)]['%Deliverble'].mean()
            volume_inc_percentage = (float(nsepyData.ix[len(nsepyData)-1]['Volume'])/nsepyData.ix[len(nsepyData)-6:len(nsepyData)]['Volume'].mean() -1)*100
        else:
            deliveryperc=0
            deliveryAvg_5 = 0
            deliveryAvg_10 = 0
            deliveryAvg_30 = 0
            volume_inc_percentage = 0
        OI = master_fo[master_fo.index==curr_date.date()]['Open Interest'].iloc[0]
        oi_last=master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Open Interest'].iloc[0]
        high_5yr =max(master['high'])
        low_5yr = min(master['low'])
        high_50d =max(master.ix[len(master)-50:len(master)]['high'])
        high_100d =max(master.ix[len(master)-100:len(master)]['high'])
        low_50d =min(master.ix[len(master)-50:len(master)]['low'])
        low_100d =min(master.ix[len(master)-100:len(master)]['low'])
        prevexpiry_close = float(master[master.index==datetime.strptime(prev_exp_str[0:10], "%Y-%m-%d").date()]['close'].iloc[0])
        last_pain, last_change_oi, last_pain_price_dif, ce_vol_last, pe_vol_last, close_last,pc_ratio_last = getprevdayData(underlying, result_yesterday)
        if math.isnan(strike_dif) or int((strike_dif*10)/10) != strike_dif:
            print 'No option volume'
            atm_strike = 0
            vol_ce = 0
            vol_pe = 0
            avg_vol = 0
            pain = 0
            pain_price_dif = 0
            max_call_OI = 0
            max_put_OI = 0
        else:
            atm_strike = get_atmStrike(master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0],strike_dif)
            print underlying , 'Calculating Option Volitility'
            vol_ce = get_vol_nsepy(curr_date, underlying, str(atm_strike), 'CE', master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0], opt_maturity)
            if(vol_ce<=0.001):
                vol_ce=float('nan')
            vol_pe = get_vol_nsepy(curr_date, underlying, str(atm_strike), 'PE', master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0], opt_maturity)
            if(vol_pe<=0.001):
                vol_pe=float('nan') 
            avg_vol = 0.5*(vol_pe+ vol_ce)
            print underlying , 'Calculating Option Pain'
            pain, OI_data = get_pain_anyDate(underlying, curr_date)
            try:
                pc_ratio = sum(OI_data['OI_PE'])/sum(OI_data['OI_CE'])
            except ZeroDivisionError:
                pc_ratio=0
            pain_price_dif = (pain/closeP - 1)*100
            max_call_OI = OI_data['Strike'][OI_data['OI_CE'].idxmax()]
            max_put_OI = OI_data['Strike'][OI_data['OI_PE'].idxmax()]
        change_oi = calc_change(OI,oi_last)
        change_pc_ratio=calc_change(pc_ratio,pc_ratio_last)
        change_ce_vol = calc_change(vol_ce, ce_vol_last)
        change_pe_vol = calc_change(vol_pe, pe_vol_last)
        LastDay_CallPut_diff=ce_vol_last-pe_vol_last
        if OI>oi_last:
            if closeP>close_last:
                fo_view= 'Long Buildup'
            elif closeP<close_last:
                fo_view = 'Short Buildup'
            else:
                fo_view = 'Unknown'    
        elif OI<oi_last:
            if closeP>close_last:
                fo_view= 'Short Covering'
            elif closeP<close_last:
                fo_view = 'Long unwinding'
            else:
                fo_view = 'Unknown'
        else:
            fo_view = 'Unknown'      
    #############################################################################    
        result_today.loc[len(result_today)] = [underlying, cdate,sector,std_dev,
                         nextDayGapReturn3to9,lastHr_totalDay,returnbtw,rsquare,tdr9to10,
                         tdr9to11,tdr9to1,tdr9to2,tdr9to3,openP, highP, lowP,closeP,
                         lastDayClose,IntraDay_return, today_return, nday_return_5, 
                         indexReturn, beta, premium, avg_premium, last_day_premium, deliveryperc
               ,deliveryAvg_5, deliveryAvg_10, deliveryAvg_30,volume_inc_percentage, OI, 
               change_oi,last_change_oi, atm_strike, vol_ce, vol_pe, avg_vol,change_ce_vol,
               change_pe_vol,LastDay_CallPut_diff, pain, last_pain, pain_price_dif, last_pain_price_dif
               ,high_5yr, low_5yr, high_50d, low_50d, high_100d, low_100d, max_call_OI,
               max_put_OI, prevexpiry_close,pc_ratio,change_pc_ratio, fo_view]
        try:
            errorL.remove(underlying)
        except:
            pass
    
    result_today.to_csv(filePath, index = False)
   
    try:
        fname=str(curr_date.date())+'.csv'    
        msg2 = MIMEMultipart(fname)

        mail_file = MIMEBase('application', 'csv')
        mail_file.set_payload(open(fname, 'rb').read())
        mail_file.add_header('Content-Disposition', 'attachment', filename=fname)
        Encoders.encode_base64(mail_file)
        msg2.attach(mail_file)
        msg2['Subject'] = 'NIFTY200 Stocks Data - ' +str(curr_date.date()) 
        recipients=['gupta.abhishek888@gmail.com ','nkhandelwal58@gmail.com','sagar.agrwl@gmail.com']
        msg2['To'] = ", ".join(recipients)
        msg2['From'] = "nkhandelwal58@gmail.com"
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login("aletrs.imba@gmail.com", "Qwerty123*")
        server.sendmail("aletrs.imba@gmail.com",recipients, msg2.as_string())
        server.quit()
    except:
        print ("Netfail")
     
    if (curr_date.weekday()>=4):
        curr_date=curr_date+timedelta(days=(7-curr_date.weekday()))
    else:
        curr_date=curr_date+timedelta(days=1)