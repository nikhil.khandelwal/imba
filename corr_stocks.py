from kiteconnect import KiteConnect
import pandas as pd
import numpy as np
import nsepy
from datetime import datetime
import matplotlib.pyplot as plt
from utilities import get_option_code
import statistics

#from plt_basics import read_data
from scipy.stats.stats import pearsonr 
access_token = "xTa5lTQJ9F5YmVueI6z3nZPJ64Njl96q"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)

def slope(df,value):
    x = np.arange(len(df)) 
    y = list(df[value])
    m, b = np.polyfit(x, y, 1)
    return m
def cor(df1,df2,value,stck1,stck2):
    x = list(df1[value+'_'+stck1])
    y = list(df2[value+'_'+stck2])
    c,d = pearsonr(x,y)
    return c,d
def cor1(df1,df2,value):
    x = list(df1[value])
    y = list(df2[value])
    c,d = pearsonr(x,y)
    return c,d

def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
    
def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'buy'),'buy'] = 1
    df.loc[(df['action'] == 'short'),'short'] = 1
    df.loc[(df['action'] == 'cover'),'cover'] = 1
    df.loc[(df['action'] == 'sell'),'sell'] = 1

#    df.loc[(df['final_action'] == 'buy'),'buy'] = 1
#    df.loc[(df['final_action'] == 'short'),'short'] = 1
#    df.loc[(df['final_action'] == 'cover'),'cover'] = 1
#    df.loc[(df['final_action'] == 'sell'),'sell'] = 1


    flag=0
    df['pos']=''
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df
    
def calc_pnl(df1,stock1,stock2):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['pnl']=0
    curr_pos=0
    prev_pos=0
    for p in range(1,len(df1)): 
        if (df1.loc[p,'pos'] != df1.loc[p-1,'pos']):
            curr_pos=p
            if prev_pos=='':
                prev_pos=curr_pos
            else:    
                if df1.loc[curr_pos,'pos']==1:
                    buy1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    buy2=df1.loc[curr_pos,'close_'+str(stock2)]
                    if df1.loc[prev_pos,'pos']==-1:
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))      # + ((sell1-buy1)+(buy2-sell2))

                elif df1.loc[curr_pos,'pos']==0:   
                    if df1.loc[prev_pos,'pos']==1:
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                    elif df1.loc[prev_pos,'pos']==-1:
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                
                elif df1.loc[curr_pos,'pos']==-1:
                    short1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    short2=df1.loc[curr_pos,'close_'+str(stock2)]
                    
                    if df1.loc[prev_pos,'pos']==1:
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                        
                prev_pos=curr_pos
    return df1

#import json
import statsmodels.tsa.stattools as ts
import urllib2
import time
#data = pd.read_csv('nifty200.csv')
#data= data.iloc[0]
#stck1 = list(data)
#stck1.append('hindpetro')
#stck1.append('nhpc')
#stck2 = stck1
data = pd.read_csv('nifty200.csv')
stck1 = list(data['Symbol'])
#stck1.append('hindpetro')
#stck1.append('nhpc')
stck2 = stck1

columns = ['Stock1','Stock2','Sector','S_CloseRatio_2013','S_CloseDif_2013','S_Cor_2013','S_pVal_2013','Std/mean_2013','mean_2013','Cointegration_2013'
,'S_CloseRatio_2014','S_CloseDif_2014','S_Cor_2014','S_pVal_2014','Std/mean_2014','mean_2014','Cointegration_2014'
,'S_CloseRatio_2015','S_CloseDif_2015','S_Cor_2015','S_pVal_2015','Std/mean_2015','mean_2015','Cointegration_2015'           
,'S_CloseRatio_2016','S_CloseDif_2016','S_Cor_2016','S_pVal_2016','Std/mean_2016','mean_2016','Cointegration_2016'
,'S_CloseRatio_2017','S_CloseDif_2017','S_Cor_2017','S_pVal_2017','Std/mean_2017','mean_2017','Cointegration_2017'
,'S_CloseRatio_2018','S_CloseDif_2018','S_Cor_2018','S_pVal_2018','Std/mean_2018','mean_2018','Cointegration_2018']

#columns = ['Stock1','Stock2','Sector','S_CloseRatio','S_CloseDif','S_Cor','S_pVal','Std/mean','mean','Cointegration']
data_score = np.zeros(shape=(len(stck1)*len(stck2),len(columns)))
score = pd.DataFrame(data_score,columns = columns)

data_stcks = {}
for i in range(len(stck1)):
    try:
        indx=False
        if stck1[i] in ['NIFTY', 'BANKNIFTY']:
            indx=True
        if stck1[i] =='NIFTY':
            tradingsym= 'NIFTY 50'
        elif stck1[i] =='BANKNIFTY':
            tradingsym= 'NIFTY BANK'
        else:
            tradingsym = stck1[i]
    
        ntry=6
        df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
        while(df_stck1.empty and ntry>0):
           try:
               df_stck1  = pd.DataFrame(kite.historical(get_option_code(tradingsym, "NSE"),datetime(2013,1,1,0,0), datetime(2018,5,6,0,0), 'day'))#
#               df_stck1=nsepy.get_history(symbol=stck1[i],start=datetime.now().replace(year=datetime.now().year-1),end=datetime.now(), index=indx,futures=False)
#               df_stck1=nsepy.get_history(symbol=stck1[i],start=datetime(2015,1,1,0,0),end=datetime(2017,12,31,0,0), index=indx,futures=False)
               print stck1[i] + ' Done'
           except AttributeError:
               time.sleep(5)
        ntry-=1
#        df_stck1 = read_data('1',stck1[i])
#        json_data = json.loads(df_stck1.to_json())
#        data_stcks[i] = json_data
        data_stcks[i] = df_stck1
        data_stcks[i]['Sector']=get_sector(stck1[i].upper())
        data_stcks[i]=data_stcks[i].reset_index()
    except urllib2.HTTPError:
        print stck1[i]
        pass

k=0
for i in range(len(data_stcks)):
    try:
        df_stck1 = pd.DataFrame(data_stcks[i])

    except:
        continue
    for j in range(i,len(stck2)):
        if(i!=j and data_stcks[i]['Sector'][1]==data_stcks[j]['Sector'][1]):
            try:
                df_stck2 = pd.DataFrame(data_stcks[j])

            except:
                continue
            if(len(df_stck1)!= len(df_stck2)):
                print 'Abort',i,j
                continue
            result = df_stck1.merge(df_stck2, on='date')                

#            result['Close_ratio']= result['Close_'+stck1[i]]/result['Close_'+stck2[j]]
#            result['Close_dif'] = result['Close_'+stck1[i]]-result['Close_'+stck2[j]]
            result['Close_ratio']= result['close_x']/result['close_y']
            result['Close_dif'] = result['close_x']-result['close_y']
            sc = [stck1[i],stck2[j],data_stcks[i]['Sector'][1]]
            for year in range(2013,2019):
                if(year==2018):
                    result1 = result[(pd.to_datetime(result['date'])>=datetime(year,1,1,0,0)) & (pd.to_datetime(result['date'])<=datetime(year,5,6,0,0))]
                else:    
                    result1 = result[(pd.to_datetime(result['date'])>=datetime(year,1,1,0,0)) & (pd.to_datetime(result['date'])<=datetime(year,12,31,0,0))]

                a = slope(result1,'Close_ratio')
                b = slope(result1,'Close_dif')
                c= np.std(result1['Close_ratio'])/np.mean(result1['Close_ratio'])
                d= np.mean(result1['Close_ratio'])
#            coin_result = ts.coint(result['Close_'+stck1[i]], result['Close_'+stck2[j]])
                coin_result= ts.adfuller(result1['Close_ratio'])
#                score_cor,score_p = cor(df_stck1,df_stck2,'Close',stck1[i],stck2[j])
                df_stck1_yr=df_stck1[(pd.to_datetime(df_stck1['date'])>=datetime(year,1,1,0,0).date()) & (pd.to_datetime(df_stck1['date'])<=datetime(year,12,31,0,0).date())]
                df_stck2_yr=df_stck2[(pd.to_datetime(df_stck1['date'])>=datetime(year,1,1,0,0).date()) & (pd.to_datetime(df_stck1['date'])<=datetime(year,12,31,0,0).date())]
                score_cor,score_p = cor1(df_stck1_yr,df_stck2_yr,'close')
                sc.extend([a,b,score_cor,score_p,c,d,coin_result[1]])
            score.ix[k]=sc
#                print score_cor,stck1[i],stck2[j]    
#            score.ix[k] = [stck1[i],stck2[j],data_stcks[i]['Sector'][1],a,b,score_cor,score_p,c,d,coin_result[1]]
                
            k+=1

score = score[score['Stock1']!=0]
score=score[score.Sector!='']
score_year=score[['Stock1','Stock2','Sector','Std/mean_2013','Std/mean_2014','Std/mean_2015','Std/mean_2016','Std/mean_2017','Std/mean_2018']]

def calc_mavg(mavg,thresh=100):
    mavg['mavg']=1
    mavg['stdev']=0
    for i in range(1,len(mavg)): 
        if(i>=thresh):
            mavg.loc[i,'mavg']=mavg['close'][i-thresh:i].mean()
            mavg.loc[i,'stdev']=np.std(mavg['close'][i-thresh:i])
    return mavg

#pain_data=pd.read_csv("optionPain_Expiry_dates1.csv")
def calc_rsi(rsi_d,win=7):
#    rsi_d=data_stcks[stck1.index(stock1)][(pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])>=datetime(year,1,1,0,0).date()) & (pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])<=datetime(year,12,31,0,0).date())].reset_index()
    rsi_d['gain'] = 0.0
    rsi_d['loss'] = 0.0
    rsi_d['avg_g'] = 0.0
    rsi_d['avg_l'] =0.0
    rsi_d['rs']=0.0
    rsi_d['rsi'] =0.0
    rsi_d['rolling_avg_20']=0.0

    for i in range(1,len(rsi_d)): 
        if (rsi_d.loc[i,'close']-rsi_d.loc[i-1,'close']>0):
            rsi_d.loc[i,'gain'] = rsi_d.loc[i,'close']-rsi_d.loc[i-1,'close']
        else:
           rsi_d.loc[i,'loss'] = rsi_d.loc[i-1,'close']-rsi_d.loc[i,'close']
        if i==win:
           rsi_d.loc[i,'avg_g'] = rsi_d['gain'][0:win].mean()
           rsi_d.loc[i,'avg_l'] = rsi_d['loss'][0:win].mean()
        elif i>win:
            rsi_d.loc[i,'avg_g'] = (rsi_d.loc[i-1,'avg_g']*(win-1)+ rsi_d.loc[i,'gain'])/float(win)
            rsi_d.loc[i,'avg_l'] = (rsi_d.loc[i-1,'avg_l']*(win-1)+ rsi_d.loc[i,'loss'])/float(win)
        if(i>=20):
            rsi_d.loc[i,'rolling_avg_20']=rsi_d['close'][i-20:i].mean()
    
    rsi_d['rs'] = rsi_d['avg_g']/rsi_d['avg_l']
    rsi_d['rsi'] = 100.0 - 100.0/(1.0+rsi_d['rs'])
    return rsi_d['rsi']

def top_stock_pnl(stock1,stock2,year,a,b):
    if year==2018:
        df_stck1=data_stcks[stck1.index(stock1)][(pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])>=datetime(year-1,9,1,0,0).date()) & (pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])<=datetime(year,12,31,0,0).date())].reset_index()
        df_stck2=data_stcks[stck1.index(stock2)][(pd.to_datetime((data_stcks[stck1.index(stock2)])['date'])>=datetime(year-1,9,1,0,0).date()) & (pd.to_datetime((data_stcks[stck1.index(stock2)])['date'])<=datetime(year,12,31,0,0).date())].reset_index()
    else:
        df_stck1=data_stcks[stck1.index(stock1)][(pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])>=datetime(year,1,1,0,0).date()) & (pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])<=datetime(year,12,31,0,0).date())].reset_index()
        df_stck2=data_stcks[stck1.index(stock2)][(pd.to_datetime((data_stcks[stck1.index(stock2)])['date'])>=datetime(year,1,1,0,0).date()) & (pd.to_datetime((data_stcks[stck1.index(stock2)])['date'])<=datetime(year,12,31,0,0).date())].reset_index()

    close_ratio=pd.DataFrame(df_stck1['close']/df_stck2['close'])
    close_ratio['date']= [d.date() for d in pd.to_datetime(df_stck1['date'])]
    

    close_ratio['close_'+str(stock1)]=df_stck1['close']
    close_ratio['close_'+str(stock2)]=df_stck2['close']
    close_ratio['rsi_'+str(stock1)]=calc_rsi(df_stck1)
    close_ratio['rsi_'+str(stock2)]=calc_rsi(df_stck2)

    close_ratio=calc_mavg(close_ratio)


    close_ratio['action']=''
    close_ratio['last_action']=''
    lastaction=''
    prev_close=0

    for i in range(1,len(close_ratio)):
        mavg=close_ratio.loc[i,'mavg']
        stdev=close_ratio.loc[i,'stdev']
        close=close_ratio.loc[i,'close']
        prev_close=close_ratio.loc[i-1,'close']
        if close_ratio.loc[i-1,'action']!='do nothing':
            lastaction=close_ratio.loc[i-1,'action']       
        close_ratio.loc[i,'last_action']=lastaction

        
        if ((mavg+a*stdev)>close and (prev_close > (mavg+a*stdev)) and lastaction!='buy' and lastaction!='short'):
            close_ratio.loc[i,'action']='short'
            
        elif((mavg-a*stdev)<close and (prev_close < (mavg-a*stdev))and lastaction!='short' and lastaction!='buy'):
            close_ratio.loc[i,'action']='buy'
        
        elif(lastaction=='short'):
            if(close<mavg):
                close_ratio.loc[i,'action']='cover'
            elif close > (mavg+(b*stdev)):    
                close_ratio.loc[i,'action']='cover'
        elif(lastaction=='buy' ):
            if(close>mavg):
                close_ratio.loc[i,'action']='sell'
            elif close<(mavg-(b*stdev)):    
                close_ratio.loc[i,'action']='sell'
                
        if(close_ratio.loc[i,'action']==''):
            close_ratio.loc[i,'action']='do nothing'
            
########################### Using RSI ########################################

    close_ratio['recalculated_action']=''
    close_ratio['recalculated_last_action']=''

    lastaction1=''
    for i in range(1,len(close_ratio)):
        if close_ratio.loc[i-1,'recalculated_action']!='do nothing':
            lastaction1=close_ratio.loc[i-1,'recalculated_action']       
        close_ratio.loc[i,'recalculated_last_action']=lastaction1
        diff=(close_ratio.loc[i,'rsi_'+str(stock1)]-close_ratio.loc[i,'rsi_'+str(stock2)])
        
        if (diff>=5 and lastaction1!='short'):
            close_ratio.loc[i,'recalculated_action']='short'    
        elif(diff<=-5 and lastaction1!='buy'):
            close_ratio.loc[i,'recalculated_action']='buy'    

        elif(lastaction1=='short'):
            if(diff<0):
                close_ratio.loc[i,'recalculated_action']='cover'
            else:    
                close_ratio.loc[i,'recalculated_action']='do nothing'
        elif(lastaction1=='buy' ):
            if(diff>0):
                close_ratio.loc[i,'recalculated_action']='sell'
            else:
                close_ratio.loc[i,'recalculated_action']='do nothing'
        else:
            close_ratio.loc[i,'recalculated_action']='do nothing'
                
        if(close_ratio.loc[i,'recalculated_action']==''):
            close_ratio.loc[i,'recalculated_action']='do nothing'


###############################################################################
    close_ratio['final_action']=''    
    close_ratio['final_last_action']=''    
    lastaction2=''
    for k in range(1,len(close_ratio)-1):
        if close_ratio.loc[k-1,'final_action']!='do nothing':
            lastaction2=close_ratio.loc[k-1,'final_action']       
        close_ratio.loc[k,'final_last_action']=lastaction2
        if(close_ratio.loc[k+1,'last_action']==('short' or 'buy')):
            if ((close_ratio.loc[k+1,'recalculated_last_action']==close_ratio.loc[k+1,'last_action']) and (lastaction2!=close_ratio.loc[k+1,'recalculated_last_action'])):
                close_ratio.loc[k,'final_action']=close_ratio.loc[k+1,'recalculated_last_action']  
    
            else:
                close_ratio.loc[k,'final_action']='do nothing'
        else:
            close_ratio.loc[k,'final_action']=close_ratio.loc[k,'action']
        if(close_ratio.loc[k,'final_action']==''):
            close_ratio.loc[k,'final_action']='do nothing'
     
    
    close_ratio=close_ratio.fillna(0)
    
    close_ratio=cal_pos(close_ratio)        
    close_ratio=calc_pnl(close_ratio,stock1,stock2)
    return close_ratio


#stock1='HCLTECH'
#stock2='TCS'
#close_ratio=top_stock_pnl(stock1,stock2,2015,1,4)


#top_stocks=pd.DataFrame()
#temp=pd.DataFrame()
    
#for year in range(2013,2018):
#    pos=0
#    temp[['stock1_'+str(year),'stock2_'+str(year)]]=(score_year.sort_values('Std/mean_'+str(year))[0:10].reset_index())[['Stock1','Stock2']]
#    for k in range(10):
#        stock1=temp['stock1_'+str(year)][k]
#        stock2=temp['stock2_'+str(year)][k]
##       a=top_stock_pnl(stock1,stock2,year+1)
##       top_stocks.loc[k,'avg stock1 price_'+str(year+1)]= np.mean(a['close_'+stock1])
##       top_stocks.loc[k,'pnl_'+str(year+1)]=sum(a['pnl'])
##       top_stocks.loc[k,' Recalculated_ pnl_'+str(year+1)]=top_stocks.loc[k,'pnl_'+str(year+1)]/top_stocks.loc[k,'avg stock1 price_'+str(year+1)]
#        
#        l=1
#        m=4
##        while l<=3.1 :
##            m=l+1
##            print l
##            while m<=4.1:
#        a=top_stock_pnl(stock1,stock2,year+1,l,m)
#        top_stocks.loc[pos,'stdev_coeff1']=l
#        top_stocks.loc[pos,'stdev_coeff2']=m
#        top_stocks.loc[pos,'stock1_'+str(year)]=stock1
#        top_stocks.loc[pos,'stock2_'+str(year)]=stock2
#        pos_count=len(a[(a['pos']!=0) & (a['pos']!='')]['pos'])
#        if(pos_count==0):
#            top_stocks.loc[pos,'pnl_'+str(year+1)]=0
#        else:    
#            top_stocks.loc[pos,'pnl_'+str(year+1)] = (sum(a['pnl'])/np.mean(a['close_'+stock1]))*(len(a)/pos_count)
#        top_stocks.loc[pos,'count_pos_'+str(year+1)]= pos_count                          
#        top_stocks.loc[pos,'count_pnl'+str(year+1)]=len(a[a['pnl']!=0]['pos'])
#        pos+=1
##                m+=0.2
##            
##            l+=0.2

#b=pd.DataFrame()
#for year in range(2014,2019):
#    b.loc[0,'pnl_'+str(year)]=np.mean(top_stocks['pnl_'+str(year)])
#    b.loc[0,'pos_'+str(year)]=np.mean(top_stocks['count_pos_'+str(year)])
#
#    b.loc[0,'stdev_'+str(year)]=statistics.stdev(top_stocks['pnl_'+str(year)])
#    b.loc[0,'var_'+str(year)]=b.loc[0,'stdev_'+str(year)]*b.loc[0,'stdev_'+str(year)]

'''
a=top_stocks
a=a.round({'stdev_coeff1': 1, 'stdev_coeff2': 1})
#a=a.groupby(['stdev_coeff1','stdev_coeff2'], as_index=False)[['pnl_2015','pnl_2016','pnl_2017']].sum()
#a[['pnl_2015','pnl_2016','pnl_2017']]=a[['pnl_2015','pnl_2016','pnl_2017']]/10
c=a[((a['stdev_coeff1']==1.0) & (a['stdev_coeff2']==4.0)) | ((a['stdev_coeff1']==1.8) & (a['stdev_coeff2']==4.0))].reset_index()
del c['index']
c = c.sort_values(['stdev_coeff1', 'stdev_coeff2'], ascending=[True, True])

b=a
b=b.groupby(['stdev_coeff1','stdev_coeff2'], as_index=False)[['pnl_2015','pnl_2016','pnl_2017']].sum()
b[['pnl_2015','pnl_2016','pnl_2017']]=b[['pnl_2015','pnl_2016','pnl_2017']]/10

b=b[((b['stdev_coeff1']==1.0) & (b['stdev_coeff2']==4.0)) | ((b['stdev_coeff1']==1.8) & (b['stdev_coeff2']==4.0))].reset_index()
del b['index']
b = b.sort_values(['stdev_coeff1', 'stdev_coeff2'], ascending=[True, True])

for year in range(2015,2018):
    b.loc[0,'stdev_'+str(year)]=statistics.stdev(a[(a['stdev_coeff1']==1.0) & (a['stdev_coeff2']==4.0)]['pnl_'+str(year)])
    b.loc[1,'stdev_'+str(year)]=statistics.stdev(a[((a['stdev_coeff1']==1.8) & (a['stdev_coeff2']==4.0))]['pnl_'+str(year)])
    b.loc[0,'var_'+str(year)]=b.loc[0,'stdev_'+str(year)]*b.loc[0,'stdev_'+str(year)]
    b.loc[1,'var_'+str(year)]=b.loc[1,'stdev_'+str(year)]*b.loc[1,'stdev_'+str(year)]
    
'''
def plot(stock1,stock2):
    close_ratio=top_stock_pnl(stock1,stock2)
    close_ratio['mavg']=close_ratio['mavg'].fillna(method='bfill')
    close_ratio['stdev']=close_ratio['stdev'].fillna(method='bfill')
    plt.plot(close_ratio['close'])
    plt.plot(close_ratio['mavg']) 
    plt.plot(close_ratio['mavg']+2*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']-2*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']+3*close_ratio['stdev'])
    plt.plot(close_ratio['mavg']-3*close_ratio['stdev'])    
    plt.show()

    
stock1='IPCALAB'
stock2='LUPIN'
plot(stock1,stock2)

plt.subplot(3, 1, 1).set_title(stock1 +" &" +stock2+" "+str(year))

stock1='OFSS'
stock2='TCS'
year=2015
plt.subplot(3, 1, 2).set_title(stock1 +" &" +stock2+" "+str(year))
plot(stock1,stock2,year)

stock1='IGL'
stock2='PETRONET'
year=2017
plt.subplot(3, 1, 3).set_title(stock1 +" &" +stock2+" "+str(year))
plot(stock1,stock2,year)

    
    