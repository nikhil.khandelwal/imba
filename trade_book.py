import pandas as pd
from datetime import datetime, timedelta
from kiteconnect import KiteConnect
from utilities import get_option_code
import time

access_token = "dvnYvf0w7TKQoWpFQHwynSDWXlNOQMVt"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
root='C:/Users/DELL/Commodity_OptionData/'

def reset_trade_book():    
    try:
        df=pd.read_csv(root+"tradebook.csv")
        df=df.iloc[0:0]
    except:
        df=pd.DataFrame(columns=['Stratergy','Date','Stock_name','Quantity','Trading_price','ClosePrice','BuyPrice','Realized_profit','Unrealized_profit'])
    df.to_csv(root+"tradebook.csv", index = False)

def tradebook(curr_date):
    df=pd.read_csv(root+"tradebook.csv")
    for i in range(len(initial)):
        
        ################ historical ###########################################
#        data=pd.DataFrame(kite.historical_data(get_option_code(initial.loc[i,'Stock_name'], "NSE"), curr_date, curr_date+timedelta(days=1), 'day', continuous=False))
#        curr_price=data['close'].iloc[0]
#        initial.loc[i,'Trading_price']=data['open'].iloc[0]
        #######################################################################

        ################ live #################################################
        data=kite.quote(str(initial.loc[i,'Stock_name']))[str(initial.loc[i,'Stock_name'])]
        curr_price=data['last_price']
        #######################################################################

        d=df[(df['Stock_name']==initial.loc[i,'Stock_name'])&(df['Stratergy']==initial.loc[i,'Stratergy'])].reset_index(drop=True)
        if d.empty:
            realized_p=0
            unrealized_p=initial.loc[i,'Quantity']*(curr_price-initial.loc[i,'Trading_price'])
            df.loc[len(df)]=[str(initial.loc[i,'Stratergy']),str(curr_date.date()),str(initial.loc[i,'Stock_name']),initial.loc[i,'Quantity'],(initial.loc[i,'Trading_price']),curr_price,initial.loc[i,'Trading_price'],realized_p,unrealized_p]
        else:
            curr_quant=initial.loc[i,'Quantity']
            trade_price=initial.loc[i,'Trading_price']
            last_quant=sum(d['Quantity'])
            buy_price=d['BuyPrice'].iloc[-1]
            realized_p=d['Realized_profit'].iloc[-1]

            if last_quant<0 and curr_quant<0:
                #short=abs(curr_quant+last_quant)
                buy_price=((buy_price*abs(last_quant)) + (trade_price*abs(curr_quant)))/(abs(last_quant)+abs(curr_quant))
                unrealized_p=abs(curr_quant+last_quant)*(buy_price-curr_price)
            elif last_quant<0 and curr_quant>=0 and (last_quant+curr_quant)<0:
                #sum=-ve    
                #sell=curr_quant
                #short=abs(curr_quant+last_quant)
                realized_p+=(abs(curr_quant))*(buy_price-trade_price)
                unrealized_p=abs(curr_quant+last_quant)*(buy_price-curr_price)
        
            elif last_quant<0 and curr_quant>0 and (last_quant+curr_quant)>=0:    
                #sum=+ve
                #sell =last_quant
                #buy=curr_quant+last_quant
                realized_p+=abs(last_quant)*(buy_price-trade_price)
                buy_price=trade_price
                unrealized_p=(curr_quant+last_quant)*(curr_price-buy_price)
        
            elif last_quant>=0 and curr_quant>=0 :
                buy_price=((buy_price*last_quant) + (trade_price*curr_quant))/(last_quant+curr_quant)
                unrealized_p=(last_quant+curr_quant)*(curr_price-buy_price)
        
            elif last_quant>0 and curr_quant <0 and (last_quant+curr_quant)>=0:
                realized_p+=(abs(curr_quant))*(trade_price-buy_price)
                unrealized_p=(last_quant+curr_quant)*(curr_price-buy_price)
        
            elif last_quant>=0 and curr_quant <0 and (last_quant+curr_quant)<0:
                realized_p+=(abs(last_quant))*(trade_price-buy_price)
                buy_price=trade_price
                unrealized_p=abs(last_quant+curr_quant)*(buy_price-curr_price)
            
            df.loc[len(df)]=[str(initial.loc[i,'Stratergy']),str(curr_date.date()),str(initial.loc[i,'Stock_name']),initial.loc[i,'Quantity'],(initial.loc[i,'Trading_price']),curr_price,buy_price,realized_p,unrealized_p]
        time.sleep(2)
    df.to_csv(root+"tradebook.csv", index = False)
    return df


curr_date=datetime.now()
initial=pd.read_csv(root+"initial.csv")
df=tradebook(curr_date)

data=pd.DataFrame(columns=['Stratergy','Date','Stock_name','Quantity','Trading_price','ClosePrice','BuyPrice','Realized_profit','Unrealized_profit'])
list1 = df[['Stratergy', 'Stock_name']].apply(lambda x: ' '.join(x), axis=1).unique().tolist()
for i in range(len(list1)):
    stratergy,stock=list1[i].split()
    a=df[(df['Stock_name']==stock) &(df['Stratergy']==stratergy)]
    if sum(a['Quantity'])!=0:
        curr_price=kite.quote(stock)[stock]['last_price']
        curr_quant=a['Quantity'].iloc[-1] 
        trade_price=a['Trading_price'].iloc[-1]
        last_quant=sum(a['Quantity'])-curr_quant
        buy_price=a['BuyPrice'].iloc[-1]
        realized_p=a['Realized_profit'].iloc[-1]
        
        if last_quant<0 and curr_quant<0:
            unrealized_p=abs(curr_quant+last_quant)*(buy_price-curr_price)
        elif last_quant<0 and curr_quant>=0 and (last_quant+curr_quant)<0:
            unrealized_p=abs(curr_quant+last_quant)*(buy_price-curr_price)
        elif last_quant<0 and curr_quant>0 and (last_quant+curr_quant)>=0:    
            unrealized_p=(curr_quant+last_quant)*(curr_price-buy_price)
        elif last_quant>=0 and curr_quant>=0 :
            unrealized_p=(last_quant+curr_quant)*(curr_price-buy_price)
        elif last_quant>0 and curr_quant <0 and (last_quant+curr_quant)>=0:
            unrealized_p=(last_quant+curr_quant)*(curr_price-buy_price)
        elif last_quant>=0 and curr_quant <0 and (last_quant+curr_quant)<0:
            unrealized_p=abs(last_quant+curr_quant)*(buy_price-curr_price)
        
        data.loc[len(data)]=[stratergy,str(curr_date.date()),stock ,curr_quant,trade_price,curr_price,buy_price,realized_p,unrealized_p]
    time.sleep(1)
    