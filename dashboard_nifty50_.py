from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime, timedelta
import time
from utilities import get_option_code, get_vol_nsepy,get_strike_diff,get_pain_anyDate,calc_expiry
import math
import statistics
import nsepy
from calendar import monthrange
import scipy
######################################################################
access_token = "dvnYvf0w7TKQoWpFQHwynSDWXlNOQMVt"
from_date = datetime(2017, 4, 28 ,9, 15, 0)
to_date = datetime(2017, 4, 30, 9, 15, 0)
######################################################################
stocks=pd.read_csv("ind_nifty100list.csv",usecols=[2])
stocks_list = list(stocks['Symbol'])
stocks_list.extend(['NIFTY', 'BANKNIFTY'])


def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
def get_atmStrike(price, strike_dif):
    try:
        temp = int(int(price / strike_dif)*strike_dif)
    except:
        return 0
    if underlying =='NIFTY' or underlying =='BANKNIFTY':
        return temp
    data = pd.read_excel('strike_price.xlsx')
    baseP = data[data['Ticker ']==underlying]['Strike Price'].iloc[0]
    if int((temp - baseP)/strike_dif) == (temp - baseP)/strike_dif:
        return temp
    else:
        print (underlying)
        return int(baseP + int((temp - baseP)/strike_dif)*baseP)
def getprevdayData(underlying, result_yesterday):
    try:
        data  = result_yesterday[result_yesterday['Sciptname']==underlying].iloc[0]
        return data['pain'], data['change oi'], data['pain_price_ratio'], data['vol_ce'], data['vol_pe'], data['close'],data['P-C ratio']
    except:
        return 0,0,0,0,0,0,0
def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r
def calc_last_thursday(curr_date):
    a,max_days = monthrange(curr_date.year, curr_date.month) 
    while(max_days>20):
        curr_date=curr_date.replace(day=max_days)
        if(curr_date.weekday()==3):
            return curr_date
        else:
            max_days-=1    

api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
exch="NFO"
exch2 ="NSE"
if(from_date>to_date):
    raise ValueError('from_date cannot be greater than to_date')


ntry=4
indexData1=pd.DataFrame(data=None,index=None,columns=None)
while(indexData1.empty and ntry>0):
    try:
        indexData1=nsepy.get_history(symbol='NIFTY 50',start=from_date.replace(year=from_date.year-5),end=to_date, index=True,futures=False)
#        indexData1  = pd.DataFrame(kite.historical(get_option_code('NIFTY 50', exch2),fdate, tdate, 'day'))
    except Exception:
        time.sleep(100)
        pass
    ntry-=1
indexData1.columns=map(str.lower, indexData1.columns)
if (from_date.weekday()>4):
    curr_date=from_date+timedelta(days=(7-from_date.weekday()))
else:
    curr_date=from_date

    
while(curr_date<=to_date):
    while(indexData1[indexData1.index==curr_date.date()].empty and curr_date<=to_date):
        curr_date+=timedelta(days=1)

    opt_maturity=calc_expiry(curr_date).replace(hour=15,minute=30) 
    prev_expiry=calc_expiry(curr_date,'prev').replace(hour=9,minute=15)     
    
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    maturity = str(cur_year)+cur_month
    prev_exp_str = prev_expiry.strftime('%Y-%m-%dT%H:%M:%S+0530')
    to_date_now = datetime.strftime(curr_date, "%Y-%m-%d")
    
    yesterday_date = curr_date - timedelta(days = 1)    
    while(indexData1[indexData1.index==yesterday_date.date()].empty and yesterday_date>=from_date-timedelta(days=10)):
        yesterday_date-=timedelta(days=1)

    prev_ydate = yesterday_date - timedelta(days = 1)    
    while(indexData1[indexData1.index==prev_ydate.date()].empty and prev_ydate>=from_date-timedelta(days=10)):
        prev_ydate-=timedelta(days=1)

    last_opt_maturity=calc_expiry(yesterday_date).replace(hour=15,minute=30) 
    last_lastopt_maturity=calc_expiry(prev_ydate).replace(hour=15,minute=30)
    yesterday_date = datetime.strftime(yesterday_date, "%Y-%m-%d")
    prev_ydate=datetime.strftime(prev_ydate, "%Y-%m-%d")

    from_date_now_min = datetime.strftime(curr_date-timedelta(days=29), "%Y-%m-%d")
    from_date_now = datetime.strftime(curr_date.replace(year = datetime.now().year-5), "%Y-%m-%d")

    curr_time = datetime.strftime(datetime.now(), "%H:%M:%S")
#    root='C:/Users/Administrator/Desktop/zerodha/dashboard/files/'
    root='C:/Users/DELL/Commodity_OptionData/'
    filePath = root+ to_date_now+'(historical).csv'
#    filePath_yesterday = root+yesterday_date+'(historical).csv'

    columns = ['Sciptname','Date','Sector','Standard deviation(last 5 Days)',
               'Gap Return(last day 3:20 to today 9:20)','lastHour/TotalDay Return',
               'Return(1-3:30)-Return(9:20-1)','R-square(5 min candle)',
               'today(9:20-10)','today(9:20-11)','today(9:20-1)',
               'today(9:20-2)','today(9:20-3)','open', 'high', 'low','close',
               'IntraDay_return','today_return', '5day_return', 'niftyReturn', 
               'beta', 'premium','5d avg_premium','%delivery','deliveryAvg_5', 
               'deliveryAvg_10', 'deliveryAvg_30','volume_inc_perc_5','volume_inc_perc_10'
               ,'volume_inc_perc_30', 'OI','change oi','atm_strike','vol_ce', 'vol_pe',
               'avg_vol','change ce_vol','change pe_vol','Last Day CallIV-PutIV', '5yr high',
               '5yr_low', '50d high','50d low','100d high', '100d low', 'prev_expiry Close',
               'fo_view',
               'LastDay Gap Return(Last_tolast day 3:20 to lastday 9:20)','LastDay(Last_tolastHour/lastDay Return)',
               'LastDayReturn(1-3:30)-LastDayReturn(9:20-1)','LastDay R-square(5 min candle)','LastDay(9:20-10)',
               'LastDay(9:20-11)','LastDay(9:20-1)','LastDay(9:20-2)','LastDay(9:20-3)',
               'LastDay open', 'LastDay high', 'LastDay low','LastDay close','LastDay IntraDay_return',
               'LastDay today_return', 'LastDay 5day_return', 'LastDay niftyReturn','LastDay Premium','Last_to_LastDay Premium',
               'LastDay %delivery','LastDay deliveryAvg_5', 'LastDay deliveryAvg_10', 'LastDay deliveryAvg_30',
               'LastDay volume_inc_perc_5','LastDay volume_inc_perc_10','LastDay volume_inc_perc_30',
               'LastDay OI','LastDay change oi','Last_to_LastDay OI','Last_to_LastDay ChangeOI',
               'LastDay atm_strike','LastDay vol_ce', 'LastDay vol_pe', 'LastDay avg_vol','LastDay change ce_vol',
               'LastDay change pe_vol','LastDay fo_view']
    

   
    result_today = pd.DataFrame(columns = columns)
        
    errorL = []
    error_run = 0
    if error_run == 1 and len(errorL)>0:
        run_l = errorL[:]
        result_today = pd.read_csv(filePath)
    else:
        run_l = stocks_list[:]
        result_today = pd.DataFrame(columns = columns)
        
    indexData  = indexData1[indexData1.index>=datetime.strptime(from_date_now, "%Y-%m-%d").date()]
    indexData  = indexData[indexData.index<=datetime.strptime(to_date_now, "%Y-%m-%d").date()]
    indexData_min=pd.DataFrame(kite.historical_data(get_option_code('NIFTY 50', exch2), from_date_now_min, to_date_now, 'minute', continuous=False))
    for i in range(len(run_l)):
        
        if len(errorL)==0:
            underlying  = stocks_list[i]
        else:
            underlying = errorL[0]
        if underlying =='NIFTY':
            tradingsym= 'NIFTY 50'
        elif underlying =='BANKNIFTY':
            tradingsym= 'NIFTY BANK'
        else:
            tradingsym = underlying
        fo_ticker = underlying+maturity+'FUT'
        fo_ticker_curr=underlying+maturity[0:2]+datetime.now().strftime("%b").upper()+'FUT'
        strike_dif = get_strike_diff(underlying)
        

        print underlying
        ############ PULL DATA ####################
        try:
            ##########START:::CALCULATING MASTER DATA #####################
            d1=datetime.strptime(to_date_now, "%Y-%m-%d")
            f1=d1.replace(year=d1.year-1)
            master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            f_date=f1
            t_date=calc_expiry(f_date)
            if(underlying=='NIFTY'or underlying=='BANKNIFTY'):
                indx=True
            else:
                indx=False

            while(t_date<d1):
                t_date=calc_expiry(f_date)
                if(t_date>d1):
                    t_date=d1
                if(f_date>t_date):
                    f_date=t_date
                    
                if (master_fo.empty):
                    ntry=4
                    while(master_fo.empty and ntry>0):
                        try:
                            master_fo=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=indx,futures=True,expiry_date=calc_expiry(f_date.date()))
                        except AttributeError:
                            print "Please wait. Trying to fetch data from nsepy..."
                            time.sleep(300)
                            pass
                        ntry-=1
                else:
                    mData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
                    ntry=4
                    while(mData.empty and ntry>0):
                        try:
                            mData=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=indx,futures=True,expiry_date=calc_expiry(f_date.date()))
                            master_fo=pd.concat([master_fo,mData])
                        except AttributeError as e:
                            pass
                        ntry-=1
                f_date=t_date+timedelta(days=1)    
                              
            if(master_fo.empty):
                continue
            if underlying in ['NIFTY', 'BANKNIFTY']:
                master = master_fo[:][:]
                master.columns=map(str.lower, master.columns)
            else:
                ntry=4
                master=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
                while(master.empty and ntry>0):
                    try:
                        master=nsepy.get_history(symbol=underlying,start=datetime.strptime(from_date_now, "%Y-%m-%d"),end=datetime.strptime(to_date_now, "%Y-%m-%d"), index=False,futures=False)
                        master.columns=map(str.lower, master.columns)
                    except Exception:
                        print "Please wait. Trying to fetch data from nsepy..."
                        time.sleep(100)
                        pass
                    ntry-=1
###############################################################################
            openP = master.ix[len(master)-1]['open']
            highP = master.ix[len(master)-1]['high']
            lowP = master.ix[len(master)-1]['low']
            closeP = master.ix[len(master)-1]['close']
            lastDayClose = master.ix[len(master)-2]['close']
            IntraDay_return = (closeP/openP - 1)*100
            today_return = (closeP/lastDayClose - 1)*100
            if(today_return<1):
                print("today_return is less than 1. Moving to next...")
                try:
                    errorL.remove(underlying)
                except:
                    pass
                continue
###############################################################################

            ntry=10
            master_min=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(master_min.empty and ntry>0):
                try:
                    master_min=pd.DataFrame(kite.historical_data(get_option_code(tradingsym, exch2), from_date_now_min, to_date_now, 'minute', continuous=False))
#                    master_min = pd.DataFrame(kite.historical(get_option_code(tradingsym, exch2),from_date_now_min, to_date_now, 'minute'))
                except Exception:
                    time.sleep(4)
                    pass
                ntry-=1
            ########END:::CALCULATING MASTER DATA #########################          
            ###START:::NSEPY DATA #############################################
            ntry=5
            nsepyData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(nsepyData.empty and ntry>0):
                try:
                    if(underlying=='NIFTY'or underlying=='BANKNIFTY'):
                        nsepyData=nsepy.get_history(symbol=underlying,start=from_date-timedelta(days=60),end=to_date, index=True,futures=False)
                    else:    
                        nsepyData=nsepy.get_history(symbol=underlying,start=from_date-timedelta(days=60),end=to_date, index=False,futures=False)
                except Exception:
                    print "Please wait. Trying to fetch data from nsepy..."
                    time.sleep(100)
                    pass
                ntry-=1
            ##END::NSEPY DATA #################################################
            master_min['close'][0]
            print underlying , 'Data Successfully pulled. Doing Calculations...'   
            
        except Exception as e:
            print underlying , 'Data pull failed. Moving to next... Error was:', e.__class__.__name__
            if not underlying in errorL:
                errorL.append(underlying)
            time.sleep(5)
            continue
        ############## CALCULATIONS################
        
        sector = get_sector(underlying)
        cdate=to_date_now

        tdr9to10=((master_min[master_min['date']==cdate+"T10:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        tdr9to11=((master_min[master_min['date']==cdate+"T11:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        tdr9to1=((master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        tdr9to2=((master_min[master_min['date']==cdate+"T14:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        tdr9to3=((master_min[master_min['date']==cdate+"T15:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        
        ldr9to10=((master_min[master_min['date']==yesterday_date+"T10:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        ldr9to11=((master_min[master_min['date']==yesterday_date+"T11:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        ldr9to1=((master_min[master_min['date']==yesterday_date+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        ldr9to2=((master_min[master_min['date']==yesterday_date+"T14:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        ldr9to3=((master_min[master_min['date']==yesterday_date+"T15:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100
        
        nextDayGapReturn3to9=((master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T15:20:00+0530"]['close'].iloc[0]) -1)*100
        lastdayGapReturn3to9=((master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==prev_ydate+"T15:20:00+0530"]['close'].iloc[0]) -1)*100
      
        returnbtw=(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0]) -1)*100)-(((master_min[master_min['date']==cdate+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:20:00+0530"]['close'].iloc[0]) -1)*100)    
        lastday_returnbtw=(((master_min[master_min['date']==yesterday_date+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T13:00:00+0530"]['close'].iloc[0]) -1)*100)-(((master_min[master_min['date']==yesterday_date+"T13:00:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:20:00+0530"]['close'].iloc[0]) -1)*100)    
    
        lastHr_totalDay=(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T14:30:00+0530"]['close'].iloc[0]) -1)*100)/(((master_min[master_min['date']==cdate+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==cdate+"T09:15:00+0530"]['close'].iloc[0]) -1)*100)
        lastday_lastHr_totalDay=(((master_min[master_min['date']==yesterday_date+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T14:30:00+0530"]['close'].iloc[0]) -1)*100)/(((master_min[master_min['date']==yesterday_date+"T15:29:00+0530"]['close'].iloc[0])/(master_min[master_min['date']==yesterday_date+"T09:15:00+0530"]['close'].iloc[0]) -1)*100)
 
        ##############R-square(5 min candle)###################################            
        q=datetime.now().replace(hour=9,minute=15,second=0)
        list1=[]
        while (q.strftime("%H:%M:%S")<"15:30:00"):
            list1.append(master_min[master_min['date']==cdate+"T"+q.strftime("%H:%M:%S")+"+0530"]['close'].iloc[0])
            q+=timedelta(minutes=5)  
        slope, intercept, r_value, p_value, std_err=scipy.stats.linregress(range(len(list1)),list1)
        rsquare = (r_value) * (r_value)
       
        #############################LAST DAY R SQUARE#########################
        q=datetime.now().replace(hour=9,minute=15,second=0)
        list1=[]
        while (q.strftime("%H:%M:%S")<"15:30:00"):
            list1.append(master_min[master_min['date']==yesterday_date+"T"+q.strftime("%H:%M:%S")+"+0530"]['close'].iloc[0])
            q+=timedelta(minutes=5)  
        slope, intercept, r_value, p_value, std_err=scipy.stats.linregress(range(len(list1)),list1)
        lastday_rsquare = (r_value) * (r_value)
        
        ##########Standard deviation(last 5 Days)##############################  
        lastday_return=[]
        for r in range(2,7):
            lastday_return.append((master.ix[len(master)-r]['close']/master.ix[len(master)-(r+1)]['close'] - 1)*100)
        std_dev=statistics.stdev(lastday_return)
    #################################Day Data Calculation########################
        
        openP = master.ix[len(master)-1]['open']
        highP = master.ix[len(master)-1]['high']
        lowP = master.ix[len(master)-1]['low']
        closeP = master.ix[len(master)-1]['close']
        
        lastday_openP = master.ix[len(master)-2]['open']
        lastday_highP = master.ix[len(master)-2]['high']
        lastday_lowP = master.ix[len(master)-2]['low']
        lastday_closeP = master.ix[len(master)-2]['close']
        lastday_lastDayClose= master.ix[len(master)-3]['close']
        
        IntraDay_return = (closeP/openP - 1)*100
        today_return = (closeP/lastDayClose - 1)*100
        nday_return_5 = (closeP/master.ix[len(master)-6]['close'] - 1)*100
        niftyReturn =  (indexData.ix[len(indexData)-1]['close']/indexData.ix[len(indexData)-2]['close'] - 1)*100
        indexReturn =  today_return - niftyReturn
        
        lastday_IntraDay_return = (lastday_closeP/lastday_openP - 1)*100
        lastday_today_return = (lastday_closeP/lastday_lastDayClose - 1)*100
        lastday_nday_return_5 = (lastday_closeP/master.ix[len(master)-7]['close'] - 1)*100
        lastday_niftyReturn =  (indexData.ix[len(indexData)-2]['close']/indexData.ix[len(indexData)-3]['close'] - 1)*100
        lastday_indexReturn =  lastday_today_return - lastday_niftyReturn
        
        avg_change = master.ix[len(master)-200:len(master)]['close'].std()/master.ix[len(master)-200:len(master)]['close'].mean()
        index_avg_change = indexData.ix[len(indexData)-200:len(indexData)]['close'].std()/indexData.ix[len(indexData)-200:len(indexData)]['close'].mean()
        beta = avg_change/index_avg_change
        sector = get_sector(underlying)

        premium  = (master_fo.ix[len(master_fo)-1]['Close']/master.ix[len(master)-1]['close'] -1)*100
        lastday_premium=(master_fo.ix[len(master_fo)-2]['Close']/master.ix[len(master)-2]['close'] -1)*100
        last_lastday_premium=(master_fo.ix[len(master_fo)-3]['Close']/master.ix[len(master)-3]['close'] -1)*100

        avg_premium=[]
        for q in range(2,7):
            avg_premium.append((master_fo.ix[len(master_fo)-q]['Close']/master.ix[len(master)-q]['close'] -1)*100)
        avg_premium=statistics.mean(avg_premium)
                
        if (len(nsepyData)>0 and underlying not in ['NIFTY', 'BANKNIFTY']):
            deliveryperc=nsepyData[nsepyData.index==curr_date.date()]['%Deliverble'].iloc[0]*100
            deliveryAvg_5 = nsepyData.ix[len(nsepyData)-5:len(nsepyData)]['%Deliverble'].mean()
            deliveryAvg_10 = nsepyData.ix[len(nsepyData)-10:len(nsepyData)]['%Deliverble'].mean()
            deliveryAvg_30 = nsepyData.ix[len(nsepyData)-30:len(nsepyData)]['%Deliverble'].mean()
#            volume_inc_percentage = (float(nsepyData[nsepyData.index==curr_date.date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-6:len(nsepyData)]['Volume'].mean() -1)*100
            volume_inc_percentage_5=(float(nsepyData[nsepyData.index==curr_date.date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-5:len(nsepyData)]['Volume'].mean() -1)*100
            volume_inc_percentage_10=(float(nsepyData[nsepyData.index==curr_date.date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-10:len(nsepyData)]['Volume'].mean() -1)*100
            volume_inc_percentage_30=(float(nsepyData[nsepyData.index==curr_date.date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-30:len(nsepyData)]['Volume'].mean() -1)*100


            lastday_deliveryperc=nsepyData[nsepyData.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['%Deliverble'].iloc[0]*100
            lastday_deliveryAvg_5 = nsepyData.ix[len(nsepyData)-6:len(nsepyData)-1]['%Deliverble'].mean()
            lastday_deliveryAvg_10 = nsepyData.ix[len(nsepyData)-11:len(nsepyData)-1]['%Deliverble'].mean()
            lastday_deliveryAvg_30 = nsepyData.ix[len(nsepyData)-31:len(nsepyData)-1]['%Deliverble'].mean()
#            lastday_volume_inc_percentage = (float(nsepyData[nsepyData.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Volume'])/nsepyData.ix[len(nsepyData)-7:len(nsepyData)-1]['Volume'].mean() -1)*100
            lastday_volume_inc_percentage_5=(float(nsepyData[nsepyData.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-6:len(nsepyData)-1]['Volume'].mean() -1)*100
            lastday_volume_inc_percentage_10=(float(nsepyData[nsepyData.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-11:len(nsepyData)-1]['Volume'].mean() -1)*100
            lastday_volume_inc_percentage_30=(float(nsepyData[nsepyData.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Volume'].iloc[0])/nsepyData.ix[len(nsepyData)-31:len(nsepyData)-1]['Volume'].mean() -1)*100

        else:
            deliveryperc=0
            deliveryAvg_5 = 0
            deliveryAvg_10 = 0
            deliveryAvg_30 = 0
            volume_inc_percentage = 0
            
            lastday_deliveryperc=0
            lastday_deliveryAvg_5=0
            lastday_deliveryAvg_10=0
            lastday_deliveryAvg_30=0
            lastday_volume_inc_percentage = 0


        OI = master_fo[master_fo.index==curr_date.date()]['Open Interest'].iloc[0]
        lastday_OI=master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Open Interest'].iloc[0]
        last_lastdayOI=master_fo[master_fo.index==datetime.strptime(prev_ydate, "%Y-%m-%d").date()]['Open Interest'].iloc[0]

        change_oi = calc_change(OI,lastday_OI)
        lastday_changeOI = calc_change(lastday_OI,last_lastdayOI)
        
        last_3rd_dayOI=master_fo.ix[len(master_fo)-4]['Open Interest']
        last_lastdayChangeOI=calc_change(last_lastdayOI,last_3rd_dayOI)
        
        high_5yr =max(master['high'])
        low_5yr = min(master['low'])
        high_50d =max(master.ix[len(master)-50:len(master)]['high'])
        high_100d =max(master.ix[len(master)-100:len(master)]['high'])
        low_50d =min(master.ix[len(master)-50:len(master)]['low'])
        low_100d =min(master.ix[len(master)-100:len(master)]['low'])
        prevexpiry_close = float(master[master.index==datetime.strptime(prev_exp_str[0:10], "%Y-%m-%d").date()]['close'].iloc[0])

        if math.isnan(strike_dif) or int((strike_dif*10)/10) != strike_dif:
            print 'No option volume'
            atm_strike = 0
            vol_ce = 0
            vol_pe = 0
            avg_vol = 0

            lastday_atm_strike=0
            last_lastday_atm_strike=0
            lastday_vol_ce=0
            lastday_vol_pe=0
            lastday_avg_vol=0
            datetime.strptime(yesterday_date, "%Y-%m-%d")
        else:
            atm_strike = get_atmStrike(master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0],strike_dif)
            lastday_atm_strike = get_atmStrike(master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Close'].iloc[0],strike_dif)
            last_lastday_atm_strike = get_atmStrike(master_fo[master_fo.index==datetime.strptime(prev_ydate, "%Y-%m-%d").date()]['Close'].iloc[0],strike_dif)
            print underlying , 'Calculating Option Volitility'
            vol_ce = get_vol_nsepy(curr_date, underlying, str(atm_strike), 'CE', master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0], opt_maturity)
            if(vol_ce<=0.001):
                vol_ce=float('nan')
            vol_pe = get_vol_nsepy(curr_date, underlying, str(atm_strike), 'PE', master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0], opt_maturity)
            if(vol_pe<=0.001):
                vol_pe=float('nan') 
            avg_vol = 0.5*(vol_pe+ vol_ce)

            
            lastday_vol_ce = get_vol_nsepy(datetime.strptime(yesterday_date, "%Y-%m-%d"), underlying, str(lastday_atm_strike), 'CE', master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Close'].iloc[0], last_opt_maturity)
            last_lastdayvolce=get_vol_nsepy(datetime.strptime(prev_ydate, "%Y-%m-%d"), underlying, str(last_lastday_atm_strike), 'CE', master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Close'].iloc[0], last_lastopt_maturity)

            if(lastday_vol_ce<=0.001):
                lastday_vol_ce=float('nan')
            
            lastday_vol_pe = get_vol_nsepy(datetime.strptime(yesterday_date, "%Y-%m-%d"), underlying, str(lastday_atm_strike), 'PE', master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Close'].iloc[0], last_opt_maturity)
            last_lastdayvolpe = get_vol_nsepy(datetime.strptime(prev_ydate, "%Y-%m-%d"), underlying, str(last_lastday_atm_strike), 'PE', master_fo[master_fo.index==datetime.strptime(yesterday_date, "%Y-%m-%d").date()]['Close'].iloc[0], last_lastopt_maturity)
            if(lastday_vol_pe<=0.001):
                lastday_vol_pe=float('nan') 
                
            avg_vol = 0.5*(vol_pe+ vol_ce)
            lastday_avg_vol= 0.5*(lastday_vol_pe+ lastday_vol_ce)
            
            print underlying , 'Calculating Option Pain'

        change_ce_vol = calc_change(vol_ce, lastday_vol_ce)
        change_pe_vol = calc_change(vol_pe, lastday_vol_pe)
        lastday_change_ce_vol= calc_change(lastday_vol_ce,last_lastdayvolce)
        lastday_change_pe_vol = calc_change(lastday_vol_pe,last_lastdayvolpe)
        
        LastDay_CallPut_diff=lastday_vol_ce-lastday_vol_pe
        
        if OI>lastday_OI:
            if closeP>lastday_closeP:
                fo_view= 'Long Buildup'
            elif closeP<lastday_closeP:
                fo_view = 'Short Buildup'
            else:
                fo_view = 'Unknown'    
        elif OI<lastday_OI:
            if closeP>lastday_closeP:
                fo_view= 'Short Covering'
            elif closeP<lastday_closeP:
                fo_view = 'Long unwinding'
            else:
                fo_view = 'Unknown'
        else:
            fo_view = 'Unknown'      

            lastday_OI
            
          
        if lastday_OI>last_lastdayOI:
            if lastday_closeP>lastday_lastDayClose:
                lastday_fo_view= 'Long Buildup'
            elif lastday_closeP<lastday_lastDayClose:
                lastday_fo_view = 'Short Buildup'
            else:
                lastday_fo_view = 'Unknown'    
        elif lastday_OI<last_lastdayOI:
            if lastday_closeP>lastday_lastDayClose:
                lastday_fo_view= 'Short Covering'
            elif lastday_closeP<lastday_lastDayClose:
                lastday_fo_view = 'Long unwinding'
            else:
                lastday_fo_view = 'Unknown'
        else:
            lastday_fo_view = 'Unknown'      
           
    #############################################################################    
        result_today.loc[len(result_today)] = [underlying, cdate,sector,std_dev,
             nextDayGapReturn3to9,lastHr_totalDay,returnbtw,rsquare,tdr9to10,
             tdr9to11,tdr9to1,tdr9to2,tdr9to3,openP, highP, lowP,closeP,
             IntraDay_return, today_return, nday_return_5, 
             indexReturn, beta, premium, avg_premium, deliveryperc
             ,deliveryAvg_5, deliveryAvg_10, deliveryAvg_30,volume_inc_percentage_5,
             volume_inc_percentage_10,volume_inc_percentage_30, OI, 
             change_oi, atm_strike, vol_ce, vol_pe, avg_vol,change_ce_vol,change_pe_vol,
             LastDay_CallPut_diff,high_5yr, low_5yr, high_50d, low_50d, high_100d, 
             low_100d, prevexpiry_close, fo_view,lastdayGapReturn3to9,
             lastday_lastHr_totalDay, lastday_returnbtw,lastday_rsquare,ldr9to10,ldr9to11,
             ldr9to1,ldr9to2,ldr9to3,lastday_openP, lastday_highP, lastday_lowP,
             lastday_closeP,lastday_IntraDay_return, lastday_today_return, 
             lastday_nday_return_5,lastday_indexReturn, lastday_premium,last_lastday_premium,
             lastday_deliveryperc,lastday_deliveryAvg_5, lastday_deliveryAvg_10, 
             lastday_deliveryAvg_30,lastday_volume_inc_percentage_5,lastday_volume_inc_percentage_10,
             lastday_volume_inc_percentage_30,lastday_OI,lastday_changeOI,last_lastdayOI,
             last_lastdayChangeOI, lastday_atm_strike, lastday_vol_ce, lastday_vol_pe,
             lastday_avg_vol, lastday_change_ce_vol, lastday_change_pe_vol, lastday_fo_view]
   

        try:
            errorL.remove(underlying)
        except:
            pass
    
    result_today.to_csv(filePath, index = False)

    if (curr_date.weekday()>=4):
        curr_date=curr_date+timedelta(days=(7-curr_date.weekday()))
    else:
        curr_date=curr_date+timedelta(days=1)