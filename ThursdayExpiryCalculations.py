'''
Thursday expiry calculations using the strike of last monday/tuesday/wednesday
'''

import pandas as pd
import nsepy
import time
from datetime import datetime, timedelta
from utilities import calc_expiry
import numpy as np
import matplotlib.pyplot as plt


#############################################################################################################
def get_atmStrike(price, strike_dif=100.0):
    temp = int(int(price / strike_dif)*strike_dif)
    return temp

##############################################################################################################
f_date=datetime(2018,6,1,0,0)
t_date=datetime(2018,6,28,0,0)


data=nsepy.get_history(symbol='BANKNIFTY',start=f_date,end=t_date, index=True,futures=False).reset_index()

master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)

while(f_date<=t_date):
    ### last tuesday before expiry
    if(f_date.weekday()==1):
        try:
            close=data[data['Date']==f_date.date()]['Close'].iloc[0]
            from_date=f_date
        except:
            close=data[data['Date']==(f_date-timedelta(days=1)).date()]['Close'].iloc[0]
            from_date=f_date-timedelta(days=1)
        exp=f_date+timedelta(days=2)
#        exp=f_date+timedelta(days=3)
        if(f_date==datetime(2017,10,17,0,0)):
            exp=f_date+timedelta(days=1)
        try:
            pr=data[data['Date']==exp.date()]['Close'].iloc[0]
        except:
            exp=f_date+timedelta(days=1)
#            exp=f_date+timedelta(days=2)
        atm_price=int(int(close / 100.0)*100.0)

        if (master_fo.empty):
            ntry=4
            while(master_fo.empty and ntry>0):
                try:
                    master_fo=nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='CE',
                                      strike_price=atm_price, expiry_date=exp)
                    master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='PE',
                                      strike_price=atm_price, expiry_date=exp))

                    master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='CE',
                                      strike_price=atm_price+200, expiry_date=calc_expiry(from_date)))
                    master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='PE',
                                      strike_price=atm_price-200, expiry_date=calc_expiry(from_date)))

                    master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=exp.date(),end=exp.date(), index=True,option_type='CE',
                                      strike_price=atm_price+200, expiry_date=calc_expiry(from_date)))
                    master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=exp.date(),end=exp.date(), index=True,option_type='PE',
                                      strike_price=atm_price-200, expiry_date=calc_expiry(from_date)))
                    
                except AttributeError:
                    print "Please wait. Trying to fetch data from nsepy..."
                    time.sleep(10)
                    pass
                ntry-=1
        else:
            try:
                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='CE',
                                  strike_price=atm_price, expiry_date=exp))
                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='PE',
                                  strike_price=atm_price, expiry_date=exp))


                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='CE',
                                  strike_price=atm_price+200, expiry_date=calc_expiry(from_date)))
                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=from_date.date(),end=from_date.date(), index=True,option_type='PE',
                                  strike_price=atm_price-200, expiry_date=calc_expiry(from_date)))


                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=exp.date(),end=exp.date(), index=True,option_type='CE',
                                  strike_price=atm_price+200, expiry_date=calc_expiry(from_date)))
                master_fo=master_fo.append(nsepy.get_history(symbol="BANKNIFTY",start=exp.date(),end=exp.date(), index=True,option_type='PE',
                                  strike_price=atm_price-200, expiry_date=calc_expiry(from_date)))

            except AttributeError as e:
                pass
    f_date=f_date+timedelta(days=1)


master_fo['ClosePrice']=''
for i in range(len(master_fo)):
    master_fo['ClosePrice'][i]=data[data.Date==master_fo.index[i]]['Close']
    
cols = list(master_fo.columns.values)    

cols = cols[:10]+cols[-1:] + cols[10:-1]
master_fo=master_fo[cols]
master_fo=master_fo.reset_index()
m=pd.DataFrame(columns=['Date','Spread'])
for i in range(0,len(master_fo),6):
    m.loc[len(m)]=[master_fo['Date'][i+4],master_fo['Close'][i]+master_fo['Close'][i+1]-abs(master_fo['ClosePrice'][i+4]-master_fo['Strike Price'][i])+master_fo['Close'][i+4]+master_fo['Close'][i+5]-master_fo['Close'][i+2]-master_fo['Close'][i+3]]
    