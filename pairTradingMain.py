import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from utilities import plot_data, plot_daily, VARanalysis, hurst, LR
from statsmodels.tsa.stattools import adfuller
from sklearn import linear_model
from statsmodels.tsa.api import VAR, DynamicVAR
from statsmodels.tsa.stattools import coint
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import time
import nsepy
import matplotlib.mlab as mlab
from scipy.stats import norm
from tabulate import tabulate

def get_sector(underlying):
    data = pd.read_excel('C:/Users/Nikhil/files/sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
def pull_data(stck1,curr_date,to_date):
    data_stcks = {}
    curr_date=curr_date.replace(hour=9,minute=15,second=0,microsecond=0)
    to_date=to_date.replace(hour=9,minute=15,second=0,microsecond=0)
    print("Calculating data....")
    for i in range(len(stck1)):
        try:
            indx=False
            if stck1[i] in ['NIFTY', 'BANKNIFTY']:
                indx=True
            if stck1[i] =='NIFTY':
                tradingsym= 'NIFTY 50'
            elif stck1[i] =='BANKNIFTY':
                tradingsym= 'NIFTY BANK'
            else:
                tradingsym = stck1[i]
            print tradingsym    
            ntry=6
            df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            while(df_stck1.empty and ntry>0):
               try:
                   df_stck1= nsepy.get_history(symbol=stck1[i],start=curr_date, end=to_date,index=indx)
               except AttributeError:
                   time.sleep(5)
            if max(df_stck1['High'][0:-1].reset_index(drop=True)/df_stck1['Low'][1:].reset_index(drop=True))>2:
                print stck1[i], 'Splited'
                
            ntry-=1
            data_stcks[stck1[i]] = df_stck1.copy().reset_index()
            data_stcks[stck1[i]]['Sector']=get_sector(stck1[i].upper())
        except Exception:
            print stck1[i]
            pass
    print("Data calculation finish.")
    return data_stcks

#############################################################################
root=''
#############################################################################

def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'buy'),'buy'] = 1
    df.loc[(df['action'] == 'short'),'short'] = 1
    df.loc[(df['action'] == 'cover'),'cover'] = 1
    df.loc[(df['action'] == 'sell'),'sell'] = 1

    flag=0
    df['pos']=''
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df

###############################################################################
def calc_pnl(df1):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['pnl']=0
    curr_pos=0
    prev_pos=0
    for p in range(1,len(df1)): 
        if (df1.loc[p,'pos'] != df1.loc[p-1,'pos']):
            curr_pos=p
            if prev_pos=='':
                prev_pos=curr_pos
            else:    
                if df1.loc[curr_pos,'pos']==1:
                    buy1=df1.loc[curr_pos,'close1'] 
                    buy2=df1.loc[curr_pos,'close2']
                    if df1.loc[prev_pos,'pos']==-1:
                        cover1=df1.loc[curr_pos,'close1'] 
                        cover2=df1.loc[curr_pos,'close2']
                        short1=df1.loc[prev_pos,'close1'] 
                        short2=df1.loc[prev_pos,'close2']
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))

                elif df1.loc[curr_pos,'pos']==0:   
                    if df1.loc[prev_pos,'pos']==1:
                        buy1=df1.loc[prev_pos,'close1'] 
                        buy2=df1.loc[prev_pos,'close2']
                        sell1=df1.loc[curr_pos,'close1'] 
                        sell2=df1.loc[curr_pos,'close2']
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                    elif df1.loc[prev_pos,'pos']==-1:
                        short1=df1.loc[prev_pos,'close1'] 
                        short2=df1.loc[prev_pos,'close2']
                        cover1=df1.loc[curr_pos,'close1'] 
                        cover2=df1.loc[curr_pos,'close2']
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                
                elif df1.loc[curr_pos,'pos']==-1:
                    short1=df1.loc[curr_pos,'close1'] 
                    short2=df1.loc[curr_pos,'close2']
                    
                    if df1.loc[prev_pos,'pos']==1:
                        sell1=df1.loc[curr_pos,'close1'] 
                        sell2=df1.loc[curr_pos,'close2']
                        buy1=df1.loc[prev_pos,'close1'] 
                        buy2=df1.loc[prev_pos,'close2']
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                        
                prev_pos=curr_pos
    return df1
    
####################################################################################################    
def top_stock_pnl(temp,entryZscore,exitZscore,stoploss):
    
    temp['action']=''
    temp['last_action']=''
    lastaction=''
    prev_zscore=0

    for i in range(1,len(temp)):
        close=temp.loc[i,'zScore']
        prev_zscore=temp.loc[i-1,'zScore']
        if temp.loc[i-1,'action']!='do nothing':
            lastaction=temp.loc[i-1,'action']       
        temp.loc[i,'last_action']=lastaction

        
        if (entryZscore>close and prev_zscore >entryZscore and lastaction!='buy' and lastaction!='short'):
            temp.loc[i,'action']='short'
            
        elif(-entryZscore<close and prev_zscore < -entryZscore and lastaction!='short' and lastaction!='buy'):
            temp.loc[i,'action']='buy'
        
        elif(lastaction=='short'):
            if(close<0):
                temp.loc[i,'action']='cover'
            elif close > stoploss:    
                temp.loc[i,'action']='cover'
        elif(lastaction=='buy' ):
            if(close>0):
                temp.loc[i,'action']='sell'
            elif close<-stoploss:    
                temp.loc[i,'action']='sell'
                
        if(temp.loc[i,'action']==''):
            temp.loc[i,'action']='do nothing'

    temp=cal_pos(temp)        
    temp=calc_pnl(temp)
    return temp
###############################################################################
def calc_unpnl(df1):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['unr_pnl']=0
    p=0
    while(p<len(df1)):
        if df1.loc[p,'pos']==1:
            buy1=df1.loc[p,'close1'] 
            buy2=df1.loc[p,'close2']
            p=p+1
            while p<len(df1) and df1.loc[p,'pos']==1:
                sell1=df1.loc[p,'close1'] 
                sell2=df1.loc[p,'close2']
                df1.loc[p,'unr_pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2))
                p+=1
        
        elif df1.loc[p,'pos']==-1:
            short1=df1.loc[p,'close1'] 
            short2=df1.loc[p,'close2']
            p+=1
            while p<len(df1) and df1.loc[p,'pos']==-1:
                cover1=df1.loc[p,'close1'] 
                cover2=df1.loc[p,'close2']
                df1.loc[p,'unr_pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                p+=1                   
        p+=1        
    return df1
###################################################################################
    
def calc_strategy_params(pnl_list,base_price=1,time_period = 'daily'):
    
    avg_profit = sum(pnl_list)/float(len(pnl_list))
    max_drawdown = 0
    temp_sum= 0
    for i in range(len(pnl_list)):
        if temp_sum+pnl_list[i]<0:
            temp_sum=temp_sum + pnl_list[i]
        else:
            temp_sum = 0
        if temp_sum<max_drawdown:
            max_drawdown =temp_sum
            

    risk_free_ratio = 7
    if time_period == 'monthly':
        scaling_factor = 12.0
    elif time_period == 'daily':
        scaling_factor =255.0
    else:
        scaling_factor = 1.0
        
    sharpe_ratio = np.sqrt(scaling_factor)*((avg_profit/base_price)*100 - risk_free_ratio/scaling_factor)/np.std([x*100 / base_price for x in pnl_list])
    return sharpe_ratio,max_drawdown

###############################################################################
def calc_n(data):
    data['n']=0
    for i in range(1,len(data)):
        
        if data.loc[i,'pos']!=0 and data.loc[i,'pos']!='':
            data.loc[i,'n']=2                            
            if data.loc[i-1,'n']!=0:
                data.loc[i,'n']=data.loc[i-1,'n']    
        else:
            data.loc[i,'n']=0
        
        start_n=data.loc[i,'n']
        if (start_n!=0):
            if data.loc[i,'pos']==1:
                if -0.5<data.loc[i,'zScore']<0:
                    data.loc[i,'n']=1
                elif data.loc[i,'zScore']>0 or data.loc[i,'zScore']<-4 :
                    data.loc[i,'n']=0
                elif -0.5<data.loc[i-1,'zScore']<0 and -1<data.loc[i,'zScore']<-0.5:
                    data.loc[i,'n']=1
                elif -1<data.loc[i-1,'zScore']<0 and -3<data.loc[i,'zScore']<-1:
                    data.loc[i,'n']=2
                elif -3<data.loc[i-1,'zScore']<-1 and -4<data.loc[i,'zScore']<-3:
                    data.loc[i,'n']=3
                elif -4<data.loc[i-1,'zScore']<-3 and -3<data.loc[i,'zScore']<-1:
                    data.loc[i,'n']=3
                elif -4<data.loc[i-1,'zScore']<-3 and -1<data.loc[i,'zScore']<-0.5:
                    data.loc[i,'n']=2
                
            elif data.loc[i,'pos']==-1:
                if 0<data.loc[i,'zScore']<0.5:
                    data.loc[i,'n']=1
                elif data.loc[i,'zScore']<0 or data.loc[i,'zScore']>4 :
                    data.loc[i,'n']=0
                elif 0<data.loc[i-1,'zScore']<0.5 and 0.5<data.loc[i,'zScore']<1:
                    data.loc[i,'n']=1
                elif 0<data.loc[i-1,'zScore']<1 and 1<data.loc[i,'zScore']<3:
                    data.loc[i,'n']=2
                elif 1<data.loc[i-1,'zScore']<3 and 3<data.loc[i,'zScore']<4:
                    data.loc[i,'n']=3
                elif 3<data.loc[i-1,'zScore']<4 and 1<data.loc[i,'zScore']<3:
                    data.loc[i,'n']=3
                elif 3<data.loc[i-1,'zScore']<4 and 0.5<data.loc[i,'zScore']<1:
                    data.loc[i,'n']=2
            elif data.loc[i,'pos']=='':
                data.loc[i,'n']=0

        elif(start_n==0):
            if data.loc[i-1,'pos']==data.loc[i,'pos']:
                data.loc[i,'n']=0
            elif data.loc[i-1,'pos']==0 and (data.loc[i,'pos']==1 or data.loc[i,'pos']==-1):
                data.loc[i,'n']=2
            elif data.loc[i,'pos']=='':
                data.loc[i,'n']=0

    return data 
#######################################################################################################
def calc_data(df,entryZscore,exitZscore=0,stoploss=4):    
    df=top_stock_pnl(df,entryZscore,exitZscore,stoploss)
    df=calc_unpnl(df)
    df['rpnl']=0
    for e in range(len(data)):
        if df.loc[e,'pnl']!=0:
            df.loc[e,'rpnl']=df.loc[e,'pnl']-0.001*(df.loc[e,'close1']+abs(beta[1])*df.loc[e,'close2'])
        else:
            df.loc[e,'rpnl']=0
    df['PNL']=np.cumsum(df['pnl'])+df['unr_pnl']
    return df
    
#################################################

###############################################################################
data = pd.read_csv('C:/Users/Nikhil/files/nifty200.csv')
stocks_list= list(data['Symbol'])
stocks_list.append('CAPF')
stocks_list.append('NESTLEIND')
stocks =stocks_list
data_stcks = pull_data(stocks_list,datetime(2014,1,1,0,0),datetime(2017,1,1,0,0))

for i in stocks:
    if stocks=='NIFTY' or stocks=='NIFTYBANK':
        continue
    f = [0]
    for j in range(1, len(data_stcks[i]['Close'])):
        f.append((data_stcks[i]['Close'][j]- data_stcks[i]['Close'][j-1])/data_stcks[i]['Close'][j])
    data_stcks[i]['Change'] = f


data={}
stocks1=[]
for r in stocks:
    df_stck1=data_stcks[r]
    if max(df_stck1['High'][0:-1].reset_index(drop=True)/df_stck1['Low'][1:].reset_index(drop=True))>2:
        print r, 'Splited'
    else:
        data[r] = df_stck1.copy().reset_index()
        stocks1.append(r)

stocks=stocks1[:]    

score=pd.DataFrame(data=None,columns=['Stock1','Stock2','sector','adf[0]','adf[1]','linReg Tvalue','beta','ub','ustd','error','pnl','sharpe_ratio'])
for k in range(len(stocks)):
    s1=stocks[k]
    try:
        df_stck1 = pd.DataFrame(data_stcks[s1])
    except:
        continue
    for j in range(k+1,len(stocks)):
        s2=stocks[j]
        if(k!=j and data_stcks[s1]['Sector'][1]==data_stcks[s2]['Sector'][1]):
            sector=data_stcks[s1]['Sector'][1]
            try:
                df_stck2 = pd.DataFrame(data_stcks[s2])
                
            except:
                continue
            if(len(df_stck1)!= len(df_stck2)):
                print 'Abort',k,j
                continue

            adfuller(data_stcks[s1]['Close'])
            adfuller(data_stcks[s1]['Change'])
#    #################################################
        
            stock1 = data_stcks[s1]['Close']/data_stcks[s1]['Close'][0]
            stock2 = data_stcks[s2]['Close']/data_stcks[s2]['Close'][0]
            stock1.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
            stock2.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
            
            x = stock1.values
            y = stock2.values
            length = len(x)
            x = x.reshape(length, 1)
            y = y.reshape(length, 1)
            regr = linear_model.LinearRegression()
            regr.fit(x, y)
            regr.coef_
            regr.intercept_
            error = (regr.predict(x) - y)
            errorl = pd.DataFrame()
            error = list(error.T[0])
            errorl['e'] = error
            errorl.index = pd.DatetimeIndex(data_stcks['NIFTY']['Date'])
            adf=adfuller(errorl['e'])###first two
            ############EG Procedure###############3
            deltay = stock2.diff().dropna()
            deltax = stock1.diff().dropna()
            X = errorl.shift(1).dropna()
            X['dx'] = deltax
            lr=LR(X, deltay)      
            ####################
            errorl['e1'] = errorl.shift(1)
            errorl = errorl.dropna()
            lm = LinearRegression()
            lm.fit(errorl[1:],errorl.shift(1).dropna(0))
#            B = lm.coef_[0][0]

            m = VAR(errorl)
            results = m.fit(1)
            results.summary()
            B=results.params['e']['L1.e']
            if B>=1:
                continue
            tau = 1/252.0
            theta = -np.log(B)/tau
            H = np.log(2)/theta
            ue = lm.intercept_[0]/(1-B)
            
            roll = H/tau
            beta = [1, -regr.coef_[0][0]]
            spread = beta[0]*stock1 + beta[1]*stock2
            ub = spread.mean()
            ustd  = spread.std()
            
            data=pd.DataFrame()
            data['Date']=spread.index
            data['close1']=data_stcks[s1]['Close']
            data['close2']=data_stcks[s2]['Close']
            spread=spread.reset_index(drop=True)
            meanSpread = spread.rolling(window=int(roll)).mean()
            stdSpread = spread.rolling(window=int(roll)).std()
                
            data['zScore'] = (spread-meanSpread)/stdSpread
            data['NiftyClose']=data_stcks['NIFTY']['Close']
            data['Nifty_AdjClose']=data['NiftyClose']-data['NiftyClose'][0]
            
            data=calc_data(data.copy(),1.8)
            for k in range(1,len(data)):
                data.loc[k,'daily_pnl']=data.loc[k,'PNL']-data.loc[k-1,'PNL']
                
            data['pnl_']=(np.cumsum(data['pnl'])+data['rpnl'])
            data['pnl_']=data['pnl_']/data['close1']
            
            
            '''
            ############################ CALCULATE %Change ################################################
            data['%change']=0
            for l in range(1,len(data)):
                data.loc[l,'%change']=((data.loc[l,'PNL']-data.loc[l-1,'PNL'])/data.loc[l-1,'PNL'])*100
            '''            
            ############### Sharpe Ration AND Max Drawdown    
            
            data['sharpe_ratio']=float('nan')
            data['max_drawdown']= float('nan')
            for p in range(100,len(data)):
                a,b=calc_strategy_params(data['daily_pnl'][p-100:p].reset_index(drop=True))
                data.loc[p,'sharpe_ratio']=a
            ############### Sharpe Ration AND Max Drawdown  ###############################
            score.loc[len(score)]=[s1,s2,sector,adf[0],adf[1],lr['t values'][1],beta,ub,ustd,errorl['e'].mean(),data['pnl_'][-1:].iloc[0],data['sharpe_ratio'].mean()]
