'''
CAlculate the list of stocks which are excluded and included in the NIfty100 list from year 2013-2015
'''
import pandas as pd
df=pd.read_excel("IndexInclExcl.xls",sheetname=2)
df3=pd.read_csv("ind_nifty100list.csv")
nifty_c = list(df3.Symbol)
df = df[["Event Date","Description","Ticker"]]
#df4 = df[92:179]
df4 = df[::-1].reset_index(drop=True)
nifty = list(nifty_c)
result = {}
for i in range(len(df4)):
    print (df4.ix[i]['Event Date'])
    print (i, len(nifty))
    if df4.ix[i]['Description'] =='Exclusion from Index ' or df4.ix[i]['Description'] =='Exclusion from Index':
        try:    
            nifty.append(df4.ix[i]['Ticker'])
        except:
            continue
    elif df4.ix[i]['Description'] =='Inclusion into Index' or df4.ix[i]['Description'] =='Inclusion into Index ':
        try:    
            nifty.remove(df4.ix[i]['Ticker'])
        except:
            continue
    else:
        print "Error"
        break
    result[df4["Event Date"][i]] = nifty[:]
list1=[]
for j in range(len(result.keys())):
    list1+=result[result.keys()[j]]
alist1=list(set(list1))
print alist1

df00 = pd.DataFrame({'Stock':alist1})
df00.to_csv("NIFTYALL2012-2015list.csv",index=False)
