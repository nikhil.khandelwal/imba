'''
Calculate highs, lows, theier counts and percentages for the given expiry week
'''
from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime
import time
from utilities import calc_expiry
import nsepy
######################################################################

access_token = "brgetBKoEWihxAsi4hNdqeQ1QVtMknCx"
from_date = datetime(2018, 1, 1 ,9, 15, 0)
to_date = datetime(2018, 4, 1 ,9, 15, 0)
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
exch="NFO"
exch2 ="NSE"
ntry=4
indexData1=pd.DataFrame(data=None,index=None,columns=None)
while(indexData1.empty and ntry>0):
    try:
        indexData1=nsepy.get_history(symbol='NIFTY 50',start=from_date.replace(year=from_date.year-5),end=to_date, index=True,futures=False)
    except Exception:
        time.sleep(100)
        pass
    ntry-=1

indexData=indexData1.reset_index()
date1=datetime(2015, 1, 1 ,9, 15, 0)
prev=0
date1=calc_expiry(date1)
exp_data=pd.DataFrame(columns=['Date','Avg(close-close)','Avg(close-max_high)','Avg(close-min_low)','win','loss','win%','loss%'])
while(date1<to_date):
    a=indexData[indexData['Date'] == date1.date()].index[0]   
    data=indexData[a-5:a].reset_index(drop=True)
    close1=data.loc[0,'Close']
    close2=data.loc[4,'Close']
#    close2=data.loc[6,'Close']

    high=max(data['High'])
    low=min(data['Low'])
    win=0
    loss=0
    win_count=0
    loss_count=0
    for i in range(1,5):
        diff=data.loc[i,'Close']-data.loc[i-1,'Close']
        if(diff>=0):
            win+=diff
            win_count+=1
        else:
            loss+=abs(diff)
            loss_count+=1
    win_per=win/(win_count*close1)  
    loss_per=loss/(loss_count*close1)
    exp_data.loc[len(exp_data)]=[str(date1.date()),((close2-close1)/close1),((high-close1)/close1),((close1-low)/close1),win_count,loss_count,win_per,loss_per]
    date1=calc_expiry(date1)
    
'''           
exp_data.to_csv("NIFTY(5TradingDays).csv")
'''