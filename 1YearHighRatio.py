'''
Prints the stock if the ratio of last 1 year high and current close is greater than 0.55   
'''

import pandas as pd
from datetime import datetime, timedelta
import nsepy
import time
from collections import defaultdict

##########################################################
root='C:/Users/Administrator/Desktop/zerodha/dashboard/'
##########################################################

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import Encoders

while(1):
    curr=datetime.now()
    if(curr.hour==8 and curr.weekday()<5):
        list1=list(pd.read_csv(root+"Nifty100_list.csv")['Scriptname'])
        
        df = defaultdict(list)
        d1=list1[:]
        splitted_stock=[]
        for i in range(len(list1)):
            tradingsym=list1[i]
            ntry=3
            while(ntry>0):
                try:
                    nsepyData=nsepy.get_history(symbol=tradingsym,start=datetime.now()-timedelta(days=365),end=datetime.now(), index=False,futures=False)
                    nsepyData=nsepyData.reset_index()
                    nsepyData.columns=map(str.lower, nsepyData.columns)
                    if max(nsepyData['high'][0:-1].reset_index(drop=True)/nsepyData['low'][1:].reset_index(drop=True))>2:
                        print tradingsym, 'Splited'
                        splitted_stock.append(tradingsym)
                        ntry=0
                    else:    
                        df[tradingsym]=nsepyData  
                        d1.remove(tradingsym)
                        ntry=0    
                except Exception:
                    print tradingsym, 'Error Fetching'
                    ntry-=1
                    pass    
                
        list1=list(set(list1).difference(set(d1)))
        
        splitted_stock=pd.DataFrame(splitted_stock,columns=['Scriptname'])
        curr_date = datetime.now()
        data = pd.DataFrame(columns=['Scriptname','Date','Close','High','Ratio less than','Ratio'])
        k=1
        for i in range(len(list1)):
            tradingsym=list1[i]    
        
            try:
                df1=df[tradingsym]
                ratio=(df1['close'].iloc[-1]/max(df1['close']))
                if ratio<0.55:
                    data.loc[len(data)]=[tradingsym,str(curr_date.date()),df1['close'].iloc[-1],max(df1['close']),'0.55',round(ratio,2)]
                elif ratio<0.60:
                    data.loc[len(data)]=[tradingsym,str(curr_date.date()),df1['close'].iloc[-1],max(df1['close']),'0.60',round(ratio,2)]
                elif ratio<0.65:
                    data.loc[len(data)]=[tradingsym,str(curr_date.date()),df1['close'].iloc[-1],max(df1['close']),'0.65',round(ratio,2)]
        
            except Exception:
                continue
        
        writer = pd.ExcelWriter(root+str(curr_date.date())+'.xlsx')
        data.to_excel(writer,'Sheet1',index=False)
        splitted_stock.to_excel(writer,'Splitted-Stocks',index=False)
        writer.save()
            
           
        try:
            fname=str(curr_date.date())+'.xlsx'    
            msg2 = MIMEMultipart(fname)
        
            mail_file = MIMEBase('application', 'xlsx')
            mail_file.set_payload(open(fname, 'rb').read())
            mail_file.add_header('Content-Disposition', 'attachment', filename=fname)
            Encoders.encode_base64(mail_file)
            msg2.attach(mail_file)
            if(curr.weekday()==3):
                msg2['Subject'] = '*ALERT:THURSDAY* NIFTY100 365 days High - ' +str(curr_date.date()) 
            else:    
                msg2['Subject'] = 'NIFTY100 365 days High - ' +str(curr_date.date()) 
            recipients=['gupta.abhishek888@gmail.com ','nkhandelwal58@gmail.com','sagar.agrwl@gmail.com']
            msg2['To'] = ", ".join(recipients)
            msg2['From'] = "nkhandelwal58@gmail.com"
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.ehlo()
            server.starttls()
            server.login("aletrs.imba@gmail.com", "Qwerty123*")
            server.sendmail("aletrs.imba@gmail.com",recipients, msg2.as_string())
            server.quit()
        except:
            print ("Netfail")
        time.sleep(((datetime.now()+timedelta(days=1)).replace(hour=8,minute=10)-datetime.now()).seconds)
    else:
        time.sleep(((datetime.now()+timedelta(days=1)).replace(hour=8,minute=10)-datetime.now()).seconds)
