from kiteconnect import KiteConnect
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from utilities import get_option_code
from scipy.stats.stats import pearsonr 
import statsmodels.tsa.stattools as ts
import urllib2
import time
import os
#############################################################################
access_token = "t3cc2ovdcjrakrmwiug7gm6lsn3gxvae"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
#############################################################################
def slope(df,value):
    x = np.arange(len(df)) 
    y = list(df[value])
    m, b = np.polyfit(x, y, 1)
    return m
def cor(df1,df2,value):
    x = list(df1[value])
    y = list(df2[value])
    c,d = pearsonr(x,y)
    return c,d
def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
###############################################################################    
def cal_pos(df):
    df['buy']=0
    df['sell']=0
    df['cover']=0
    df['short']=0

    df.loc[(df['action'] == 'buy'),'buy'] = 1
    df.loc[(df['action'] == 'short'),'short'] = 1
    df.loc[(df['action'] == 'cover'),'cover'] = 1
    df.loc[(df['action'] == 'sell'),'sell'] = 1

    flag=0
    df['pos']=''
    for p in range(0,len(df)):
        if(flag==0):
            if(df.loc[p,'buy']==0 and df.loc[p,'short']==0):
                continue
            else:
                flag=1 
                df.loc[p,'pos'] = 1 if(df.loc[p,'buy']==1) else -1
        else:
            if(df.loc[p,'buy']==1):
                df.loc[p,'pos']=1
            elif(df.loc[p,'short']==1):
                df.loc[p,'pos']=-1
            elif(df.loc[p,'sell']==1 and df.loc[p-1,'pos']==1):    
                df.loc[p,'pos']=0
            elif(df.loc[p,'cover']==1 and df.loc[p-1,'pos']==-1):
                df.loc[p,'pos']=0
            else:
                df.loc[p,'pos']=df.loc[p-1,'pos']
    
    return df
###############################################################################


###############################################################################    
def calc_pnl(df1,stock1,stock2):
    buy1=0  
    sell1=0 
    short1=0 
    cover1=0 
    buy2=0  
    sell2=0 
    short2=0 
    cover2=0
    df1['pnl']=0
    curr_pos=0
    prev_pos=0
    for p in range(1,len(df1)): 
        if (df1.loc[p,'pos'] != df1.loc[p-1,'pos']):
            curr_pos=p
            if prev_pos=='':
                prev_pos=curr_pos
            else:    
                if df1.loc[curr_pos,'pos']==1:
                    buy1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    buy2=df1.loc[curr_pos,'close_'+str(stock2)]
                    if df1.loc[prev_pos,'pos']==-1:
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))      # + ((sell1-buy1)+(buy2-sell2))

                elif df1.loc[curr_pos,'pos']==0:   
                    if df1.loc[prev_pos,'pos']==1:
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                    elif df1.loc[prev_pos,'pos']==-1:
                        short1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        short2=df1.loc[prev_pos,'close_'+str(stock2)]
                        cover1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        cover2=df1.loc[curr_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(short1-cover1)+((cover1/cover2)*(cover2-short2))
                
                elif df1.loc[curr_pos,'pos']==-1:
                    short1=df1.loc[curr_pos,'close_'+str(stock1)] 
                    short2=df1.loc[curr_pos,'close_'+str(stock2)]
                    
                    if df1.loc[prev_pos,'pos']==1:
                        sell1=df1.loc[curr_pos,'close_'+str(stock1)] 
                        sell2=df1.loc[curr_pos,'close_'+str(stock2)]
                        buy1=df1.loc[prev_pos,'close_'+str(stock1)] 
                        buy2=df1.loc[prev_pos,'close_'+str(stock2)]
                        df1.loc[p,'pnl']=(sell1-buy1)+((buy1/buy2)*(buy2-sell2)) 
                        
                prev_pos=curr_pos
    return df1
###############################################################################


###############################################################################    
def calc_mavg(mavg,thresh=100):
    mavg['mavg']=float('nan')
    mavg['stdev']=float('nan')
    for i in range(1,len(mavg)): 
        if(i>=thresh):
            mavg.loc[i,'mavg']=mavg['close'][i-thresh:i].mean()
            mavg.loc[i,'stdev']=np.std(mavg['close'][i-thresh:i])            
    return mavg
###############################################################################


###############################################################################
def top_stock_pnl(stock1,stock2,date,a=1,b=4):

    df_stck1=data_stcks[stck1.index(stock1)][(pd.to_datetime((data_stcks[stck1.index(stock1)])['date'])+timedelta(minutes=330))<=date].reset_index()
    df_stck2=data_stcks[stck1.index(stock2)][(pd.to_datetime((data_stcks[stck1.index(stock2)])['date'])+timedelta(minutes=330))<=date].reset_index()

#    df_stck1=data_stcks[stck1.index(stock1)]
#    df_stck2=data_stcks[stck1.index(stock2)]

    close_ratio=pd.DataFrame(df_stck1['close']/df_stck2['close'])
    close_ratio['date']= [d.date() for d in pd.to_datetime(df_stck1['date'])]
    

    close_ratio['close_'+str(stock1)]=df_stck1['close']
    close_ratio['close_'+str(stock2)]=df_stck2['close']

    close_ratio=calc_mavg(close_ratio)
    close_ratio['action']=''
    close_ratio['last_action']=''
    lastaction=''
    prev_close=0

    for i in range(1,len(close_ratio)):
        mavg=close_ratio.loc[i,'mavg']
        stdev=close_ratio.loc[i,'stdev']
        close=close_ratio.loc[i,'close']
        prev_close=close_ratio.loc[i-1,'close']
        if close_ratio.loc[i-1,'action']!='do nothing':
            lastaction=close_ratio.loc[i-1,'action']       
        close_ratio.loc[i,'last_action']=lastaction

        
        if ((mavg+a*stdev)>close and (prev_close > (mavg+a*stdev)) and lastaction!='buy' and lastaction!='short'):
            close_ratio.loc[i,'action']='short'
            
        elif((mavg-a*stdev)<close and (prev_close < (mavg-a*stdev))and lastaction!='short' and lastaction!='buy'):
            close_ratio.loc[i,'action']='buy'
        
        elif(lastaction=='short'):
            if(close<mavg):
                close_ratio.loc[i,'action']='cover'
            elif close > (mavg+(b*stdev)):    
                close_ratio.loc[i,'action']='cover'
        elif(lastaction=='buy' ):
            if(close>mavg):
                close_ratio.loc[i,'action']='sell'
            elif close<(mavg-(b*stdev)):    
                close_ratio.loc[i,'action']='sell'
                
        if(close_ratio.loc[i,'action']==''):
            close_ratio.loc[i,'action']='do nothing'

    close_ratio=cal_pos(close_ratio)        
    close_ratio=calc_pnl(close_ratio,stock1,stock2)
    return close_ratio
###############################################################################


###############################################################################
def calc_top_stocks(date,temp=[]):
    top_stocks=pd.DataFrame()
    if not temp:
        temp=pd.DataFrame()
        temp[['stock1','stock2']]=(score.sort_values('Std/mean')[0:10].reset_index())[['Stock1','Stock2']]
        temp.loc[len(temp)]=['CAPF','IDFCBANK']
    for k in range(len(temp)):
        stock1=temp['stock1'][k]
        stock2=temp['stock2'][k]
        a=top_stock_pnl(stock1,stock2,date)
        buyP=0
        shortP=0
        l=len(a)-1
        while(buyP==0 or shortP==0):
            if(a['action'][l]=='do nothing'):
                l-=1
            else:
                if(a['action'][l]=='buy'):
                    buyP=a['close'][l]
                    shortP=float('nan')
                elif(a['action'][l]=='short'):
                    shortP=a['close'][l]    
                    buyP=float('nan')
                else:
                    buyP=float('nan')
                    shortP=float('nan')
        top_stocks.loc[k,'stock1']=stock1
        top_stocks.loc[k,'ClosePrice_stock1']=a['close_'+str(stock1)][-1:].iloc[0]
        top_stocks.loc[k,'stock2']=stock2
        top_stocks.loc[k,'ClosePrice_stock2']=a['close_'+str(stock2)][-1:].iloc[0]
        top_stocks.loc[k,'CloseRatio']=a['close'][-1:].iloc[0]
        top_stocks.loc[k,'BuyPrice']=buyP
        top_stocks.loc[k,'ShortPrice']=shortP
        mavg=a['mavg'][-1:].iloc[0]
        stdev=a['stdev'][-1:].iloc[0]
        top_stocks.loc[k,'Moving_Average']=mavg     
        top_stocks.loc[k,'Standard_Deviation']=stdev
        top_stocks.loc[k,'Mavg+Stdev']=mavg+stdev
        top_stocks.loc[k,'Mavg-Stdev']=mavg-stdev
        top_stocks.loc[k,'Mavg+4*Stdev']=mavg+4*stdev
        top_stocks.loc[k,'Mavg-4*Stdev']=mavg-4*stdev
        top_stocks.loc[k,'Current_Position']=a['pos'][-1:].iloc[0]    
        top_stocks.loc[k,'z-value']=(a['close'][-1:].iloc[0]-mavg)/stdev
        pos_count=len(a[(a['pos']!=0) & (a['pos']!='')]['pos'])
        if(pos_count==0):
            top_stocks.loc[k,'pnl']=0
        else:    
            top_stocks.loc[k,'pnl'] = (sum(a['pnl'])/np.mean(a['close_'+stock1]))*(len(a)/pos_count)
        top_stocks.loc[k,'count_pos']= pos_count                          
        top_stocks.loc[k,'count_pnl']=len(a[a['pnl']!=0]['pos'])
    return top_stocks
###############################################################################

################################## Read last 1 year Data 
data = pd.read_csv('nifty200.csv')
stck1 = list(data['Symbol'])
stck1.append('CAPF')

stck2 = stck1

data_stcks = {}
curr_date=(datetime.now()-timedelta(days=1)).date()
from_date=curr_date.replace(year=curr_date.year-1)
print("Calculating Last 1 year data....")
for i in range(80,len(stck1)):
    try:
        indx=False
        if stck1[i] in ['NIFTY', 'BANKNIFTY']:
            indx=True
        if stck1[i] =='NIFTY':
            tradingsym= 'NIFTY 50'
        elif stck1[i] =='BANKNIFTY':
            tradingsym= 'NIFTY BANK'
        else:
            tradingsym = stck1[i]
        print tradingsym    
        ntry=6
        df_stck1=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
        while(df_stck1.empty and ntry>0):
           try:
               df_stck1  = pd.DataFrame(kite.historical(get_option_code(tradingsym, "NSE"),from_date, curr_date, 'day'))#
           except:
               pass
           ntry-=1
        data_stcks[i] = df_stck1
        data_stcks[i]['Sector']=get_sector(stck1[i].upper())
        data_stcks[i]=data_stcks[i].reset_index()
    except urllib2.HTTPError:
        print stck1[i]
        pass
print("Data calculation finish.")

################################## Read last 1 year Data

################################## Calculate their different score
k=0
columns = ['Stock1','Stock2','Sector','S_CloseRatio','S_CloseDif','S_Cor','S_pVal','Std/mean','mean','Cointegration']
data_score = np.zeros(shape=(len(stck1)*len(stck2),len(columns)))
score = pd.DataFrame(data_score,columns = columns)

for i in range(len(data_stcks)):
    try:
        df_stck1 = pd.DataFrame(data_stcks[i])

    except:
        continue
    for j in range(i,len(stck2)):
        if(i!=j and data_stcks[i]['Sector'][1]==data_stcks[j]['Sector'][1]):
            try:
                df_stck2 = pd.DataFrame(data_stcks[j])

            except:
                continue
            if(len(df_stck1)!= len(df_stck2)):
                print 'Abort',i,j
                continue
            result = df_stck1.merge(df_stck2, on='date')                

            result['Close_ratio']= result['close_x']/result['close_y']
            result['Close_dif'] = result['close_x']-result['close_y']
            sc = [stck1[i],stck2[j],data_stcks[i]['Sector'][1]]
            
            a = slope(result,'Close_ratio')
            b = slope(result,'Close_dif')
            c= np.std(result['Close_ratio'])/np.mean(result['Close_ratio'])
            d= np.mean(result['Close_ratio'])
            coin_result= ts.adfuller(result['Close_ratio'])
            score_cor,score_p = cor(df_stck1,df_stck2,'close')
            score.ix[k] = [stck1[i],stck2[j],data_stcks[i]['Sector'][1],a,b,score_cor,score_p,c,d,coin_result[1]]
            k+=1

score = score[score['Stock1']!=0]
score=score[score.Sector!='']
################################## Calculate their different score

############################################
root='C:/Users/DELL/Commodity_OptionData/'
############################################

curr_date=datetime.now().replace(hour=9,minute=15,second=0,microsecond=0)

yesterday_date=curr_date - timedelta(days = 1)    
while(data_stcks[0][(pd.to_datetime(data_stcks[0]['date'])+timedelta(minutes=330))==yesterday_date].empty):
    yesterday_date-=timedelta(days=1)

top_stocks=calc_top_stocks(yesterday_date)

prev_ydate = yesterday_date - timedelta(days = 1)    
while(data_stcks[0][(pd.to_datetime(data_stcks[0]['date'])+timedelta(minutes=330))==prev_ydate].empty):
    prev_ydate-=timedelta(days=1)

try:
    prev_data1=pd.read_csv(root+'last_day_top_stocks.csv')
    prev_data=prev_data1[['stock1','ClosePrice_stock1','stock2','ClosePrice_stock2','CloseRatio',
                          'BuyPrice','ShortPrice','Moving_Average','Standard_Deviation','Mavg+Stdev',
                          'Mavg-Stdev','Mavg+4*Stdev','Mavg-4*Stdev','Current_Position','z-value',
                          'pnl','count_pos','count_pnl']]
    if(int(time.ctime(os.path.getmtime(root+'last_day_top_stocks.csv')).split(" ")[2])!=yesterday_date.day):
        prev_data=calc_top_stocks(prev_ydate) 

except:
    prev_data=calc_top_stocks(prev_ydate) 
    
if not top_stocks[['stock1','stock2']].equals(prev_data[['stock1','stock2']]):
    top_stocks=top_stocks.append(prev_data[(~prev_data.stock1.isin(top_stocks.stock1))&(~prev_data.stock2.isin(top_stocks.stock2))], ignore_index=True)
    top_stocks=calc_top_stocks(yesterday_date,top_stocks[['stock1','stock2']])

top_stocks.to_csv(root+'curr_top_stocks.csv', index = False)
