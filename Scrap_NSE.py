'''
@author: Nikhil Khandelwal

Python 3.6

To scrap NIFTY 50 data from NSE India website for last 1 year and compute the rolling mean with
a window of 10 days.
'''


import pandas as pd
from selenium import webdriver
from datetime import datetime,timedelta
import time

def scrap_index_date(Index="NIFTY 50", from_date=datetime.now()-timedelta(days=365), to_date=datetime.now()):
    '''
    Input:
        Index: NIFTY 50 (default value)
               name of the index whose data you want to scrap
                
        from_date: current date (default value)
                   starting date from which you want to retrieve the data
        
        to_date: 1 year previous date from current date (default value)
                 ending date till which you want to retrieve the data              
    
    Output:
         Dataframe, df: Dataframe with Index's opening, closing, high, low, shares traded and 
                        turnover prices for the corresponding date
                                                                  
    '''
    #Defining a pandas DataFrame df
    df=pd.DataFrame(columns=['Date', 'Open', 'High', 'Low', 'Close', 'Shares Traded', 'Turnover(Cr)'])

    # Using Chrome to access web
    '''
    Change path to the file where chromdriver.exe file is 
        
    URL to download file: http://chromedriver.chromium.org/downloads
    '''
    driver = webdriver.Chrome("C:/Users/Downloads/chromedriver_win32/chromedriver.exe")

    while(from_date.date()<to_date.date()):
        '''
        loop to retrieve data from start date to end date
        Since a maximum of 100 enteries are shown at a time so we will run it multiple times 
        till we are able to retrieve all the data we want
        '''

        #Open the website
        driver.get('https://www.nseindia.com/products/content/equities/indices/historical_index_data.htm')

        #Select and click the Index from the drop-down list    
        el = driver.find_element_by_id('indexType')
        for option in el.find_elements_by_tag_name('option'):
            if option.text == Index:
                option.click()
                break

        #Send from date as a value to the web    
        driver.find_element_by_id("fromDate").send_keys(datetime.strftime(from_date,'%d-%m-%Y'))
        
        #Send from date as a value to the web
        '''
        Since there has to be a maximum difference of 365 days between start and end date.
        Keeping a constraint to check that
        '''
        if((to_date-from_date).days>=365):         
            driver.find_element_by_id("toDate").send_keys(datetime.strftime(from_date+timedelta(days=364),'%d-%m-%Y'))    
        else:    
            driver.find_element_by_id("toDate").send_keys(datetime.strftime(to_date,'%d-%m-%Y'))
    
        #Click submit button
        driver.find_elements_by_xpath('//*[@id="get"]')[0].click()
        
        #Wait till page is loading
        time.sleep(1)

        # Loop to extract the table data row wise
        row=4
        while(True):
            try:
                data=driver.find_elements_by_xpath('//*[@id="replacetext"]/table/tbody/tr['+str(row)+']')[0].text.split()
                df.loc[len(df)]=data  
                row+=1
            except:
                break

        #Re-initializing start date to the next day date till which we were able to retrieve the data 
        from_date=datetime.strptime(df['Date'].iloc[-1],'%d-%b-%Y')+timedelta(days=1)
    
        
    #Closing Chrome access        
    driver.close()

    return df

if __name__ == '__main__':

    #Pass parameters as Index name (str), start date(datetime), end date(datetime)
    data=scrap_index_date()

    # Calculating rolling mean with a window of 10 days
    data['Rolling_avg_10']=data['Close'].rolling(window=10).mean()
 