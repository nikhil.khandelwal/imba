'''
Nifty Call and Put Calculations
calculate the rise and fall probabilities for different years(2015 to 2018) 
for different working days(1 to 25).
   
'''

import nsepy
import math
from datetime import datetime,timedelta
import pandas as pd
from utilities import calc_expiry, get_option_code
from kiteconnect import KiteConnect
import numpy as np
import time
from strategy_params import calc_strategy_params
import matplotlib.pyplot as plt
import scipy.optimize as spo


tradingsym="NIFTY 50"
exch2 ="NSE"

access_token = "brgetBKoEWihxAsi4hNdqeQ1QVtMknCx"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
#percentile = 0.06

def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r

def get_atmStrike(price, strike_dif):
    try:
        temp = int(int(price / strike_dif)*strike_dif)
        return temp
    except:
        return 0

def calc_prob_risefall(percentile):
    '''
    To calculate the rise and fall probabilities for different years(2015 to 2018) 
    and different working days(1 to 25).
    
    Return: dataframe 
    '''
    Prob_rise_fall=pd.DataFrame(columns = ['year','workingdays', 'prob_rise', 'prob_fall'])
    for year in range(2015,2019):
    
        from_date_now = datetime.strftime(datetime(year-5,1,1), "%Y-%m-%d")
        to_date_now=datetime.strftime(datetime(year-1,12,31), "%Y-%m-%d")
        indexData  = pd.DataFrame(kite.historical_data(get_option_code(tradingsym, exch2),from_date_now, to_date_now, 'day'))

        for workingdays in range(1,27):
            indexData['Change'] = 0
            for i in range(workingdays, len(indexData)):
                indexData.loc[i, 'Change'] = calc_change(indexData.loc[i,'close'], indexData.loc[i-workingdays,'close'])
            bear = indexData[indexData['Change']<=0][['date','Change']]
            bull = indexData[indexData['Change']>=0][['date','Change']]
            max_fall = int(min(bear['Change']))
            max_rise = int(max(bull['Change']))
        
            bear_frq =bear.groupby(pd.cut(bear['Change'], np.arange(max_fall, 0, 0.25))).count()
            bull_frq =bull.groupby(pd.cut(bull['Change'], np.arange(0,max_rise, 0.25))).count()
    
            bull_frq['cum_sum'] = bull_frq.Change.cumsum()/sum(bull_frq.Change)
            bear_frq['cum_sum'] = bear_frq.Change.cumsum()/sum(bear_frq.Change)
            prob_rise = bull_frq[bull_frq['cum_sum']>1-percentile].index[0]
            prob_fall = bear_frq[bear_frq['cum_sum']<percentile].index[-1]
            Prob_rise_fall.loc[len(Prob_rise_fall)]=[int(year),workingdays,prob_rise.left,prob_fall.right]
    return Prob_rise_fall
###############################################################################

'''  
if file exists then read the file else calculate it using the calc_prob_rise_fall funtion 
'''
try:
    df=pd.read_csv('Prob_rise_fall(2015-2018).csv')
except:
    df=calc_prob_risefall(percentile=0.12)    
###############################################################################


############ Calculate Nifty Futures Data #####################################
''' Output master_fo dataframe'''

startD = datetime(2009, 8, 19)
endD=datetime(2011, 12, 31)

master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
f_date=startD
t_date=calc_expiry(f_date)

while(t_date<endD):                    
    t_date=calc_expiry(f_date)
    if(t_date>endD):
        t_date=endD
    if(f_date>t_date):
        f_date=t_date
        
    if (master_fo.empty):
        ntry=4
        while(master_fo.empty and ntry>0):
            try:
                master_fo=nsepy.get_history(symbol='NIFTY',start=f_date.date(),end=t_date.date(), index=True,futures=True,expiry_date=calc_expiry(f_date.date()))
            except AttributeError:
                time.sleep(50)
                pass
            ntry-=1
    else:
        mData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
        ntry=4
        while(mData.empty and ntry>0):
            try:
                mData=nsepy.get_history(symbol='NIFTY',start=f_date.date(),end=t_date.date(), index=True,futures=True,expiry_date=calc_expiry(f_date.date()))
                master_fo=pd.concat([master_fo,mData])
            except AttributeError as e:
                time.sleep(50)
                pass
            ntry-=1
    f_date=t_date+timedelta(days=1)    
#####################################################################################

nifty = nsepy.get_history("NIFTY", startD, endD, index= True)
pnl = pd.DataFrame(columns=['Date','Nifty_Price','atm_strike','Iprice_call','Iprice_put'])

for i in range(len(nifty)):
    currD=nifty.index[i]   
    Nifty_Price=nifty['Close'][i]
    next_exp = calc_expiry(currD)
    workingdays = np.busday_count( currD, next_exp)

    strike_dif=100.0
    atm_strike = get_atmStrike(master_fo[master_fo.index==currD]['Close'].iloc[0],strike_dif)
    price_call,price_put=(atm_strike + pow(workingdays,0.5)*0.0068*atm_strike*2.5, atm_strike- pow(workingdays,0.5)*0.0067*atm_strike*2.5)
    price_call=math.ceil(price_call /100.0)*100.0
    price_put=math.ceil(price_put /100.0)*100.0
    pnl.loc[len(pnl)]=[currD,Nifty_Price,atm_strike,price_call,price_put]
###############################################################################
    

pnl['callPrice']=0
pnl['putPrice']=0
pnl['curr_callPrice']=0
pnl['curr_putPrice']=0

for i in range(len(pnl)):
    if (i%100==0):
        print i  
    pnl.ix[i,'callPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i,'Iprice_call'], option_type = 'CE', expiry_date = calc_expiry(pnl.ix[i,'Date']))['Close'].iloc[0]
    pnl.ix[i,'putPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i,'Iprice_put'], option_type = 'PE', expiry_date = calc_expiry(pnl.ix[i,'Date']))['Close'].iloc[0]
    
    try:
        if(pnl.ix[i,'Date']!=calc_expiry(pnl.ix[i,'Date'],'prev')):
            pnl.ix[i,'curr_callPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i-1,'Iprice_call'], option_type = 'CE', expiry_date = calc_expiry(pnl.ix[i,'Date']))['Close'].iloc[0]
            pnl.ix[i,'curr_putPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i-1,'Iprice_put'], option_type = 'PE', expiry_date = calc_expiry(pnl.ix[i,'Date']))['Close'].iloc[0]
    
        else:
            pnl.ix[i,'curr_callPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i-1,'Iprice_call'], option_type = 'CE', expiry_date = calc_expiry(pnl.ix[i,'Date'],'prev'))['Close'].iloc[0]
            pnl.ix[i,'curr_putPrice'] = nsepy.get_history("NIFTY", pnl.ix[i,'Date'], pnl.ix[i,'Date'], index= True, strike_price =pnl.ix[i-1,'Iprice_put'], option_type = 'PE', expiry_date = calc_expiry(pnl.ix[i,'Date'],'prev'))['Close'].iloc[0]
    except:
        pnl.ix[i,'curr_callPrice']=0
        pnl.ix[i,'curr_putPrice']=0
        
pnl['pnlCall']=pnl['callPrice']-pnl['curr_callPrice']
pnl['pnlPut']=pnl['putPrice']-pnl['curr_putPrice']
pnl['pnlCall-Put']=pnl['pnlPut'] +pnl['pnlCall']


''' 

####  Optimization

def f(w):
    w=float(w/100.0)
    print w
    pnl['pnlCall-Put']=w*pnl['pnlPut'] +(1-w)*pnl['pnlCall']
    a= -calc_strategy_params(pnl['pnlCall-Put'],time_period='daily')['Profit Factor']
    return a 
#pnl['pnlCall-Put']=0.5*pnl['pnlPut'] +0.5*pnl['pnlCall'] - 0.1

w = 0.5
pnl['pnlCall-Put']=w*pnl['pnlPut'] +(1-w)*pnl['pnlCall']
pnl['pnlCall-Put2']=w*pnl['pnlCall'] +(1-w)*pnl['pnlPut']
    
spo.minimize_scalar(f, bounds=(1, 100), method='bounded')

############ Graph Plot    
plt.plot(pnl['Date'], pnl['pnlCall'].cumsum())
plt.plot(pnl['Date'], pnl['pnlPut'].cumsum())
plt.plot(pnl['Date'], pnl['pnlCall-Put'].cumsum())
plt.plot(pnl['Date'], pnl['pnlCall-Put2'].cumsum())


#calc_strategy_params(pnl['pnlCall-Put'],time_period='daily')
'''