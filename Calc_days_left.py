import datetime
from monthdelta import monthdelta
from time import strptime
from calendar import monthrange


def calc_days_left(d,search_month):
    """
    Input d: 'datetime format'
    Input search_month:'short abbreviation for month("JAN","FEB",..)' 
    return days left to next nifty Expiry
    """
    nd=0
    if (search_month!=d.strftime("%b")):
        while(search_month!=d.strftime("%b").upper()):
            a,max_days = monthrange(d.year, d.month)
            nd+=max_days
            d=d+monthdelta(1)
            
    date=str(d.date())    
    split_date = date.split('-')
    if len(split_date)!=3:
        print ('Check date')
        return -1
    month = int(split_date[1])
    year = int(split_date[0])
    day = int(split_date[2])
    a,max_days = monthrange(year, month)
    for i in range(1,max_days+1): 
        weekday = datetime.date(year,month,i).weekday()
        if weekday==3:
            last_thurs = i
    ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month,day)).days
    if ndays<1:
        if month<12:
            month+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month-1,day)).days
        else:
            month=1
            year+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year-1,12,day)).days 
            
    return ndays+nd,last_thurs

#tm=datetime.datetime(2017,1,1,9,15,59)                
tm=datetime.datetime.now()
emonth="MAR"
print(calc_days_left(tm,emonth))
